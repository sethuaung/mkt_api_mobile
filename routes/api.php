<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Api\ErrorController;
use App\Http\Controllers\Api\Auth\LoginController;
use App\Http\Controllers\Api\Client\ClientController;
use App\Http\Controllers\Api\Client\StaffController;
use App\Http\Controllers\Api\Client\GuarantorController;
use App\Http\Controllers\Api\Client\CenterController;
use App\Http\Controllers\Api\DataEntry\DataEntryController;

use App\Http\Controllers\Api\TestingController;
use App\Http\Controllers\Api\Loan\LoanController;
use App\Http\Controllers\Api\Loan\LoanScheduleController;
use App\Http\Controllers\Api\Loan\LoanDepositController;
use App\Http\Controllers\Api\Loan\LoanDisbursementController;

use App\Http\Controllers\Api\Repayment\RepaymentController;
use App\Http\Controllers\Api\Repayment\RepaymentDueController;
use App\Http\Controllers\Api\Repayment\RepaymentPreController;
use App\Http\Controllers\Api\Repayment\RepaymentLateController;
use App\Http\Controllers\Api\SavingWithdrawl\SavingWithdrawlController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



Route::get('testapi', [ClientController::class, 'testapi']);

//Route::group(['middleware' => ['ApiSecreteKey']], function () {
     Route::get('tokenFail', [ErrorController::class, 'tokenfail'])->name('token.fail');
     Route::post('login', [LoginController::class, 'login'])->name('login');
     Route::post('logout', [LoginController::class, 'logout'])->name('logout');
     Route::post('register', [LoginController::class, 'register']);
//});

// Data Create
//Route::group(['middleware' => ['ApiSecreteKey']], function () {
//
 	// Staff
	 Route::post('staff/detail', [StaffController::class, 'getDetail']);
     Route::post('staff/appinfo', [StaffController::class, 'getAppInfoWithStaff']);

     // Location
     Route::post('division-list', [DataEntryController::class, 'getDivision']);
     Route::post('districts-list', [DataEntryController::class, 'getDistricts']);
     Route::post('townships-list', [DataEntryController::class, 'getTownships']);
     Route::post('provinces-list', [DataEntryController::class, 'getProvinces']);
     Route::post('quarters-list', [DataEntryController::class, 'getQuarters']);
     // NRC
     Route::post('nrc/township', [DataEntryController::class, 'getTownShip']);
     Route::post('nrcname', [DataEntryController::class, 'getNRCName']);
     Route::post('nrccheck', [DataEntryController::class, 'checkNRC']);
     // Suvery & Ownership 
     Route::post('ownershipfarmland/survey',[DataEntryController::class,'getSurveyOwnership']);
     // Job
     Route::post('job/industrytype', [DataEntryController::class, 'getJobIndustryType']);
     Route::post('job/department', [DataEntryController::class, 'getJobDepartment']);
     Route::post('job/position', [DataEntryController::class, 'getJobPosition']);
     // Photo
     Route::post('photo/client', [DataEntryController::class, 'uploadPhotoClient']);
     // type
     Route::post('client/type', [DataEntryController::class, 'getClientTypeForCreate']); 
     Route::post('client/groupbycerterid', [DataEntryController::class, 'getGroupByCenterID']); 

     // Client
     Route::post('client/create/test', [ClientController::class, 'createClientTest']);
     Route::post('client/create', [ClientController::class, 'createClient']);
     Route::post('client/list', [ClientController::class, 'getClientList']);
     Route::post('client/detail', [ClientController::class, 'getClientByid']);
     Route::post('client/search', [ClientController::class, 'getClientListSearch']);

     Route::post('center/id', [ClientController::class, 'getCenterByid']); 
     // Guarantor
     //oute::post('guarant/search/id', [GuarantorController::class, 'getGuarantorByid']);
     Route::post('guarantors/search', [GuarantorController::class, 'getGuarantorSearch']);
     Route::post('guarantors/list', [GuarantorController::class, 'getGuarantorList']);
     // Center
     Route::post('center/serach/id', [CenterController::class, 'getCenterByid']); 
     // group
     Route::post('group/serach/id', [CenterController::class, 'getGroupbyid']); 

     // Loan
     Route::post('loan/list', [LoanController::class, 'getLoanList']); 
	 Route::post('loan/list/pending', [LoanController::class, 'getLoanPendingList']); 
 	 Route::post('loan/detail', [LoanController::class, 'getLoanDetail']); 

     Route::post('loan/for/client/list', [LoanController::class, 'getClientListforLoan']); 
     Route::post('loan/for/guarantor/list', [LoanController::class, 'getGuarantorListforLoan']); 
     Route::post('loan/for/businesstype/list', [LoanController::class, 'getBusinessTypeforLoan']); 
     Route::post('loan/for/businesscategory/list', [LoanController::class, 'getBusinessCategoryforLoan']); 
     Route::post('loan/for/type/list', [LoanController::class, 'getLoanTypeForCreate']);
     Route::post('loan/for/chargescompulsory/list', [LoanController::class, 'getChargesCompulsory']);

     Route::post('loan/search/type', [LoanController::class, 'getLoanType']);
     Route::post('loan/search/general', [LoanController::class, 'searchByGeneral']);
     Route::post('loan/search/status', [LoanController::class, 'getChargesCompulsory']);

 	 Route::post('loan/schedule', [LoanController::class, 'getLoanSchedule']);

     // Deposit
     Route::post('loan/deposit', [LoanDepositController::class, 'getLoanDepositApprovedList']);
     Route::post('loan/deposit/active', [LoanDepositController::class, 'getLoanDepositActiveList']);
	 Route::post('loan/deposit/create', [LoanDepositController::class, 'createDeposit']);
     Route::post('loan/deposit/search', [LoanDepositController::class, 'getLoanDepositListSearch']);
	 Route::post('loan/deposit/total', [LoanDepositController::class, 'getLoanDepositTotal']);

     // Disbursement
     Route::post('loan/disbursement', [LoanDisbursementController::class, 'getLoanDepositApprovedList']);
     Route::post('loan/disbursement/active', [LoanDisbursementController::class, 'getLoanDisbursemenActivetList']);
     Route::post('loan/disbursement/create', [LoanDisbursementController::class, 'createDisbursement']);
     Route::post('loan/disbursement/search', [LoanDisbursementController::class, 'getLoanDisbursementListSearch']);

     // Repayment
     Route::post('loan/repayment', [RepaymentController::class, 'getLoanRepaymentActiveList']);
 	 Route::post('loan/repayment/search', [RepaymentController::class, 'getLoanRepaymentActiveListSearch']);

     // Repayment schedule
     Route::post('repayment/schedule', [RepaymentController::class, 'RepaymentSchduleListByLoanID']);
	 Route::post('repayment/schedule/detail', [RepaymentController::class, 'getLoanRepaymentDetailByScheduleID']);

     // Repayment Pre
     Route::post('repayment/pre/create', [RepaymentPreController::class, 'createRepaymentPre']);
     // Repayment Due
     Route::post('repayment/due/create', [RepaymentDueController::class, 'createRepaymentDue']);
     // Repayment Late
     Route::post('repayment/late/create', [RepaymentLateController::class, 'createRepaymentLate']);

     Route::post('test/api', [TestingController::class, 'getLoanSchedule']);

     Route::post('test/loanschedule', [LoanScheduleController::class, 'getLoanSchedule']);

 	 // savingwithdrawl
 	 Route::post('savingwithdrawl/list', [SavingWithdrawlController::class, 'getSavingWithdrawlList']);
	 Route::post('savingwithdrawl/search', [SavingWithdrawlController::class, 'getSavingWithdrawlListSearch']);

	 // news && announcements
 	 Route::post('news/list', [DataEntryController::class, 'getNews']);
	 Route::post('announcements/list', [DataEntryController::class, 'getAnnouncements']);

//});

Route::group(['middleware' => ['ApiSecreteKey', 'auth:api']], function () {
     Route::post('staff-list', [StaffController::class, 'getList']);
});

