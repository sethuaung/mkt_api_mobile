<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Intervention\Image\ImageManagerStatic as Image;

class ResizeController extends Controller
{
    public function resizeImage(Request $request)
    {
                // $this->validate($request, [
        //     'file' => 'required|image|mimes:jpg,jpeg,png,gif,svg|max:2048',
        // ]);
        // // image store path 
        // $destinationPath = public_path('images/clients');
        // $thumbnailPath = public_path('clientphotos');
        // if (isset($_FILES['image'])) {
        //     // delete old image
        //     File::delete(public_path('clientphotos'.$request->image));
        //     $image = request()->file('image');
        //     $image_name = uniqid().'_'.'p'.'_'. $image->getClientOriginalName();
        //     $resize_image = Image::make($image->getRealPath());
        //     // custom resize image
        //     $resize_image->resize(500, 500, function($constraint){
        //         $constraint->aspectRatio();
        //     })->save($thumbnailPath . '/' . $image_name);

        //     $image->move($destinationPath, $image_name);
        // } else {
        //     $image_name = $request->image;
        // }
	    $this->validate($request, [
            'file' => 'required|image|mimes:jpg,jpeg,png,gif,svg|max:2048',
        ]);

        $image = $request->file('file');
        $input['file'] = time().'.'.$image->getClientOriginalExtension();
        
        $destinationPath = public_path('/thumbnail');

        $imgFile = Image::make($image->getRealPath());

        $imgFile->resize(150, 150, function ($constraint) {
		    $constraint->aspectRatio();
		})->save($destinationPath.'/'.$input['file']);

        $destinationPath = public_path('/uploads');
        $image->move($destinationPath, $input['file']);

        return back()
        	->with('success','Image has successfully uploaded.')
        	->with('fileName',$input['file']);
    }
}
