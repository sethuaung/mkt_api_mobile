<?php

namespace App\Http\Controllers\Api\SavingWithdrawl;

use App\Http\Controllers\Controller;
use App\Models\Branch;
use App\Models\CenterLeader;
use App\Models\Group;
use App\Models\Guarantor;
use App\Models\Loan\LoanBusinessCategory;
use App\Models\Loan\LoanBusinessType;
use App\Models\Loan\LoanInfo;
use App\Models\Loan\LoanType;
use App\Models\Staff;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SavingWithdrawlController extends Controller
{
    //auto get branch id
    public function getBranchID($staffid){
        //dd($request->staffid);
        $obj_branchcode = DB::table('tbl_staff')
            ->join('tbl_branches', 'tbl_branches.id', '=', 'tbl_staff.branch_id')
            ->where('tbl_staff.staff_code',$staffid)
            ->select('tbl_branches.branch_code')
            ->first();
        
        $res = strtolower((string)$obj_branchcode->branch_code); 
        return $res;               
    } 
       // showLoanList
    public function getSavingWithdrawlList (Request $request)
    {
    	//dd($request);
        $branchid = $request->branchid;
        $staffid = $request->staffid;
    
        $bcode = "";
        $bcode = $this->getBranchID($staffid);

        $data = DB::table($bcode.'_saving_withdrawl')
        //->leftjoin('tbl_client_basic_info', 'tbl_client_basic_info.client_uniquekey', '=', $bcode.'_saving_withdrawl.client_idkey')
        ->leftjoin('tbl_main_join_saving', 'tbl_main_join_saving.main_loan_id', '=', $bcode.'_saving_withdrawl.loan_unique_id')
        ->leftjoin('tbl_main_join_loan', 'tbl_main_join_loan.main_loan_id', '=', 'tbl_main_join_saving.main_loan_id')
        ->leftjoin($bcode.'_loans', $bcode.'_loans.loan_unique_id', '=', 'tbl_main_join_loan.portal_loan_id')
        ->leftjoin('tbl_main_join_client', 'tbl_main_join_client.portal_client_id', '=', $bcode.'_loans.client_id')
        ->where($bcode.'_loans.loan_officer_id', $staffid)
        ->select($bcode.'_saving_withdrawl.*',$bcode.'_loans.loan_officer_id','tbl_main_join_loan.main_loan_code','tbl_main_join_client.main_client_code')
        ->orderBy($bcode.'_saving_withdrawl.saving_unique_id', 'DESC')
        ->get();
    
    	// $mainStaffID = DB::table('tbl_main_join_staff')->where('portal_staff_id', $staffid)->first()->main_staff_id;
    	// $data=DB::connection('main')->table('loan_compulsory_'.$branchid)
    	// ->leftJoin('loans_'.$branchid, 'loans_'.$branchid.'.id', '=', 'loan_compulsory_'.$branchid.'.loan_id')
    	// ->where('loans_'.$branchid.'.loan_officer_id', $mainStaffID)
    	// ->where('compulsory_status','=','Active')
    	// ->select('loan_id')
    	// ->get();
    	
		// $saving_active_loans=array();
//     	foreach($data as $d){
//         	$portal_info=DB::connection('portal')->table('tbl_main_join_loan')->where('main_loan_id',$d->loan_id)->first();
        	
//         	$principle=DB::connection('main')->table('compulsory_saving_transaction')
//         	->where('loan_id',$d->loan_id)
//             ->where('train_type_ref','=','deposit')
//         	->first()->amount;
        
//         	$interest=DB::connection('main')->table('compulsory_saving_transaction')
//         		->where('loan_id',$d->loan_id)
//         		->where('train_type_ref','=','accrue-interest')
//         		->get();  
//         	$total_interest=0;
//         	foreach($interest as $i){
//         		$total_interest += $i->amount;
//         	}
        
//         	$client_info=DB::table($bcode.'_loans')
//         	->leftJoin('tbl_client_basic_info','tbl_client_basic_info.client_uniquekey',$bcode.'_loans.client_id')
//         	->where($bcode.'_loans.loan_unique_id',$portal_info->portal_loan_id)
//         	->first();
        	
//         	if(DB::connection('main')->table('compulsory_saving_transaction')->where('loan_id',$d->loan_id)->where('train_type_ref','=','withdraw')->first()){
//     			$withdraw_saving_info=DB::connection('main')->table('compulsory_saving_transaction')
//         			->where('loan_id',$d->loan_id)
//             		->where('train_type_ref','=','withdraw')
//             		->orderBy('id','desc')
//         			->first();
            
//     			$principle_balance=$withdraw_saving_info->total_principle;
//                 $interest_balance=$withdraw_saving_info->total_interest;
//     			// $total_balance=$withdraw_saving_info->available_balance;
				
//         			// $withdraw_total=DB::connection('main')->table('compulsory_saving_transaction')
//         			// ->where('loan_id',$main_loan_id)
//         			// ->where('train_type_ref','=','withdraw')
//         			// ->get();
//         			// $total_withdrew=0;
//         			// foreach($withdraw_total as $wt){
//         			// $total_withdrew += abs($wt->amount);
//         			// }
            	
//             }else{
//     			$principle_balance=$principle;
//     			$interest_balance=$total_interest;
//     			// $total_balance=$principle + $total_interest;
//             	// $total_withdrew=0;
//             }  
//         	array_push($saving_active_loans,[
//             	'loan_unique_id'=>$portal_info->portal_loan_id,
//             	'total_saving'=>$principle,
//             	'total_interest'=>$total_interest,
//             	'client_idkey'=>$client_info->client_uniquekey,
//             	'client_name'=>$client_info->name,
//             	// 'saving_withdrawl_amount'=> ,
//             	// 'interest_withdrawl_amount' => ,
//             	'saving_left' => $principle_balance,
//             	'interest_left'=> $interest_balance
//              ]);
//         }
    	
    
        if($data){
            return response()->json(['status_code'=>200,'message'=>'success','data'=>$data]);
        }else{
            return response()->json(['status_code'=>422,'message'=>'fail','data'=>null]);
        }
    }

       // showLoanList
    public function getSavingWithdrawlListSearch (Request $request)
    {
    	//dd($request);
        $branchid = $request->branchid;
        $staffid = $request->staffid;
     	$searchvalue = $request->searchvalue;
    
        $bcode = "";
        $bcode = $this->getBranchID($staffid);

    	$data = DB::table($bcode.'_saving_withdrawl')
        ->leftjoin('tbl_main_join_saving', 'tbl_main_join_saving.main_loan_id', '=', $bcode.'_saving_withdrawl.loan_unique_id')
        ->leftjoin('tbl_main_join_loan', 'tbl_main_join_loan.main_loan_id', '=', 'tbl_main_join_saving.main_loan_id')
        ->leftjoin($bcode.'_loans', $bcode.'_loans.loan_unique_id', '=', 'tbl_main_join_loan.portal_loan_id')
        ->leftjoin('tbl_main_join_client', 'tbl_main_join_client.portal_client_id', '=', $bcode.'_loans.client_id')
        ->leftjoin('tbl_client_basic_info', 'tbl_client_basic_info.client_uniquekey', '=', $bcode.'_saving_withdrawl.client_idkey')
        ->leftjoin('loan_type', 'loan_type.id', '=', $bcode.'_loans.loan_type_id')
        ->where($bcode.'_loans.loan_unique_id', 'LIKE', $searchvalue . '%')
        ->orWhere($bcode.'_loans.client_id', 'LIKE', $searchvalue . '%')
        ->orWhere($bcode.'_loans.center_id', 'LIKE', $searchvalue . '%')
        ->orWhere($bcode.'_loans.group_id', 'LIKE', $searchvalue . '%')
        ->orWhere($bcode.'_loans.center_id', 'LIKE', $searchvalue . '%')
        ->orWhere($bcode.'_saving_withdrawl.saving_unique_id', 'LIKE', $searchvalue . '%')
        ->orWhere($bcode.'_saving_withdrawl.client_name', 'LIKE', $searchvalue . '%')
        ->where($bcode.'_loans.loan_officer_id',  $staffid)
        ->select($bcode.'_saving_withdrawl.*')
        ->orderBy($bcode.'_saving_withdrawl.saving_unique_id', 'DESC')
        ->get();
    
//     	$main_loan_id=DB::table('tbl_main_join_loan')
//         		->leftjoin($bcode.'_loans', $bcode.'_loans.loan_unique_id', '=', 'tbl_main_join_loan.portal_loan_id')
//         		->where('tbl_main_join_loan.portal_loan_id','LIKE',$searchvalue.'%')
//         		->orWhere('tbl_main_join_loan.main_loan_code','LIKE',$searchvalue.'%')
//         		->orWhere($bcode.'_loans.client_id','LIKE',$searchvalue.'%')
//         		->select('tbl_main_join_loans.main_loan_id')->first()->main_loan_id;
    	
//       	$portal_info=DB::connection('portal')->table('tbl_main_join_loan')->where('main_loan_id',$main_loan_id)->first();
    
//     	$principle=DB::connection('main')->table('compulsory_saving_transaction')
//         	->where('loan_id',$main_loan_id)
//             ->where('train_type_ref','=','deposit')
//         	->first()->amount;
        
//         	$interest=DB::connection('main')->table('compulsory_saving_transaction')
//         		->where('loan_id',$main_loan_id)
//         		->where('train_type_ref','=','accrue-interest')
//         		->get();  
//         	$total_interest=0;
        	// foreach($interest as $i){
        	// 	$total_interest += $i->amount;
        	// }
        
//         	$client_info=DB::table($bcode.'_loans')
//         	->leftJoin('tbl_client_basic_info','tbl_client_basic_info.client_uniquekey',$bcode.'_loans.client_id')
//         	->where($bcode.'_loans.loan_unique_id',$portal_info->portal_loan_id)
//         	->first();
        	
//         	if(DB::connection('main')->table('compulsory_saving_transaction')->where('loan_id',$main_loan_id)->where('train_type_ref','=','withdraw')->first()){
//     			$withdraw_saving_info=DB::connection('main')->table('compulsory_saving_transaction')
//         			->where('loan_id',$main_loan_id)
//             		->where('train_type_ref','=','withdraw')
//             		->orderBy('id','desc')
//         			->first();
            
//     			$principle_balance=$withdraw_saving_info->total_principle;
//                 $interest_balance=$withdraw_saving_info->total_interest;
//     			// $total_balance=$withdraw_saving_info->available_balance;
				
//         			// $withdraw_total=DB::connection('main')->table('compulsory_saving_transaction')
//         			// ->where('loan_id',$main_loan_id)
//         			// ->where('train_type_ref','=','withdraw')
//         			// ->get();
//         			// $total_withdrew=0;
//         			// foreach($withdraw_total as $wt){
//         			// $total_withdrew += abs($wt->amount);
//         			// }
            	
//             }else{
//     			$principle_balance=$principle;
//     			$interest_balance=$total_interest;
//     			// $total_balance=$principle + $total_interest;
//             	// $total_withdrew=0;
//             }  
//         	array_push($saving_active_loans,[
//             	'loan_unique_id'=>$portal_info->portal_loan_id,
//             	'total_saving'=>$principle,
//             	'total_interest'=>$total_interest,
//             	'client_idkey'=>$client_info->client_uniquekey,
//             	'client_name'=>$client_info->name,
//             	// 'saving_withdrawl_amount'=> ,
//             	// 'interest_withdrawl_amount' => ,
//             	'saving_left' => $principle_balance,
//             	'interest_left'=> $interest_balance
//              ]);
        
        if($data){
            return response()->json(['status_code'=>200,'message'=>'success','data'=>$data]);
        }else{
            return response()->json(['status_code'=>422,'message'=>'fail','data'=>null]);
        }
    }

}