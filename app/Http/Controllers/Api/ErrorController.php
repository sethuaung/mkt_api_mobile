<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ErrorController extends Controller
{
    public function tokenfail(Request $request)
    { 
        return response()->json(['status_code'=>500,'message'=>'Su Taung Lay']);
    }
}
