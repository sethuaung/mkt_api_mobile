<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class TestingController extends Controller
{
    public function converttoImage(Request $request)
    {

        $datacid = $request->clientid;
        $dataimage = $request->image;
        $image = str_replace('data:image/png;base64,', '', $dataimage);
        $image_name = uniqid().'_'.$datacid.'_'.time().'.jpg';
    
        $disk = Storage::disk('clients-photo')->put($image_name, base64_decode($image));
        dd($disk );
    }

}
