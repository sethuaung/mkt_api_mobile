<?php

namespace App\Http\Controllers\Api\DataEntry;

use App\Http\Controllers\Controller;
use App\Models\Branch;
use App\Models\CenterLeader;
use Illuminate\Http\Request;

use App\Models\ClientBasicInfo;
use App\Models\ClientFamilyInfo;
use App\Models\ClientInfo;
use App\Models\ClientJob;
use App\Models\ClientJoin;
use App\Models\CustomerType;
use App\Models\Photo;
use App\Models\SurveyOwnership;

use App\Models\Division;
use App\Models\Districts;
use App\Models\Group;
use App\Models\GroupLoan;
use App\Models\Guarantor;
use App\Models\Townships;
use App\Models\Provinces;
use App\Models\Quarters;
use App\Models\NRC;

use App\Models\JobIndustryType;
use App\Models\JobDepartment;
use App\Models\JobPosition;
use App\Models\Ownership;
use App\Models\OwnershipFarmlands;
use App\Models\Staff;
use App\Models\Surveys;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

class DataEntryController extends Controller
{

    public function getClientTypeForCreate(Request $request){
        // Generate Client ID
        $branchid =  $request->branchid;
        $staffid = $request->staffid;
        $obj_branchid = Branch::where('id',$branchid)->get()->last();
        //$obj_branchid = 'AB-22-00997';

        // Get Loan Officer 
        $obj_staff = DB::table('tbl_staff')
        	->join('tbl_main_join_staff', 'tbl_main_join_staff.portal_staff_id', '=', 'tbl_staff.staff_code')
        	->where('tbl_staff.branch_id', $branchid)
            ->where('tbl_staff.staff_code', $staffid)
        	///->select('tbl_staff.id','staff_code','name')
        	->get();

        // Get Center
        $obj_center = DB::table('tbl_center')
        	->join('tbl_main_join_center', 'tbl_main_join_center.portal_center_id', '=', 'tbl_center.center_uniquekey')
        	->where('tbl_center.branch_id', $branchid)
        	->where('tbl_center.staff_id', $staffid)
        	->get();

        // Get Group
        //$obj_group = Group::where('branch_id', $branchid)->get();
        $obj_group = DB::table('tbl_group')
        	->join('tbl_main_join_group', 'tbl_main_join_group.portal_group_id', '=', 'tbl_group.group_uniquekey')
            //->where('tbl_group.center_id', $staffid)
        	->get();

        // Get Customer Type
    	$obj_guarantor = DB::table('tbl_guarantors')
        	->leftjoin('tbl_main_join_guarantor', 'tbl_main_join_guarantor.portal_guarantor_id', '=', 'tbl_guarantors.guarantor_uniquekey')
            ->where('tbl_guarantors.branch_id', $branchid)
        	->get();

        $data = array(
            array(
                "clientid" =>"",
                "branchid" => $obj_branchid->id,
                "branchname" => $obj_branchid->branch_name,
                "branchcode" => $obj_branchid->branch_code,
                // "loanofficer-list" => $obj_staff,
                // "center-list" => $obj_center,
                // "group-list" => $obj_group,
                // "customer-type" => $obj_customertype,
                // "guarantor-list" => $obj_guarantor,
            )
        );
        return response()->json(['status_code'=>200, 'message'=>'success',
        'data'=>$data,'loanofficer-list'=>$obj_staff,'center-list'=>$obj_center,'group-list'=>$obj_group,'guarantor-list'=>$obj_guarantor]);
    }

    public function getGroupByCenterID(Request $request){
        // Generate Client ID
        $branchid =  $request->branchid;
     	$centerid =  $request->centerid;
        // Get Group
        $obj_group = Group::join('tbl_main_join_group', 'tbl_main_join_group.portal_group_id', '=', 'tbl_group.group_uniquekey')
        	->where('center_id',$centerid)->where('branch_id', $branchid)->get();

        return response()->json(['status_code'=>200, 'message'=>'success','group-list'=>$obj_group]);
    }

    // Location
    public function getDivision()
    {
       $entery = Division::get();
       if($entery){
        return response()->json(['status_code'=>200, 'message'=>'success','data'=>$entery]);
        }else{
            return response()->json(['status_code'=>500,'message'=>'division not found','data'=>null]);
        }
    }

    public function getDistricts(Request $request)
    {
       $divisionid =  $request->divisionid;
       $entery = Districts::where('division_id',$divisionid)->get();
       if($entery){
        return response()->json(['status_code'=>200, 'message'=>'success','data'=>$entery]);
        }else{
            return response()->json(['status_code'=>500,'message'=>'districts not found','data'=>null]);
        }
    }

    public function getTownships(Request $request)
    {
       $districtsid =  $request->districtsid;
       $entery = Townships::where('district_id',$districtsid)->get();
       if($entery){
        return response()->json(['status_code'=>200, 'message'=>'success','data'=>$entery]);
        }else{
            return response()->json(['status_code'=>500,'message'=>'townships not found','data'=>null]);
        }
    }

    public function getProvinces(Request $request)
    {
       $district_id =  $request->districtid;
       $entery = Provinces::where('district_id',$district_id)->get();
       if($entery){
        return response()->json(['status_code'=>200, 'message'=>'success','data'=>$entery]);
        }else{
            return response()->json(['status_code'=>500,'message'=>'provinces not found','data'=>null]);
        }
    }

    public function getQuarters(Request $request)
    {
       $townshipid =  $request->townshipid;
       $entery = Quarters::where('township_id',$townshipid)->get();
       if($entery){
        return response()->json(['status_code'=>200, 'message'=>'success','data'=>$entery]);
        }else{
            return response()->json(['status_code'=>500,'message'=>'quarters not found','data'=>null]);
        }
    }

    public function getNRCName(Request $request)
    {
        $nrccode =  $request->nrccode;
        $entery = NRC::where('nrc_code',$nrccode)->orderBy('id', 'desc')->get();
        if($entery){
            return response()->json(['status_code'=>200, 'message'=>'success','data'=>$entery]);
        }else{
            return response()->json(['status_code'=>500,'message'=>'nrc name not found','data'=>null]);
        }
    }
    // Job 
    public function getJobIndustryType()
    {
        $entery = JobIndustryType::all();
        if($entery){
        return response()->json(['status_code'=>200, 'message'=>'success','data'=>$entery]);
        }else{
            return response()->json(['status_code'=>500,'message'=>'industry type not found','data'=>null]);
        }
    }

    public function getJobDepartment(Request $request)
    {
        $industryid =  $request->industryid;
        $entery = JobDepartment::where('industry_id',$industryid)->orderBy('id', 'desc')->get();
        if($entery){
            return response()->json(['status_code'=>200, 'message'=>'success','data'=>$entery]);
        }else{
            return response()->json(['status_code'=>500,'message'=>'job department not found','data'=>null]);
        }
    }

    public function getJobPosition(Request $request)
    {
        $departmentid =  $request->departmentid;
        $entery = JobPosition::where('department_id',$departmentid)->orderBy('id', 'desc')->get();
        if($entery){
            return response()->json(['status_code'=>200, 'message'=>'success','data'=>$entery]);
        }else{
            return response()->json(['status_code'=>500,'message'=>'job position not found','data'=>null]);
        }
    }

    public function getSurveyOwnership(Request $request)
    {
        //remove_accets, un_remove_accets
        if($request->status == "remove_accets"){
            $dataownershipfarmlands = OwnershipFarmlands::select('',)->get();
        }else{

        }
        
        if($dataownershipfarmlands){
            return response()->json(['status_code'=>200, 'message'=>'success','dataownershipfarmlands'=>$dataownershipfarmlands]);
        }else{
            return response()->json(['status_code'=>500,'message'=>'quarters not found','data'=>null]);
        }
    }

    //NRC Township for check
    public function getTownShip(Request $request)
    {
        $stateid = request()->stateid;
        $datanrctownship = NRC::where('nrc_code',$stateid)->select('name_en', 'name_mm', 'nrc_code')->get();
        return response()->json(['status_code'=>200,'message'=>'Success to get data','data'=> $datanrctownship]);
    }

    //NRC check
    public function checkNRC(Request $request)
    {
        $req_clientname = $request->client_name;
        $req_nrc = $request->nrc;
        $req_nrc_status = $request->nrc_status;
            
        if($req_nrc_status == 'new'){
            $data_nrc = ClientBasicInfo::where('nrc', $req_nrc)->get();
        }else if($req_nrc_status == 'old'){
            $data_nrc = ClientBasicInfo::where('old_nrc', $req_nrc)->get();
        }else if($req_nrc_status == 'cardid'){
            $data_nrc = ClientBasicInfo::where('nrc_card_id', $req_nrc)->get();
        }else{
            return response()->json(['status_code'=>422,'message'=>'nrc status not found']);
        }
            
        if (count($data_nrc) > 0) {
            return response()->json(['status_code'=>200,'message'=>'nrc found','data'=>$data_nrc]);
        }else{
            return response()->json(['status_code'=>422,'message'=>'nrc not found']);
        }
    }

    // Photo
    public function uploadPhotoClient(Request $request)
    {
        date_default_timezone_set('asia/yangon');
        $this->converttoImage($request->clientphoto);
        //$this->converttoImage($request->nrc_front_photo);
    }

    //decode base64 string
    public function converttoImage($image)
    {
        $image = str_replace('data:image/png;base64,', '', $image);
        $image = str_replace(' ', '+', $image);
        $image_name = uniqid().'_'.time().'.jpg';

        $destinationPath = public_path('images/clients'). $image_name;
        $data = base64_decode($image);
        file_put_contents($destinationPath, $data);
        return $image_name;
    }

    public function getNews(Request $request)
    {
    	$branchid = $request->branchid;
        $staffid = $request->staffid;
        //remove_accets, un_remove_accets
 		$data = DB::table('tbl_news')
        ->orderBy('updated_at', 'DESC')
        ->limit(30)
        ->get();

        if($data){
            return response()->json(['status_code'=>200, 'message'=>'success','data'=>$data]);
        }else{
            return response()->json(['status_code'=>500,'message'=>'quarters not found','data'=>null]);
        }
    }

    public function getAnnouncements(Request $request)
    {
    	$branchid = $request->branchid;
        $staffid = $request->staffid;
        //remove_accets, un_remove_accets
 		$data = DB::table('tbl_announcements')
       		->orderBy('updated_at', 'DESC')
        	->get();

        if($data){
            return response()->json(['status_code'=>200, 'message'=>'success','data'=>$data]);
        }else{
            return response()->json(['status_code'=>500,'message'=>'quarters not found','data'=>null]);
        }
    }
}
