<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Models\Staff;
use App\Models\User;
use App\Models\Branch;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Laravel\Passport\Client as PClient;

class LoginController extends Controller
{
    private $client;
    public function __construct() {
        $this->client = PClient::find(1);
    }

    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        // $v = Validator::make(request()->all(), [
        //     'user_acc' => 'required|email',
        //     'user_password' => 'required'
        // ]);

        // if ($v->fails()){
        //     return response(['errors'=>$v->errors()->all()], 422);
        // }

        // staff check
        $staffcheck = Staff::where('nrc', $request->nrc)->first();
        if ($staffcheck) {
            return response()->json(['status_code'=>422,'message'=>'staff account is created in use this nrc']);
        }

        $user = User::where('email', $request->email)->first();
        // user check
        if (!$user) {
            // user password check
            $usercreate = User::create([
                'name' => request('name'),
                'email' => request('email'),
                'password' =>Hash::make(request('user_password')),
                'created_at' => now(),
                'updated_at' => now()
            ]);
            // user create success or fail
            if($usercreate){
               
                $token = $usercreate->createToken('RITzMKt')->accessToken;
                $staff = Staff::create([
                    'staff_code' => request('staff_code'),
                    'name' => request('name'),
                    'email' => request('email'),
                    'phone' => request('phone'),
                    'father_name' => request('father_name'),
                    'nrc' => request('nrc'),
                    'full_address' => request('full_address'),
                    'user_acc' => request('user_acc'),
                    'user_password' => Hash::make(request('user_password')),
                    'branch_id' => request('branch_code'),
                    'del_status' => request('del_status'),
                    'user_token' => $token,
                ]);
                 //$token = $usercreate->createToken('RITzMKt')->accessToken;
                 
                 return response()->json(['status_code'=>200,'message'=>'success to create staff','data'=>$staff]);
            }else{
                return response()->json(['status_code'=>422,'message'=>'fail to create staff','data'=>null]);
            }
        } else {
            $response = ["message" =>'User does not exist'];
            return response($response, 422);
        }
    }
   
    public function login (Request $request) {
        date_default_timezone_set('asia/yangon');
        $errors = Validator::make(request()->all(), [
            'email'=>'required',
            'password'=>'required'
        ]);
        
        if($errors->fails()){
            return $errors->errors()->toJson();
        }

        $stafflogin = Staff::where('email', $request->email)->first();
        if ($stafflogin) {
            if (Hash::check($request->password, $stafflogin->user_password)) {
                $staff_id = $stafflogin->staff_code;
                $staff_name = $stafflogin->name;
                $staff_photo = $stafflogin->photo;
                $branch_id = $stafflogin->branch_id;
                $user_token = $stafflogin->user_token;
                
                return response()->json(['status_code'=>200,'message'=>'login succeess',
                'staff_id'=>$staff_id,'staff_name'=>$staff_name,'staff_photo'=>$staff_photo,
                'branch_id'=>$branch_id,'access_token'=>$user_token]);
            } else {
                return response()->json(['status_code'=>422,'message'=>'Password mismatch']);
            }
        }else{
            return response()->json(['status_code'=>422,'message'=>'email not found']);
        }
    }

    public function logout (Request $request) {
        $stafftoken = Staff::where('user_token', $request->user_token)->first();
        if ($stafftoken) {
            $stafftokenremove = Staff::where('user_token', $request->user_token)->update(array('user_token' => ""));  
            if ($stafftokenremove) {
                User::where('email', $stafftoken->email)->delete();
                $token = $request->user_token;
                $token->revoke();

                return response()->json(['status_code'=>200,'message'=>'logout succeess']);
            } else {
                return response()->json(['status_code'=>422,'message'=>'logout fail']);
            }
        }else{
            return response()->json(['status_code'=>422,'message'=>'user not found']);
        }
    }


    public function forgestpassword (Request $request) {
        date_default_timezone_set('asia/yangon');
        $errors = Validator::make(request()->all(), [
            'nrc'=>'required',
            'father_name'=>'required'
        ]);
        
        if($errors->fails()){
            return $errors->errors()->toJson();
        }

        $staffforgestpassword = Staff::where('nrc', $request->nrc)->where('father_name', $request->father_name)->first();
        if ($staffforgestpassword) {
            $OTPcode = $this->generateOTPnum(6);
            $datamessage = 'you otp is '. $OTPcode.'.Dont share other person.';
            return response()->json(['status_code'=>200,'message'=> $datamessage,'data'=>$staffforgestpassword]);
        }else{
            return response()->json(['status_code'=>422,'message'=>'email not found']);
        }
    }

    public function passwordupdate (Request $request) {
        date_default_timezone_set('asia/yangon');
        $errors = Validator::make(request()->all(), [
            'password'=>'required'
        ]);
        
        if($errors->fails()){
            return $errors->errors()->toJson();
        }

        $staffpasswordupdate = Staff::where('user_token', $request->user_token)->update(array('user_token' => ""));  
        if ($staffpasswordupdate) {
            User::where('email', $staffpasswordupdate->email)->delete();
            $token = $request->user_token;
            //$token->revoke();
            return response()->json(['status_code'=>200,'message'=>'logout succeess']);
        } else {
            return response()->json(['status_code'=>422,'message'=>'logout fail']);
        }
    }

    // Generate OTP Method
    public function generateOTPnum($length) {
        $characters = '0123456789';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

}
