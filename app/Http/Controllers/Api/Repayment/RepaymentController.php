<?php

namespace App\Http\Controllers\Api\Repayment;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RepaymentController extends Controller
{

    //auto get branch id
    public function getBranchID($staffid){
        //dd($request->staffid);
        $obj_branchcode = DB::table('tbl_staff')
            ->join('tbl_branches', 'tbl_branches.id', '=', 'tbl_staff.branch_id')
            ->where('tbl_staff.staff_code',$staffid)
            ->select('tbl_branches.branch_code')
            ->first();
        
        $res = strtolower((string)$obj_branchcode->branch_code); 
        return $res;               
    } 

    // approved Deposit List
    public function getLoanRepaymentActiveList (Request $request)
    {
        $branchid = $request->branchid;
        $staffid = $request->staffid;
        // Branch ID
        $table_branchcode = "";
        $table_branchcode = $this->getBranchID($staffid);

        $data = DB::table($table_branchcode.'_loans')
        ->join('loan_type', 'loan_type.id', '=', $table_branchcode.'_loans.loan_type_id')
        ->join('tbl_group', 'tbl_group.group_uniquekey', '=', $table_branchcode.'_loans.group_id')
        ->join('tbl_center', 'tbl_center.center_uniquekey', '=', $table_branchcode.'_loans.center_id')
        ->join('tbl_client_basic_info', 'tbl_client_basic_info.client_uniquekey', '=', $table_branchcode.'_loans.client_id')
       	->leftjoin('tbl_main_join_loan', 'tbl_main_join_loan.portal_loan_id', '=', $table_branchcode.'_loans.loan_unique_id')
        ->leftjoin('tbl_main_join_client', 'tbl_main_join_client.portal_client_id', '=', $table_branchcode.'_loans.client_id')
       	->leftjoin('tbl_main_join_center', 'tbl_main_join_center.portal_center_id', '=', $table_branchcode.'_loans.center_id')
        ->leftjoin('tbl_main_join_group', 'tbl_main_join_group.portal_group_id', '=', $table_branchcode.'_loans.group_id')
        ->where($table_branchcode.'_loans.loan_officer_id',  $staffid)
        ->where($table_branchcode.'_loans.disbursement_status',  'Activated')
        ->select( $table_branchcode.'_loans.*','loan_type.name as loantype_name','tbl_client_basic_info.*','tbl_group.group_uniquekey','tbl_center.center_uniquekey',
                'tbl_main_join_client.main_client_code','tbl_main_join_loan.main_loan_code','tbl_main_join_center.main_center_code','tbl_main_join_group.main_group_code')
        ->orderBy($table_branchcode.'_loans.loan_unique_id', 'DESC')->get();

        if($data){
            return response()->json(['status_code'=>200,'message'=>'success','data'=>$data]);
        }else{
            return response()->json(['status_code'=>422,'message'=>'fail','data'=>null]);
        }
    }

    // approved Deposit List
    public function getLoanRepaymentActiveListSearch (Request $request)
    {
        $branchid = $request->branchid;
        $staffid = $request->staffid;
    	$searchvalue = $request->searchvalue;
        // Branch ID
        $bcode = "";
        $bcode = $this->getBranchID($staffid);

        $data = DB::table($bcode.'_loans')
        ->join('loan_type', 'loan_type.id', '=', $bcode.'_loans.loan_type_id')
        ->join('tbl_group', 'tbl_group.group_uniquekey', '=', $bcode.'_loans.group_id')
        ->join('tbl_center', 'tbl_center.center_uniquekey', '=', $bcode.'_loans.center_id')
        ->join('tbl_client_basic_info', 'tbl_client_basic_info.client_uniquekey', '=', $bcode.'_loans.client_id')
        ->leftjoin('tbl_main_join_loan', 'tbl_main_join_loan.portal_loan_id', '=', $bcode.'_loans.loan_unique_id')
        ->leftjoin('tbl_main_join_client', 'tbl_main_join_client.portal_client_id', '=', $bcode.'_loans.client_id')
       	->leftjoin('tbl_main_join_center', 'tbl_main_join_center.portal_center_id', '=', $bcode.'_loans.center_id')
        ->leftjoin('tbl_main_join_group', 'tbl_main_join_group.portal_group_id', '=', $bcode.'_loans.group_id')
        ->where($bcode.'_loans.loan_unique_id', 'LIKE', $searchvalue . '%')
        ->orWhere($bcode.'_loans.client_id', 'LIKE', $searchvalue . '%')
        ->orWhere($bcode.'_loans.center_id', 'LIKE', $searchvalue . '%')
        ->orWhere($bcode.'_loans.group_id', 'LIKE', $searchvalue . '%')
        ->orWhere($bcode.'_loans.center_id', 'LIKE', $searchvalue . '%')
        ->orWhere('loan_type.name', 'LIKE', $searchvalue . '%')
        ->orWhere('tbl_main_join_client.portal_client_id', 'LIKE', $searchvalue . '%')
        ->orWhere('tbl_main_join_loan.portal_loan_id', 'LIKE', $searchvalue . '%')
        ->orWhere('tbl_main_join_center.portal_center_id', 'LIKE', $searchvalue . '%')
        ->orWhere('tbl_main_join_group.portal_group_id', 'LIKE', $searchvalue . '%')
        ->where($bcode.'_loans.loan_officer_id',  $staffid)
        ->where($bcode.'_loans.disbursement_status',  'Activated')
        ->select($bcode.'_loans.*','loan_type.name as loantype_name','tbl_client_basic_info.*','tbl_group.group_uniquekey','tbl_center.center_uniquekey',
                'tbl_main_join_client.main_client_code','tbl_main_join_loan.main_loan_code','tbl_main_join_center.main_center_code','tbl_main_join_group.main_group_code')
       	->orderBy($bcode.'_loans.first_installment_date', 'DESC')
        ->get();
   

        if($data){
            return response()->json(['status_code'=>200,'message'=>'success','data'=>$data]);
        }else{
            return response()->json(['status_code'=>422,'message'=>'fail','data'=>null]);
        }
    }

    public function RepaymentSchduleListByLoanID(Request $request)
    {
        $branchid = $request->branchid;
        $staffid = $request->staffid;
        $loan_id = $request->loanid;
        // Branch ID
        $bcode = "";
        $bcode = $this->getBranchID($staffid);

        $loan_data = DB::table($bcode.'_loans')
            ->leftJoin('tbl_client_basic_info', 'tbl_client_basic_info.client_uniquekey', '=', $bcode.'_loans.client_id')
            ->leftJoin('tbl_staff', 'tbl_staff.staff_code', '=', $bcode.'_loans.loan_officer_id')
            ->leftJoin('tbl_branches', 'tbl_branches.id', '=', $bcode.'_loans.branch_id')
            ->leftjoin('tbl_main_join_loan', 'tbl_main_join_loan.portal_loan_id', '=', $bcode.'_loans.loan_unique_id')
            ->leftjoin('tbl_main_join_client', 'tbl_main_join_client.portal_client_id', '=', $bcode.'_loans.client_id')
            ->where($bcode.'_loans.loan_unique_id', '=', $loan_id)
            ->select($bcode.'_loans.*', 'tbl_client_basic_info.*', 'tbl_staff.name as loan_officer_name', 'tbl_branches.branch_name as branch_name',
                    'tbl_main_join_client.main_client_code','tbl_main_join_loan.main_loan_code')
            ->first();

        $loan_schedule = DB::table($bcode.'_loans')
            ->leftJoin($bcode.'_loans_schedule', $bcode.'_loans_schedule.loan_unique_id', '=', $bcode.'_loans.loan_unique_id')
            ->leftJoin('loan_type', 'loan_type.id', '=', $bcode.'_loans.loan_type_id')
            ->where($bcode.'_loans.loan_unique_id', $loan_id)
            ->select($bcode.'_loans_schedule.*', $bcode.'_loans_schedule.id as schedule_id', 'loan_type.loan_term_value')
        	->orderBy($bcode.'_loans_schedule.id', 'asc')
            ->get();

        $count = DB::table($bcode.'_loans')
            ->leftJoin($bcode.'_loans_schedule', $bcode.'_loans_schedule.loan_unique_id', '=', $bcode.'_loans.loan_unique_id')
            ->where($bcode.'_loans.loan_unique_id', $loan_id)
            ->count();
    
   		$loan_schedulePaidId = DB::table($bcode.'_loans')
            ->leftJoin($bcode.'_loans_schedule', $bcode.'_loans_schedule.loan_unique_id', '=', $bcode.'_loans.loan_unique_id')
            ->where($bcode.'_loans.loan_unique_id', $loan_id)
            ->where($bcode.'_loans_schedule.status','paid')
            ->orderBy($bcode.'_loans_schedule.id', 'desc')->first();

    	if($loan_schedulePaidId){
        	$loanschedulePaidId = $loan_schedulePaidId->id;
    	}else{
        	$loanschedulePaidId = 0;
    	}
    
    // Late check
       $late_balance = "";
       foreach($loan_schedule as $dataschedulelist) {
       		if($dataschedulelist->status == "none" && $dataschedulelist->repayment_type == "late"){
            	$late_balance=DB::table($bcode.'_repayment_late')
            		->select(DB::raw('SUM(principal_balance) AS principal_balance'), DB::raw('SUM(interest_balance) AS interest_balance'), DB::raw('SUM(own_balance) AS own_balance'))
            		->where('schedule_id','=',$dataschedulelist->schedule_id)
                	->get();
    		}
                   
       }
    


        if($loan_data &&  $loan_schedule &&  $count){
            return response()->json(['status_code'=>200,'message'=>'success','loan_schedulePaidId'=>$loanschedulePaidId,'loan-data'=>$loan_data,'loan-schedule'=>$loan_schedule,'loan-late'=>$late_balance,'count'=>$count]);
        }else{
            return response()->json(['status_code'=>422,'message'=>'fail','data'=>null]);
        }
    }

    // approved Deposit List
    public function getLoanRepaymentDetailByScheduleID (Request $request)
    {
        $branchid = $request->branchid;
        $staffid = $request->staffid;
     	$loanid = $request->loanid;
     	$schedulid = $request->schedulid;
        // Branch ID
        $bcode = "";
        $bcode = $this->getBranchID($staffid);
    
        $loanschedule = DB::table($bcode.'_loans')
            ->leftJoin($bcode.'_loans_schedule', $bcode.'_loans_schedule.loan_unique_id', '=', $bcode.'_loans.loan_unique_id')
            ->leftJoin('loan_type', 'loan_type.id', '=', $bcode.'_loans.loan_type_id')
            ->where($bcode.'_loans.loan_unique_id', $loanid)
        	->where($bcode.'_loans_schedule.id', $schedulid)
            ->select($bcode.'_loans_schedule.*', $bcode.'_loans_schedule.id as schedule_id', 'loan_type.loan_term_value')
            ->first();
    
         $dataloan = DB::table($bcode.'_loans')
        	->join('loan_type', 'loan_type.id', '=', $bcode.'_loans.loan_type_id')
        	->join('tbl_client_basic_info', 'tbl_client_basic_info.client_uniquekey', '=', $bcode.'_loans.client_id')
       	 	->where($bcode.'_loans.loan_officer_id',  $staffid)
        	->where($bcode.'_loans.loan_unique_id',  $loanid)
        	->where($bcode.'_loans.disbursement_status',  'Activated')
        	->select( 'loan_type.name as loantype_name',
                 'tbl_client_basic_info.client_uniquekey','tbl_client_basic_info.name', 'tbl_client_basic_info.name_mm', 'tbl_client_basic_info.dob',
                 'tbl_client_basic_info.nrc', 'tbl_client_basic_info.old_nrc', 'tbl_client_basic_info.gender',
                 'tbl_client_basic_info.phone_primary')
        	->first();
        // 	
    	// Payment Number
    	$loanpaymentnumber = $this->genPaymentNum($bcode);
        
    	$late_balance = "";
    	if($loanschedule->status == "none" && $loanschedule->repayment_type == "late"){
            $late_balance=DB::table($bcode.'_repayment_late')
            ->select('principal_balance','interest_balance','own_balance')
            ->where('schedule_id','=',$loanschedule->schedule_id)
            ->latest()
            ->first();
    	}

        if($loanschedule){
            return response()->json(['status_code'=>200,'message'=>'success','data-paymentnumber' => $loanpaymentnumber,'data-loaninfo' => $dataloan,'loan-schedule'=>$loanschedule,'loan-late'=>$late_balance]);
        }else{
            return response()->json(['status_code'=>422,'message'=>'fail','data'=>null]);
        }
    }

	public function genPaymentNum($bcode) {
        $bcode = strtoupper($bcode);
        $randomNum = rand(1, 9999);;
        $num = $bcode;
        $num .= '-P-';
        $num .= str_pad($randomNum, 5, 0, STR_PAD_LEFT);
        $num .= date('d');
        return $num;
    }
}
