<?php

namespace App\Http\Controllers\Api\Repayment;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;

class RepaymentLateController extends Controller
{

    //auto get branch id
    public function getBranchID($staffid)
    {
        //dd($request->staffid);
        $obj_branchcode = DB::table('tbl_staff')
            ->join('tbl_branches', 'tbl_branches.id', '=', 'tbl_staff.branch_id')
            ->where('tbl_staff.staff_code', $staffid)
            ->select('tbl_branches.branch_code')
            ->first();

        $res = strtolower((string)$obj_branchcode->branch_code);
        return $res;
    }

    public function createRepaymentLate(Request $request)
    {

        $branchid = $request->branchid;
        $staffid = $request->staffid;
        // Branch ID
        $bcode = "";
        $bcode = $this->getBranchID($staffid);

//         $initial_amount = DB::table($bcode . '_loans')
//             ->leftJoin('loan_type', 'loan_type.id', '=', $bcode . '_loans.loan_type_id')
//             ->where($bcode . '_loans.loan_unique_id', $request->loan_id)
//             ->select($bcode . '_loans.loan_amount', $bcode . '_loans.loan_term_value', 'loan_type.interest_rate')
//             ->get();
//         //dd($initial_amount);

//         $initial_principal = $initial_amount[0]->loan_amount / $initial_amount[0]->loan_term_value;
//         $initial_interest = (($initial_amount[0]->interest_rate / 100) * $initial_principal) / 2;
//         $initial_total_amount = $initial_principal + $initial_interest;
    
     	$initial_amount = DB::table($bcode . '_loans_schedule')
            ->where('id', $request->schedule_id)
            ->select('capital', 'interest')
            ->get();
        // dd($request);

        $initial_principal = $initial_amount[0]->capital;
        $initial_interest = $initial_amount[0]->interest;
        $initial_total_amount = $initial_principal + $initial_interest;
    

        // Late Repayment
        $late_balance = DB::table($bcode . '_repayment_late')
            ->select($bcode . '_repayment_late.principal_balance', $bcode . '_repayment_late.interest_balance', $bcode . '_repayment_late.own_balance')
            ->where($bcode . '_repayment_late.schedule_id', '=', $request->schedule_id)
            ->latest($bcode . '_repayment_late.schedule_id')
            ->get();

        if (count($late_balance) != 0) {
            $latestnum = count($late_balance) - 1;
            $late_balance[$latestnum]->principal_balance;
            $princ_bal = $late_balance[$latestnum]->principal_balance - $request->principal;
            if ($princ_bal < 0) {
                $princ_bal = 0;
            }
            // dd($princ_bal);

            $int_bal = $late_balance[$latestnum]->interest_balance - $request->interest;
            if ($int_bal < 0) {
                $int_bal = 0;
            }

            $total_bal = $late_balance[$latestnum]->own_balance - $request->payment;
            if ($total_bal < 0) {
                $total_bal = 0;
            }
        } else {
            $princ_bal = $initial_principal - $request->principal;
            $int_bal = $initial_interest - $request->interest;
            $total_bal = $initial_total_amount - $request->payment;
        }

        // dd($princ_bal);
        $destinationPath = 'public/loan_repayments_documents/';
        if ($request->document) {
            $document_name = time() . '.' . $request->document->getClientOriginalName();
            $request->document->storeAs($destinationPath, $document_name);
        } else {
            $document_name = NULL;
        }

        DB::table($bcode . '_loans_schedule')
            ->where('id', $request->schedule_id)
            ->update([
                'repayment_type' => "late"
            ]);

        // Account id get
        $accounts = DB::table('account_chart_externals')->where('branch_id', '=', $branchid)->where('external_acc_code', 'LIKE', '1-11' . '%')->get();
        $acc_code = DB::table('account_chart_external_details')->where('account_chart_external_details.external_acc_id', $accounts[0]->id)->first()->id;

        $data = DB::table($bcode . '_repayment_late')->insert([
            'disbursement_id' => $request->disbursement_id,
            'schedule_id' => $request->schedule_id,
            'cash_acc_id' => $acc_code,
            'payment_number' => $request->payment_num,
            'receipt_no' => $request->receipt_no,
            'principal' => $initial_principal,
            'interest' => $initial_interest,
            'penalty' => $request->penalty_amount,
            // 'compulsory_saving' => $request->compulsory_saving,
            'total_amount' => $initial_total_amount,
            'principal_paid' => $request->principal,
            'interest_paid' => $request->interest,
            'total_amount_paid' => $request->payment,
            'principal_balance' => $princ_bal,
            'interest_balance' => $int_bal,
            'own_balance' => $total_bal,
            'over_days' => $request->over_days,
            'documents' => $document_name,
            'payment_method' => $request->payment_method,
            'payment_date' => date("Y-m-d H:i:s", strtotime($request->payment_date)),
        ]);
        if ($data) {

//             //start require data for live repayment
//             $route = $this->getMainUrl37();

//             $route_name = 'create-repayment';
//             $routeurl = $route . $route_name;
//             $authorization = $this->loginToken();

//             $loan_id = DB::table('tbl_main_join_loan')->where('portal_loan_id', $request->disbursement_id)->first()->main_loan_id;
//             $clientPID = DB::table($bcode . '_loans')->where('loan_unique_id', $request->disbursement_id)->first()->client_id;
//             $clientDetail = DB::table('tbl_main_join_client')->where('portal_client_id', $clientPID)->first();
//             $clientMDetail = DB::connection('main')->table('clients')->where('id', $clientDetail->id)->first();
//             //end require data for live repayment

            // Late repayment complete with no outstanding
            if ($request->outstanding == 0) {
                DB::table($bcode . '_loans_schedule')
                    ->where('id', $request->schedule_id)
                    ->update([
                        // 'loans_schedule.interest_balance'=>"0",
                        // 'loans_schedule.total_balance'=>"0",
                        'status' => "paid",
                        'repayment_type' => "late"
                    ]);

                DB::table($bcode . '_repayment_late')
                    ->where('disbursement_id', $request->disbursement_id)
                    ->update([
                        'repayment_status' => "paid",
                    ]);

                if ($request->principal_balance == 0 && $request->outstanding == 0) {
                    DB::table($bcode . '_loans')
                        ->where('loan_unique_id', $request->loan_id)
                        ->update([
                            'disbursement_status' => "closed"
                        ]);
                }

				//live repayment start
            	
            	//****** replace $repayDetail with $request
//                 $route = $this->getMainUrl37();

//                 $route_name = 'create-repayment';
//                 $routeurl = $route . $route_name;
//                 $authorization = $this->loginToken();
//                 $branch_id = DB::table('tbl_staff')->where('staff_code', session()->get('staff_code'))->first()->branch_id;

//                 $loan_id = DB::table('tbl_main_join_loan')->where('portal_loan_id', $repayDetail->loan_id)->first()->main_loan_id;
//                 $clientDetail = DB::table('tbl_main_join_client')->where('portal_client_id', $repayDetail->client_id)->first();
//                 $clientMDetail = DB::connection('main')->table('clients')->where('id', $clientDetail->id)->first();

//                 if (DB::table('tbl_main_join_repayment_schedule')->where('portal_disbursement_schedule_id', $sid)->first()) {
//                     $main_schedule_id = DB::table('tbl_main_join_repayment_schedule')->where('portal_disbursement_schedule_id', $sid)->where('branch_id', $branch_id)->first()->main_disbursement_schedule_id;
//                     // dd($main_schedule_id);

//                     $client = new \GuzzleHttp\Client(['verify' => false]);
//                     $result = $client->post($routeurl, [
//                         'headers' => [
//                             'Content-Type' => 'application/x-www-form-urlencoded',
//                             'Authorization' => 'Bearer ' . $authorization,
//                         ],
//                         'form_params' => [
//                             'disbursement_id' => $loan_id,
//                             'payment_number' => $repayDetail->payment_num,
//                             'disbursement_detail_id' =>  $main_schedule_id,
//                             'receipt_no' => $repayDetail->payment_num,
//                             'client_id' => $clientDetail->main_client_id,
//                             'client_number' => $clientDetail->main_client_code,
//                             'client_name' => $repayDetail->client_name,
//                             'over_days' => $repayDetail->over_days == NULL ? 0 : $repayDetail->over_days,
//                             'penalty_amount' => $repayDetail->penalty_amount == NULL ? 0 : $repayDetail->penalty_amount,
//                             'principle' => $repayDetail->principal / count($repayDetail->schedule_id),
//                             'interest' =>  $repayDetail->interest / count($repayDetail->schedule_id),
//                             'compulsory_saving' => 0,
//                             'owed_balance' => $repayDetail->outstanding,
//                             'principle_balance' => $repayDetail->principal_balance,
//                             'other_payment' => 0,
//                             'total_payment' => $repayDetail->payment,
//                             'cash_acc_id' => $repayDetail->cash_acc_id,
//                             'payment' => $repayDetail->payment,
//                             'payment_date' => $repayDetail->payment_date,
//                             'payment_method' => $repayDetail->payment_method,
//                             'pre_repayment' => $repayDetail->repayment_type == 'pre' ? 1 : 0,
//                             'late_repayment' => $repayDetail->repayment_type == 'late' ? 1 : 0,
//                             'branch_id' => $branch_id,
//                         ]
//                     ]);

//                     if ($repayDetail->repayment_type == 'late' && $repayDetail->outstanding == 0) {
//                         $client = new \GuzzleHttp\Client(['verify' => false]);
//                         $result = $client->post($routeurl, [
//                             'headers' => [
//                                 'Content-Type' => 'application/x-www-form-urlencoded',
//                                 'Authorization' => 'Bearer ' . $authorization,
//                             ],
//                             'form_params' => [
//                                 'disbursement_id' => $loan_id,
//                                 'payment_number' => $repayDetail->payment_num,
//                                 'disbursement_detail_id' =>  $main_schedule_id,
//                                 'receipt_no' => $repayDetail->payment_num,
//                                 'client_id' => $clientDetail->main_client_id,
//                                 'client_number' => $clientDetail->main_client_code,
//                                 'client_name' => $repayDetail->client_name,
//                                 'over_days' => $repayDetail->over_days == NULL ? 0 : $repayDetail->over_days,
//                                 'penalty_amount' => $repayDetail->penalty_amount == NULL ? 0 : $repayDetail->penalty_amount,
//                                 'principle' => 0,
//                                 'interest' =>  0,
//                                 'compulsory_saving' => 0,
//                                 'owed_balance' => 0,
//                                 'principle_balance' => 0,
//                                 'other_payment' => 0,
//                                 'total_payment' => 0,
//                                 'cash_acc_id' => $repayDetail->cash_acc_id,
//                                 'payment' => 0,
//                                 'payment_date' => $repayDetail->payment_date,
//                                 'payment_method' => $repayDetail->payment_method,
//                                 'pre_repayment' => 0,
//                                 'late_repayment' => 0,
//                                 'branch_id' => $branch_id,
//                             ]
//                         ]);

//                         DB::table('tbl_main_join_repayment')->insert([
//                             'portal_repayment_id' => DB::table($bcode . '_repayment_late')->orderBy('repayment_id', 'desc')->first()->repayment_id,
//                             'portal_schedule_id' => $sid,
//                             'main_owed_balance' => $repayDetail->outstanding,
//                             'main_repayment_disbursement_detail_id' => $main_schedule_id,
//                         ]);
//                     } else if ($repayDetail->repayment_type == 'late') {
//                         DB::table('tbl_main_join_repayment')->insert([
//                             'portal_repayment_id' => DB::table($bcode . '_repayment_late')->orderBy('repayment_id', 'desc')->first()->repayment_id,
//                             'portal_schedule_id' => $sid,
//                             'main_owed_balance' => $repayDetail->outstanding,
//                             'main_repayment_disbursement_detail_id' => $main_schedule_id,
//                         ]);
//                     }
//                 }
            
//             	$response = (string) $result->getBody();
//             	$response = json_decode($response);
//             	if ($response->status_code == 200) {
                	
//             	} else {
//                 	response()->json(['status_code' => 200, 'message' => 'fail to upload on core system', 'data' => NULL])
//             	}

                //live repayment end
            }

            if ($request->principal_balance == 0 && $request->outstanding == 0) {
                DB::table($bcode . '_loans')
                    ->where('loan_unique_id', $request->loan_id)
                    ->update([
                        'disbursement_status' => "closed"
                    ]);
            }






            return response()->json(['status_code' => 200, 'message' => 'success', 'data' => $data]);
        } else {
            return response()->json(['status_code' => 422, 'message' => 'fail', 'data' => null]);
        }
    }

    public function getMainUrl37()
    {
        // $url = 'http://172.16.50.101/mis-core/public/api/';
        $url = env('MAIN_URL');
        return $url;
    }

    public function loginToken()
    {
        $route = $this->getMainUrl37() . 'login';
        $headers = [
            'Accept' => 'application/json'
        ];
        $response = Http::withHeaders($headers)->post($route, [
            'username' => 'ict@mis.com',
            'password' => '123456'
        ]);
        return json_decode($response)->data[0]->token;
    }
}
