<?php

namespace App\Http\Controllers\Api\Repayment;

use App\Http\Controllers\Controller;
use App\Models\Loan\LoanDeposit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;


class RepaymentDueController extends Controller
{
    //auto get branch id
    public function getBranchID($staffid){
        //dd($request->staffid);
        $obj_branchcode = DB::table('tbl_staff')
            ->join('tbl_branches', 'tbl_branches.id', '=', 'tbl_staff.branch_id')
            ->where('tbl_staff.staff_code',$staffid)
            ->select('tbl_branches.branch_code')
            ->first();
        
        $res = strtolower((string)$obj_branchcode->branch_code); 
        return $res;               
    } 

    public function createRepaymentDue(Request $request){
        $branchid = $request->branchid;
        $staffid = $request->staffid;
    
    	//  $accounts = DB::table('account_chart_externals')->where('branch_id', '=', $branchid)->where('external_acc_code', 'LIKE', '1-11' . '%')->get();
    	// $acc_code=DB::table('account_chart_external_details')->where('account_chart_external_details.external_acc_id',$accounts[0]->id)->first()->id;
    	//  return response()->json(['status_code'=>200,'message'=>$acc_code]);
     	//$main_acc_code = [$acc_code];
        // Branch ID
        $bcode = "";
        $bcode = $this->getBranchID($staffid);

        $destinationPath = 'public/loan_repayments_documents/';
        if($request->document){
            $document_name = time() . '.' . $request->document->getClientOriginalName();
            $request->document->storeAs($destinationPath, $document_name);
        }else{
            $document_name=NULL;
        }
    
        $accounts = DB::table('account_chart_externals')->where('branch_id', '=', $branchid)->where('external_acc_code', 'LIKE', '1-11' . '%')->get();
    	$acc_code=DB::table('account_chart_external_details')->where('account_chart_external_details.external_acc_id',$accounts[0]->id)->first()->id;

         $data = DB::table($bcode.'_repayment_due')->insert([
                    'schedule_id'=>$request->schedule_id,
                    'disbursement_id' => $request->disbursement_id,
                    'cash_acc_id' =>  $acc_code,
                    'payment_number' => $request->payment_num,
                    'receipt_no' => $request->receipt_no,
                    'principal' => $request->principal,
                    'interest' => $request->interest,
                    'penalty' => $request->penalty_amount,
                    'compulsory_saving' => $request->compulsory_saving,
                    'total_balance' => $request->total_balance,
                    'final_paid_balance' => $request->paid_amount,
                    'own_balance' => $request->owed_balance,
                    'over_days' => $request->over_days,
                    'documents' => $document_name,
                    'payment_method' => $request->payment_method,
                    'repayment_status' => "paid",
                    'payment_date' => date("Y-m-d H:i:s", strtotime($request->payment_date)),

            ]);

        if($data){
            DB::table($bcode.'_loans_schedule')
                ->where('id',$request->schedule_id)
                ->update([
                    'status' => "paid",
                    'repayment_type'=>"due"
            ]);

            if($request->principal_balance == 0) {
                DB::table($bcode.'_loans')
                    ->where('loan_unique_id', $request->loan_id)
                    ->update([
                            'disbursement_status' => "closed"
                ]);
            }
        
        	//live repayment start
        
        	//****************** replace $repayDetail with $request
//         	$route = $this->getMainUrl37();

//         	$route_name = 'create-repayment';
//         	$routeurl = $route . $route_name;
//         	$authorization = $this->loginToken();
//         	$branch_id = DB::table('tbl_staff')->where('staff_code', session()->get('staff_code'))->first()->branch_id;

//         	$loan_id = DB::table('tbl_main_join_loan')->where('portal_loan_id', $repayDetail->loan_id)->first()->main_loan_id;
//         	$clientDetail = DB::table('tbl_main_join_client')->where('portal_client_id', $repayDetail->client_id)->first();
//             $clientMDetail = DB::connection('main')->table('clients')->where('id', $clientDetail->id)->first();

//             if (DB::table('tbl_main_join_repayment_schedule')->where('portal_disbursement_schedule_id', $sid)->first()) {
//                 $main_schedule_id = DB::table('tbl_main_join_repayment_schedule')->where('portal_disbursement_schedule_id', $sid)->where('branch_id', $branch_id)->first()->main_disbursement_schedule_id;

//                 $client = new \GuzzleHttp\Client(['verify' => false]);
//                 $result = $client->post($routeurl, [
//                     'headers' => [
//                         'Content-Type' => 'application/x-www-form-urlencoded',
//                         'Authorization' => 'Bearer ' . $authorization,
//                     ],
//                     'form_params' => [
//                         'disbursement_id' => $loan_id,
//                         'payment_number' => $repayDetail->payment_num,
//                         'disbursement_detail_id' =>  $main_schedule_id,
//                         'receipt_no' => $repayDetail->payment_num,
//                         'client_id' => $clientDetail->main_client_id,
//                         'client_number' => $clientDetail->main_client_code,
//                         'client_name' => $repayDetail->client_name,
//                         'over_days' => $repayDetail->over_days == NULL ? 0 : $repayDetail->over_days,
//                         'penalty_amount' => $repayDetail->penalty_amount == NULL ? 0 : $repayDetail->penalty_amount,
//                         'principle' => $repayDetail->principal / count($repayDetail->schedule_id),
//                         'interest' =>  $repayDetail->interest / count($repayDetail->schedule_id),
//                         'compulsory_saving' => 0,
//                         'owed_balance' => $repayDetail->outstanding,
//                         'principle_balance' => $repayDetail->principal_balance,
//                         'other_payment' => 0,
//                         'total_payment' => $repayDetail->payment,
//                         'cash_acc_id' => $repayDetail->cash_acc_id,
//                         'payment' => $repayDetail->payment,
//                         'payment_date' => $repayDetail->payment_date,
//                         'payment_method' => $repayDetail->payment_method,
//                         'pre_repayment' => 0,
//                         'late_repayment' => 0,
//                         'branch_id' => $branch_id,
//                     ]
//                 ]);
//             }
        
//         	$response = (string) $result->getBody();
//             $response = json_decode($response);
//             if ($response->status_code == 200) {
                
//             } else {
//                 return response()->json(['status_code'=>422,'message'=>'fail on upload to core-system','data'=>null])
//             }
        	//live repayment end
        	
            return response()->json(['status_code'=>200,'message'=>'success','data'=>$data]);
        }else {
            return response()->json(['status_code'=>422,'message'=>'fail','data'=>null]);
        }
    }

	public function getMainUrl37()
    {
        // $url = 'http://172.16.50.101/mis-core/public/api/';
        $url = env('MAIN_URL');
        return $url;
    }

    public function loginToken()
    {
        $route = $this->getMainUrl37() . 'login';
        $headers = [
            'Accept' => 'application/json'
        ];
        $response = Http::withHeaders($headers)->post($route, [
            'username' => 'ict@mis.com',
            'password' => '123456'
        ]);
        return json_decode($response)->data[0]->token;
    }

    
    
}
