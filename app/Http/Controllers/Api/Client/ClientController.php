<?php

namespace App\Http\Controllers\Api\Client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\ClientBasicInfo;
use App\Models\ClientFamilyInfo;
use App\Models\ClientJob;
use App\Models\ClientJoin;
use App\Models\Photo;
use App\Models\SurveyOwnership;

use App\Models\Division;
use App\Models\Districts;
use App\Models\Townships;
use App\Models\Provinces;
use App\Models\Quarters;
use App\Models\NRC;


use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\ImageManagerStatic as Image;

class ClientController extends Controller
{
    // search Client by id
    public function getClientByid (Request $request){
        $clientid = $request->clientid;
        $data = DB::table('tbl_client_basic_info')

        ->join('tbl_client_family_info', 'tbl_client_family_info.client_uniquekey', '=', 'tbl_client_basic_info.client_uniquekey')
        ->join('tbl_client_job_main', 'tbl_client_job_main.client_uniquekey', '=', 'tbl_client_basic_info.client_uniquekey')
        ->join('tbl_client_photo', 'tbl_client_photo.client_uniquekey', '=', 'tbl_client_basic_info.client_uniquekey')
        ->join('tbl_client_join', 'tbl_client_join.client_id', '=', 'tbl_client_basic_info.client_uniquekey')
        ->leftJoin('tbl_group', function($advancedLeftJoin){
            $advancedLeftJoin->on('tbl_client_join.group_id', '=', 'tbl_group.group_uniquekey');
        })
        ->leftJoin('tbl_center', function($advancedLeftJoin){
            $advancedLeftJoin->on('tbl_client_join.center_id', '=', 'tbl_center.center_uniquekey');
        })
        ->leftJoin('tbl_industry_type', function($advancedLeftJoin){
            $advancedLeftJoin->on('tbl_industry_type.id', '=', 'tbl_client_job_main.industry_id');
        })
        ->leftJoin('tbl_job_department', function($advancedLeftJoin){
            $advancedLeftJoin->on('tbl_job_department.id', '=', 'tbl_client_job_main.department_id');
        })
        ->leftJoin('tbl_job_position', function($advancedLeftJoin){
            $advancedLeftJoin->on('tbl_job_position.id', '=', 'tbl_client_job_main.job_position_id');
        })
        ->leftjoin('tbl_main_join_client', 'tbl_main_join_client.portal_client_id', '=', 'tbl_client_basic_info.client_uniquekey')
       	->leftjoin('tbl_main_join_center', 'tbl_main_join_center.portal_center_id', '=', 'tbl_client_join.center_id')
        ->leftjoin('tbl_main_join_group', 'tbl_main_join_group.portal_group_id', '=', 'tbl_client_join.group_id')
        ->where('tbl_client_basic_info.client_uniquekey',$clientid)
        ->select('tbl_client_basic_info.*',
        'tbl_client_family_info.*','tbl_client_job_main.*',
        'tbl_client_photo.*','tbl_client_join.*',
        'tbl_group.group_uniquekey','tbl_center.center_uniquekey',
        'tbl_industry_type.industry_name','tbl_industry_type.industry_name_mm',
        'tbl_job_department.department_name','tbl_job_department.department_name_mm',
        'tbl_job_position.job_position_name','tbl_job_position.job_position_name_mm',
        'tbl_main_join_client.main_client_code','tbl_main_join_center.main_center_code','tbl_main_join_group.main_group_code')
        ->first();


        if($data){
            return response()->json(['status_code'=>200,'message'=>'success','data'=>$data]);
        }else{
            return response()->json(['status_code'=>422,'message'=>'fail','data'=>null]);
        }
    }

    // show ClientList
    public function getClientList (Request $request)
    {
        $branchid = $request->branchid;
        $staffid = $request->staffid;
        $clientstatus = $request->client_status;

        $data = DB::table('tbl_client_basic_info')
        	->leftjoin('tbl_client_family_info', 'tbl_client_family_info.client_uniquekey', '=', 'tbl_client_basic_info.client_uniquekey')
        	->leftjoin('tbl_client_job_main', 'tbl_client_job_main.client_uniquekey', '=', 'tbl_client_basic_info.client_uniquekey')
        	->leftjoin('tbl_client_photo', 'tbl_client_photo.client_uniquekey', '=', 'tbl_client_basic_info.client_uniquekey')
        	->leftjoin('tbl_client_join', 'tbl_client_join.client_id', '=', 'tbl_client_basic_info.client_uniquekey')
            ->leftjoin('tbl_main_join_client', 'tbl_main_join_client.portal_client_id', '=', 'tbl_client_basic_info.client_uniquekey')
        	->leftjoin('tbl_main_join_center', 'tbl_main_join_center.portal_center_id', '=', 'tbl_client_join.center_id')
        	->leftjoin('tbl_main_join_group', 'tbl_main_join_group.portal_group_id', '=', 'tbl_client_join.group_id')
        	//->leftjoin('tbl_main_join_client', 'tbl_main_join_client.portal_client_id', '=', 'tbl_client_basic_info.client_uniquekey')
        // ->leftJoin('tbl_group', function($advancedLeftJoin){
        //     $advancedLeftJoin->on('tbl_client_join.group_id', '=', 'tbl_group.group_uniquekey');
        // })
        // ->leftJoin('tbl_center', function($advancedLeftJoin){
        //     $advancedLeftJoin->on('tbl_client_join.center_id', '=', 'tbl_center.center_uniquekey');
        // })
        	->where('tbl_client_basic_info.client_type', '=',$clientstatus)
        	->where('tbl_client_join.branch_id',$branchid)
        	// ->where('tbl_client_join.staff_id',$staffid)
        	->select('tbl_client_basic_info.id',
        		'tbl_client_basic_info.name',
        		'tbl_client_basic_info.dob',
                'tbl_client_basic_info.nrc',
        		'tbl_client_basic_info.client_uniquekey',
        		'tbl_client_basic_info.address_primary',
        		'tbl_client_basic_info.client_type',
        		'tbl_client_photo.client_photo',
        	    'tbl_client_join.registration_date',
        	    'tbl_client_join.group_id as group_uniquekey',
                'tbl_client_join.center_id as center_uniquekey',
                'tbl_main_join_client.main_client_code','tbl_main_join_center.main_center_code','tbl_main_join_group.main_group_code')
        	->orderBy('tbl_client_join.id', 'DESC')
        ->get();


        if($data){
            return response()->json(['status_code'=>200,'message'=>'success','data'=>$data]);
        }else{
            return response()->json(['status_code'=>422,'message'=>'fail','data'=>null]);
        }
    }

    // show ClientList
    public function getClientListSearch (Request $request)
    {
        $branchid = $request->branchid;
        $staffid = $request->staffid;
    	$searchvalue = $request->searchvalue;

        $data = DB::table('tbl_client_basic_info')
        	->join('tbl_client_family_info', 'tbl_client_family_info.client_uniquekey', '=', 'tbl_client_basic_info.client_uniquekey')
        	->join('tbl_client_job_main', 'tbl_client_job_main.client_uniquekey', '=', 'tbl_client_basic_info.client_uniquekey')
        	->join('tbl_client_photo', 'tbl_client_photo.client_uniquekey', '=', 'tbl_client_basic_info.client_uniquekey')
        	->join('tbl_client_join', 'tbl_client_join.client_id', '=', 'tbl_client_basic_info.client_uniquekey')
            ->leftjoin('tbl_main_join_client', 'tbl_main_join_client.portal_client_id', '=', 'tbl_client_basic_info.client_uniquekey')
        	->leftjoin('tbl_main_join_center', 'tbl_main_join_center.portal_center_id', '=', 'tbl_client_join.center_id')
        	->leftjoin('tbl_main_join_group', 'tbl_main_join_group.portal_group_id', '=', 'tbl_client_join.group_id')
        	->where('tbl_client_basic_info.client_type', 'LIKE', $searchvalue . '%')
        	->orWhere('tbl_client_basic_info.client_uniquekey', 'LIKE', $searchvalue . '%')
        	->orWhere('tbl_client_basic_info.client_type', 'LIKE', $searchvalue . '%')
        	->orWhere('tbl_client_basic_info.nrc', 'LIKE', $searchvalue . '%')
        	->orWhere('tbl_client_basic_info.dob', 'LIKE', $searchvalue . '%')
        	->orWhere('tbl_client_basic_info.phone_primary', 'LIKE', $searchvalue . '%')
        	->orWhere('tbl_main_join_client.main_client_code', 'LIKE', $searchvalue . '%') // search in main 
        	->orWhere('tbl_client_join.center_id', 'LIKE', $searchvalue . '%')
        	->orWhere('tbl_client_join.group_id', 'LIKE', $searchvalue . '%')
        	->orWhere('tbl_client_join.client_status', 'LIKE', $searchvalue . '%')
        	->where('tbl_client_join.branch_id',$branchid)
        	->where('tbl_client_join.staff_id',$staffid)
        	->select('tbl_client_basic_info.id','tbl_client_basic_info.name',
        		'tbl_client_basic_info.dob',
        		'tbl_client_basic_info.client_uniquekey',
        		'tbl_client_basic_info.address_primary',
        		'tbl_client_basic_info.client_type',
        		'tbl_client_photo.client_photo',
        		'tbl_client_join.registration_date',
        		'tbl_client_join.group_id as group_uniquekey','tbl_client_join.center_id as center_uniquekey',
                'tbl_main_join_client.main_client_code','tbl_main_join_center.main_center_code','tbl_main_join_group.main_group_code')
        	->orderBy('tbl_client_join.id', 'DESC')
        	->get();

        if($data){
            return response()->json(['status_code'=>200,'message'=>'success','data'=>$data]);
        }else{
            return response()->json(['status_code'=>422,'message'=>'fail','data'=>null]);
        }
    }

    // Storage new client
    public function createClientTest (Request $request)  
    {
        //$clientuniid = $this->generateClientID();
        //$this->converttoImage($clientuniid,$request->client_photo);

        //$strdata = $request->data;
        //$strdata_rep = str_replace(' ', '', $strdata);
        //$strdata_result = explode(',',$strdata_rep);
        //foreach($strdata_result as $key) {    
        //    echo '"'.$key.'"<br/>';    
        //}
        //return response()->json(['status_code'=>422,'message'=>'Tesing','data'=> $strdata_result]);
    }
    //decode base64 string
    public function converttoImage($dataclientid,$dataimage)
    {
        $image = str_replace('data:image/png;base64,', '', $dataimage);
        $image_name = uniqid().'_'.$dataclientid.'_'.time().'.jpg';
    
        $disk = Storage::disk('clients-photo')->put($image_name, base64_decode($image));
        return $image_name;
    }

    //auto generate client uniquekey
    public function generateClientID($staffid)
    {
        $obj_branchcode = DB::table('tbl_staff')
                            ->join('tbl_branches', 'tbl_branches.id', '=', 'tbl_staff.branch_id')
                            ->where('tbl_staff.staff_code',$staffid)
                            ->select('tbl_branches.branch_code')
                            ->get();
                           
        if (count(array($obj_branchcode)) > 0) {
            $subcode = substr($obj_branchcode[0]->branch_code,0,2); //split branch code
            // get client_uniquekey in tbl_client_basic_info
            $obj_fund = DB::table('tbl_client_basic_info')->where('client_uniquekey', 'LIKE', $subcode.'%')->get()->last();
            if($obj_fund != null){
                if (count(array($obj_fund)) > 0) {
                    $subnum = substr($obj_fund->client_uniquekey, 6); // sub-string to 01-000001
                    $addnum = $subnum+1; // add id in +1
                    $cusmclientidnum =  (string)str_pad($addnum, 7, '0', STR_PAD_LEFT); // add generate in zero
                    $clientid = $subcode; // Branch Code
                    $clientid .= '-';
                    $clientid .= '01';
                    $clientid .= '-';
                    $clientid .= $cusmclientidnum; // Client ID in branch
                    return $clientid;
                }else{
                    return response()->json(['status_code'=>500,'message'=>'client create fail','data'=>null]);
                }
            }else{
                $subcode = substr($obj_branchcode[0]->branch_code,0,2); //split branch code
                return $clientid = $subcode.'-01-0000001';
            }
           
        } else {
            $subcode = substr($obj_branchcode[0]->branch_code,0,2); //split branch code
            return $clientid = $subcode.'-01-0000001';
        }
    }
    // Storage new client
    public function createClient (Request $request)
    {
        // generate client id
        $staffid = $request->staffid;
        $clientuniid = $this->generateClientID($staffid);
        //dd( $clientuniid);
        if($request->education){
            $obj_education = DB::table('tbl_educations')->where('education_name', 'LIKE', $request->education.'%')->first();
        }

        if($request->industry_id){
            $obj_industry = DB::table('tbl_industry_type')->where('industry_name', 'LIKE', $request->industry_id.'%')->first();
        }
        
        if($request->department_id){
            $obj_department = DB::table('tbl_job_department')->where('department_name', 'LIKE', $request->department_id.'%')->first();
        }
       
        if($request->job_position_id){
            $obj_job_position = DB::table('tbl_job_position')->where('job_position_name', 'LIKE', $request->job_position_id.'%')->first();
        }

        
        // Add data to database
        ClientBasicInfo::insert([
            'client_uniquekey' =>  $clientuniid,
            'name' => $request->name_en ?? '', 
            'name_mm' => $request->name_mm ?? '', 
            'dob' => $request->dob ?? '', 
            'nrc' => $request->nrc_number ?? '', 
            'old_nrc' => $request->nrc_number ?? '', 
            'nrc_card_id' => "", 
            'gender' => $request->gender ?? '', 
            'phone_primary' => $request->primary_phone ?? '', 
            'phone_secondary' => $request->secondary_phone ?? '', 
            'email' => "", 
            'blood_type' => $request->blood_type ?? '', 
            'religion' => $request->religion ?? '', 
            'nationality' => $request->nationality ?? '', 
            'education_id' => $obj_education->id ?? '',
            'other_education' => "", 
            'village_id' => $request->village_id ?? '', 
            'province_id' => $request->province_id ?? '', 
            'quarter_id' => $request->quarter_id ?? '', 
            'township_id' => $request->blood_type ?? '', 
            'district_id' => $request->district_id ?? '', 
            'division_id' => $request->division_id ?? '', 
            'city_id' => "" ?? '', 
            'address_primary' => $request->address1 ?? '', 
            'address_secondary' => $request->address2 ?? '',
            'client_type' => 'new', 
            'status' => 'active',
        ]);

        ClientFamilyInfo::insert([
            'client_uniquekey' => $clientuniid,
            'father_name' => $request->father_name ?? '',
            'marital_status' => $request->marital_status ?? 'None',
            'spouse_name' => $request->spouse_name ?? '',
            'occupation_of_spouse' => $request->occupation_of_spouse ?? '',
            'no_of_family' => $request->no_of_family ?? '',
            'no_of_working_family' => $request->no_of_working_family ?? '',
            'no_of_children' => $request->no_of_children ?? '',
            'no_of_working_progeny' => $request->no_of_working_progeny ?? '',
        ]);

        ClientJob::insert([
            'client_uniquekey' => $clientuniid, 
            'current_business' => $request->current_business,
            'business_income' => $request->current_income,
            'job_status' => $request->job_status,
            'industry_id' => $obj_industry->id,
            'department_id' => $obj_department->id,
            'job_position_id' => $obj_job_position->id, 
            'experience' => $request->experience,
            'salary' => $request->salary, 
            'company_name' => $request->company_name, 
            'company_phone' => $request->company_phone,
            'company_address' => $request->company_address,
        ]);



        SurveyOwnership::insert([
            'client_uniquekey' => $clientuniid,
            //'land' => $request->land,
            //'ownership' => $request->ownership,
            'assetstype_removable' => $request->assetstype_removable,
            'assetstype_unremovable' => $request->assetstype_unremovable,
            'purchase_price' => $request->purchase_price,
            'current_value' => $request->current_value,
            'quantity' => $request->quantity,
            //'attach_1' => $this->converttoImage($clientuniid,$request->attach_1), 
            //'attach_2' => $this->converttoImage($clientuniid,$request->attach_2),
            //'attach_3' => $this->converttoImage($clientuniid,$request->attach_3),
            //'attach_4' => $this->converttoImage($clientuniid,$request->attach_4),
        ]);

        Photo::insert([
            'client_uniquekey'=> $clientuniid, 
            'client_photo' => $this->converttoImage($clientuniid,$request->photo_of_client) , 
            'recognition_front' => $this->converttoImage($clientuniid,$request->nrc_photo_front), 
            'recognition_back' => $this->converttoImage($clientuniid,$request->nrc_photo_back), 
            'recognition_status' => $request->recognition_status, 
            'registration_family_form_front' => $this->converttoImage($clientuniid,$request->registration_family_form_front),
            'registration_family_form_back' => $this->converttoImage($clientuniid,$request->registration_family_form_back), 
            'finger_print_bio' => 'null', 
            'government_recommendations_photo' => 'null',
        ]);

        $entery = ClientJoin::insert([
            'client_id' => $clientuniid,
            'branch_id' => $request->join_branch_id,
            'staff_id' => $request->join_staff_id,
            'guarantors_id' => $request->join_guarantors_id,
            'group_id' => $request->join_group_id,
            'center_id' => $request->join_center_id,
            'loan_id' => '00',
            'deposit_id' => '00',
            'disbursement_id' => '00',
            'repayment_id' => '00',
            'saving_withdrawal_id' => '00',
            'registration_date' => date('Y-m-d H:i:s', strtotime($request->join_register_date)),
            'client_status' => $request->join_client_status,
        ]);

        if($entery){
            return response()->json(['status_code'=>200,'message'=>'success to create new client','data'=>$entery]);
        }else{
            return response()->json(['status_code'=>422,'message'=>'fail to create new client','data'=>null]);
        }
    }


    // Client ID Generate
    public function testapi()
    {
        $query = DB::table('tbl_staff')
        ->join('tbl_branches', 'tbl_branches.id', '=', 'tbl_staff.branch_id')
        ->where('tbl_staff.staff_code',"LMD-1456")
        ->select('tbl_branches.branch_code')
        ->get();
        //dd($query[0]->branch_code);
        $obj_fund = DB::table('tbl_client_basic_info')->where('client_uniquekey', 'LIKE', '%'. $query[0]->branch_code.'%')->get()->last();
        ///dd($obj_fund->client_uniquekey);
        $subnum = substr($obj_fund->client_uniquekey, 4);
        //dd($subnum);
        $addnum = $subnum+1;
        //dd($addnum);
        $cusmclientidnum =  (string)str_pad($addnum, 7, '0', STR_PAD_LEFT);
        //dd($cusmclientidnum);
        $clientid = $query[0]->branch_code; // Branch Code
        $clientid .= '-';
        $clientid .= '01';
        $clientid .= '-';
        $clientid .= $cusmclientidnum; // Client ID in branch
        return response()->json(['status_code'=>1000,'message'=>'Test data','data'=>$clientid]);
    }

}
