<?php

namespace App\Http\Controllers\Api\Client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Guarantor;
use Illuminate\Support\Facades\DB;

class GuarantorController extends Controller
{
    public function getGuarantorSearch (Request $request){
    
        $branchid = $request->branchid;
        $staffid = $request->staffid;
    	$searchvalue = $request->searchvalue;
    
        $data =DB::table('tbl_guarantors')
        	->leftjoin('tbl_main_join_guarantor', 'tbl_main_join_guarantor.portal_guarantor_id', '=', 'tbl_guarantors.guarantor_uniquekey')
        	->where('tbl_guarantors.name', 'LIKE', $searchvalue . '%')
            ->orWhere('tbl_guarantors.name_mm', 'LIKE', $searchvalue . '%')
        	->orWhere('tbl_guarantors.guarantor_uniquekey', 'LIKE', $searchvalue . '%')
            ->orWhere('tbl_guarantors.nrc', 'LIKE', $searchvalue . '%')
            ->orWhere('tbl_guarantors.phone_primary', 'LIKE', $searchvalue . '%')
            ->orWhere('tbl_main_join_guarantor.main_guarantor_code', 'LIKE', $searchvalue . '%') // main in search
            ->orderBy('tbl_guarantors.id', 'DESC')
        	->get();

        if($data){
            return response()->json(['status_code'=>200,'message'=>'success','data'=>$data]);
        }else{
            return response()->json(['status_code'=>422,'message'=>'fail']);
        }
    }
    
    
   public function getGuarantorList (Request $request){
   
        $branchid = $request->branchid;
        $staffid = $request->staffid;
   		$status = $request->status;
   
        $data =  DB::table('tbl_guarantors')
        	->leftjoin('tbl_main_join_guarantor', 'tbl_main_join_guarantor.portal_guarantor_id', '=', 'tbl_guarantors.guarantor_uniquekey')
        	->where('tbl_guarantors.branch_id',$branchid)
       		->orderBy('tbl_guarantors.id', 'DESC')
        	->get();
        if($data){
            return response()->json(['status_code'=>200,'message'=>'success','data'=>$data]);
        }else{
            return response()->json(['status_code'=>422,'message'=>'fail']);
        }
    }
}
