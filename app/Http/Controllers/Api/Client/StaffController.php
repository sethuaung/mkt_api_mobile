<?php

namespace App\Http\Controllers\Api\Client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Staff;
use Illuminate\Support\Facades\DB;

class StaffController extends Controller
{
    //auto get branch id
    public function getBranchID($staffid){
        //dd($request->staffid);
        $obj_branchcode = DB::table('tbl_staff')
            ->join('tbl_branches', 'tbl_branches.id', '=', 'tbl_staff.branch_id')
            ->where('tbl_staff.staff_code',$staffid)
            ->select('tbl_branches.branch_code')
            ->first();
        
        $res = strtolower((string)$obj_branchcode->branch_code); 
        return $res;               
    } 

    public function getList(Request $request)
    {
       
        $staffs = Staff::all();
        return response()->json(['status_code'=>200,'message'=>'New User created successful','data'=> $staffs]);
    }

    public function getDetail(Request $request)
    {
    	$branchid = $request->branchid;
        $staffid = $request->staffid;
        // Branch ID
        $bcode = "";
        $bcode = $this->getBranchID($staffid);
    
        $staffs = Staff::where('staff_code',$staffid)
         	->join('tbl_branches', 'tbl_branches.id', '=', 'tbl_staff.branch_id')
            ->where('tbl_staff.staff_code',$staffid)
            ->select('tbl_staff.*','tbl_branches.branch_name')
        	->get();
    
    	//Pending','Approved','Deposited','Declined','Withdrawn','Written-Off','Closed','Activated','Canceled')
    	$count_client = DB::table('tbl_client_join')->where('staff_id',$staffid)->where('branch_id',$branchid)->count();
    	$count_loan = DB::table($bcode.'_loans')->where('loan_officer_id',$staffid)->count();
    	$count_pending = DB::table($bcode.'_loans')->where('loan_officer_id',$staffid)->where('disbursement_status','Pending')->count();
    	$count_deposit = DB::table($bcode.'_loans')->where('loan_officer_id',$staffid)->where('disbursement_status','Approved')->count();
    	$count_disbusement = DB::table($bcode.'_loans')->where('loan_officer_id',$staffid)->where('disbursement_status','Deposited')->count();
    	$count_repayment = DB::table($bcode.'_loans')->where('loan_officer_id',$staffid)->where('disbursement_status','Activated')->count();
    	$count_saving = DB::table($bcode.'_saving_withdrawl')
        	->join($bcode.'_loans', $bcode.'_loans.loan_unique_id', '=', $bcode.'_saving_withdrawl.loan_unique_id')
            ->where($bcode.'_loans.loan_officer_id',$staffid)
        	->count();
    
     	if ($staffs) {
            return response()->json(['status_code'=>200,'message'=>'data found',
                                     'count_client'=>$count_client,
                                     'count_loan'=> $count_loan,
                                     'count_repayment'=> $count_repayment,
                                     'count_deposit'=> $count_deposit,
                                     'count_dusbursement'=> $count_disbusement,
                                     'count_saving'=> $count_saving,
                                     'count_pending'=> $count_pending,
                                     'data'=>$staffs]);
        }else{
            return response()->json(['status_code'=>422,'message'=>'data not found']);
        }
    }


    public function getAppInfoWithStaff(Request $request)
    {
    	$branchid = $request->branchid;
        $staffid = $request->staffid;
        // Branch ID
        $bcode = "";
        $bcode = $this->getBranchID($staffid);
    
        // counter
        $count_client = DB::table('tbl_client_join')->where('staff_id',$staffid)->where('branch_id',$branchid)->count();
    	$count_loan = DB::table($bcode.'_loans')->where('loan_officer_id',$staffid)->count();
    	//$count_pending = DB::table($bcode.'_loans')->where('loan_officer_id',$staffid)->where('disbursement_status','Pending')->count();
    	$count_deposit = DB::table($bcode.'_loans')->where('loan_officer_id',$staffid)->where('disbursement_status','Approved')->count();
    	$count_disbusement = DB::table($bcode.'_loans')->where('loan_officer_id',$staffid)->where('disbursement_status','Deposited')->count();
    	$count_repayment = DB::table($bcode.'_loans')->where('loan_officer_id',$staffid)->where('disbursement_status','Activated')->count();
    	$count_saving = DB::table($bcode.'_saving_withdrawl')->count();
    	
    	// today collect amount
        // $count_loan_today_collect = DB::table($bcode.'_loans_schedule')
        // 	->leftjoin($bcode.'_loans', $bcode.'_loans.loan_unique_id', '=', $bcode.'_loans_schedule.loan_unique_id')
        // 	->where($bcode.'_loans.loan_officer_id', $staffid)
        // 	->where($bcode.'_loans_schedule.repayment_date',DB::raw('CURDATE()'))->count();
     	$count_loan_today_collect = 999999996;
     	
    	// user permissions or apk update
//     	$obj_staff = DB::table('tbl_staff')->where('del_status','active')->where('staff_code',$staffid)->first();
//     	if($obj_staff){
//             $app_message = "Welcome to MKT";
//     		$app_store = "http://mkt.com.mm/staff-apk";
//        	 	$app_status = 1;
//     	}else{    	
//         	// User permissions close
//         	$app_message = "Please contact our team \nYour user permissions is temporarily disabled";
//     		$app_store = "http://mkt.com.mm/staff-apk";
//         	$app_status = 0;
        
//         	// App Update
//         	$app_message = "App update available.\nplease check in your website";
//     		$app_store = "http://mkt.com.mm/staff-apk";
//         	$app_status = 2;
//         }
		
    	$app_status = 1;
    	$app_message = "Please contact our team \nYour user permissions is temporarily disabled";
    	$app_store = "http://mkt.com.mm/staff-apk";
    
     	if ($bcode) {
            return response()->json(['status_code'=>200,'message'=>'data found',
                                     'count_loan_collect'=> $count_loan_today_collect,
                                     'count_client'=>$count_client,
                                     'count_loan'=> $count_loan,
                                     'count_repayment'=> $count_repayment,
                                     'count_deposit'=> $count_deposit,
                                     'count_dusbursement'=> $count_disbusement,
                                     'count_saving'=> $count_saving,
                                     'app_status'=> $app_status,
                                     'app_message'=> $app_message,
                                     'app_store'=> $app_store]);
        }else{
            return response()->json(['status_code'=>422,'message'=>'data not found']);
        }
    }


}
