<?php

namespace App\Http\Controllers\Api\Client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Center;
use App\Models\Group;

class CenterController extends Controller
{
    public function getCenterByid (Request $request){
        $staff_client_id = $request->staff_client_id;
        $data = Center::select('center_uniquekey')
        ->where("staff_client_id","LIKE","%{$staff_client_id}%")->get();
        if($data){
            return response()->json(['status_code'=>200,'message'=>'success','data'=>$data]);
        }else{
            return response()->json(['status_code'=>422,'message'=>'fail']);
        }
    }

    public function getGroupbyid (Request $request){
        $staffid = $request->staffid;
        $data = Group::select('group_uniquekey')
        ->where("staff_client_id","LIKE","%{$staffid}%")->get();
        if($data){
            return response()->json(['status_code'=>200,'message'=>'success','data'=>$data]);
        }else{
            return response()->json(['status_code'=>422,'message'=>'fail','data'=>null]);
        }
    }
}
