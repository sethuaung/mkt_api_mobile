<?php

namespace App\Http\Controllers\Api\Loan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LoanDisbursementController extends Controller
{
        //auto get branch id
    public function getBranchID($staffid){
        //dd($request->staffid);
        $obj_branchcode = DB::table('tbl_staff')
            ->join('tbl_branches', 'tbl_branches.id', '=', 'tbl_staff.branch_id')
            ->where('tbl_staff.staff_code',$staffid)
            ->select('tbl_branches.branch_code')
            ->first();
        
        $res = strtolower((string)$obj_branchcode->branch_code); 
        return $res;               
    } 

    // approved Disbursement List
    public function getLoanDepositApprovedList (Request $request)
    {
        $branchid = $request->branchid;
        $staffid = $request->staffid;
        // Branch ID
        $table_branchcode = "";
        $table_branchcode = $this->getBranchID($staffid);

        $data = DB::table($table_branchcode.'_loans')
        ->join('loan_type', 'loan_type.id', '=', $table_branchcode.'_loans.loan_type_id')
        ->join('tbl_client_basic_info', 'tbl_client_basic_info.client_uniquekey', '=', $table_branchcode.'_loans.client_id')
        ->join('tbl_group', 'tbl_group.group_uniquekey', '=', $table_branchcode.'_loans.group_id')
        ->join('tbl_center', 'tbl_center.center_uniquekey', '=', $table_branchcode.'_loans.center_id')
        ->leftjoin('tbl_main_join_loan', 'tbl_main_join_loan.portal_loan_id', '=', $table_branchcode.'_loans.loan_unique_id')
        ->leftjoin('tbl_main_join_client', 'tbl_main_join_client.portal_client_id', '=', $table_branchcode.'_loans.client_id')
       	->leftjoin('tbl_main_join_center', 'tbl_main_join_center.portal_center_id', '=', $table_branchcode.'_loans.center_id')
        ->leftjoin('tbl_main_join_group', 'tbl_main_join_group.portal_group_id', '=', $table_branchcode.'_loans.group_id')
        ->leftjoin('tbl_main_join_guarantor', 'tbl_main_join_guarantor.portal_guarantor_id', '=', $table_branchcode.'_loans.guarantor_a')
        ->where($table_branchcode.'_loans.loan_officer_id',  $staffid)
        ->where($table_branchcode.'_loans.disbursement_status',  'Deposited')
        ->select( $table_branchcode.'_loans.*','loan_type.name as loantype_name','tbl_client_basic_info.*','tbl_group.group_uniquekey','tbl_center.center_uniquekey',
                'tbl_main_join_guarantor.main_guarantor_code as main_guarantor_cod_a',
                 'tbl_main_join_client.main_client_code','tbl_main_join_loan.main_loan_code','tbl_main_join_center.main_center_code','tbl_main_join_group.main_group_code')
        ->orderBy($table_branchcode.'_loans.loan_application_date', 'DESC')->get();
    
    

        if($data){
            return response()->json(['status_code'=>200,'message'=>'success','data'=>$data]);
        }else{
            return response()->json(['status_code'=>422,'message'=>'fail','data'=>null]);
        }
    }
    // active Disbursement List
    public function getLoanDisbursemenActivetList (Request $request)
    {
        $branchid = $request->branchid;
        $staffid = $request->staffid;
        // Branch ID
        $table_branchcode = "";
        $table_branchcode = $this->getBranchID($staffid);

        $data = DB::table($table_branchcode.'_disbursement')
        ->join('loan_type', 'loan_type.id', '=', $table_branchcode.'_disbursement.loan_product_id')
        ->join($table_branchcode.'_loans', $table_branchcode.'_loans.id', '=', $table_branchcode.'_disbursement.loan_id')
        ->join('tbl_client_basic_info', 'tbl_client_basic_info.id', '=', $table_branchcode.'_disbursement.client_id')
        ->leftjoin('tbl_main_join_loan', 'tbl_main_join_loan.portal_loan_id', '=', $table_branchcode.'_loans.loan_unique_id')
        ->leftjoin('tbl_main_join_client', 'tbl_main_join_client.portal_client_id', '=', $table_branchcode.'_loans.client_id')
       	->leftjoin('tbl_main_join_center', 'tbl_main_join_center.portal_center_id', '=', $table_branchcode.'_loans.center_id')
        ->leftjoin('tbl_main_join_group', 'tbl_main_join_group.portal_group_id', '=', $table_branchcode.'_loans.group_id')
        ->leftjoin('tbl_main_join_guarantor', 'tbl_main_join_guarantor.portal_guarantor_id', '=', $table_branchcode.'_loans.guarantor_a')
        ->where($table_branchcode.'_disbursement.loan_officer_id',  $staffid)
        ->select( $table_branchcode.'_disbursement.*',$table_branchcode.'_loans.*','loan_type.name as loantype_name','tbl_client_basic_info.*',
                'tbl_main_join_guarantor.main_guarantor_code as main_guarantor_cod_a',
                 'tbl_main_join_client.main_client_code','tbl_main_join_loan.main_loan_code','tbl_main_join_center.main_center_code','tbl_main_join_group.main_group_code')
        ->orderBy($table_branchcode.'_disbursement.processing_date', 'DESC')->get();
    


        if($data){
            return response()->json(['status_code'=>200,'message'=>'success','data'=>$data]);
        }else{
            return response()->json(['status_code'=>422,'message'=>'fail','data'=>null]);
        }
    }

    // search Disbursement List
    public function getLoanDisbursementListSearch (Request $request)
    {
        $branchid = $request->branchid;
        $staffid = $request->staffid;
        $searchvalue = $request->searchvalue;
        // Branch ID
        $table_branchcode = "";
        $table_branchcode = $this->getBranchID($staffid);
    
		$data = DB::table($table_branchcode.'_loans')
        ->leftjoin('tbl_client_basic_info', 'tbl_client_basic_info.client_uniquekey', '=', $table_branchcode.'_loans.client_id')
        ->leftjoin('tbl_staff', 'tbl_staff.staff_code', '=', $table_branchcode.'_loans.loan_officer_id')
        ->leftjoin('tbl_guarantors', 'tbl_guarantors.guarantor_uniquekey', '=', $table_branchcode.'_loans.guarantor_a')
        ->leftjoin('loan_type', 'loan_type.id', '=', $table_branchcode.'_loans.loan_type_id')
        ->join('tbl_group', 'tbl_group.group_uniquekey', '=', $table_branchcode.'_loans.group_id')
        ->join('tbl_center', 'tbl_center.center_uniquekey', '=', $table_branchcode.'_loans.center_id')
        ->leftjoin('tbl_main_join_loan', 'tbl_main_join_loan.portal_loan_id', '=', $table_branchcode.'_loans.loan_unique_id')
        ->leftjoin('tbl_main_join_client', 'tbl_main_join_client.portal_client_id', '=', $table_branchcode.'_loans.client_id')
       	->leftjoin('tbl_main_join_center', 'tbl_main_join_center.portal_center_id', '=', $table_branchcode.'_loans.center_id')
        ->leftjoin('tbl_main_join_group', 'tbl_main_join_group.portal_group_id', '=', $table_branchcode.'_loans.group_id')
        ->leftjoin('tbl_main_join_guarantor', 'tbl_main_join_guarantor.portal_guarantor_id', '=', $table_branchcode.'_loans.guarantor_a')
        ->where($table_branchcode.'_loans.loan_unique_id', 'LIKE', $searchvalue . '%')
        ->orWhere($table_branchcode.'_loans.disbursement_status', 'LIKE', $searchvalue . '%')
        ->orWhere($table_branchcode.'_loans.loan_officer_id', 'LIKE', $searchvalue . '%')
        ->orWhere('tbl_staff.name', 'LIKE', $searchvalue . '%')
        ->orWhere('tbl_client_basic_info.client_uniquekey', 'LIKE', $searchvalue . '%')
        ->orWhere('tbl_client_basic_info.name', 'LIKE', $searchvalue . '%')
        ->orWhere('tbl_client_basic_info.name_mm', 'LIKE', $searchvalue . '%')
        ->orWhere('tbl_client_basic_info.nrc', 'LIKE', $searchvalue . '%')
        ->orWhere('tbl_client_basic_info.old_nrc', 'LIKE', $searchvalue . '%')
        ->orWhere('tbl_client_basic_info.nrc_card_id', 'LIKE', $searchvalue . '%')
        ->orWhere('tbl_client_basic_info.phone_primary', 'LIKE', $searchvalue . '%')
        ->orWhere('tbl_client_basic_info.phone_secondary', 'LIKE', $searchvalue . '%')
        ->orWhere('tbl_guarantors.name', 'LIKE', $searchvalue . '%')
        ->orWhere('loan_type.name', 'LIKE', $searchvalue . '%')
        
        ->orWhere('tbl_client_basic_info.dob', 'LIKE', $searchvalue . '%')
        ->orWhere('tbl_client_basic_info.phone_secondary', 'LIKE', $searchvalue . '%')
        ->orWhere('tbl_main_join_client.main_client_code', 'LIKE', $searchvalue . '%') // main in search
        ->orWhere('tbl_main_join_loan.main_loan_id', 'LIKE', $searchvalue . '%') // main in search
        ->orWhere('tbl_main_join_center.main_center_code', 'LIKE', $searchvalue . '%') // main in search
        ->orWhere('tbl_main_join_group.main_group_code', 'LIKE', $searchvalue . '%') // main in search
        ->orWhere('tbl_main_join_guarantor.main_guarantor_code', 'LIKE', $searchvalue . '%') // main in search
        
        ->orWhere($table_branchcode.'_loans.center_id', 'LIKE', $searchvalue . '%')
        ->orWhere($table_branchcode.'_loans.group_id', 'LIKE', $searchvalue . '%')
        ->where($table_branchcode.'_loans.loan_officer_id',  $staffid)
        ->where($table_branchcode.'_loans.disbursement_status','=',  'Deposited')

        ->select( $table_branchcode.'_loans.*','loan_type.name as loantype_name','tbl_client_basic_info.*','tbl_group.group_uniquekey','tbl_center.center_uniquekey',
                'tbl_main_join_guarantor.main_guarantor_code as main_guarantor_cod_a',
                 'tbl_main_join_client.main_client_code','tbl_main_join_loan.main_loan_code','tbl_main_join_center.main_center_code','tbl_main_join_group.main_group_code')

        ->orderBy($table_branchcode.'_loans.loan_unique_id', 'DESC')
        ->get();
    	// $data = DB::table($table_branchcode.'_loans')
    	// ->join('loan_type', 'loan_type.id', '=', $table_branchcode.'_loans.loan_type_id')
    	// ->join('tbl_group', 'tbl_group.group_uniquekey', '=', $table_branchcode.'_loans.group_id')
    	// ->join('tbl_center', 'tbl_center.center_uniquekey', '=', $table_branchcode.'_loans.center_id')
    	// ->join('tbl_client_basic_info', 'tbl_client_basic_info.client_uniquekey', '=', $table_branchcode.'_loans.client_id')
    	// ->where($table_branchcode.'_loans.loan_officer_id',  $staffid)
    	// ->where($table_branchcode.'_loans.disbursement_status',  'Deposited')
    	// ->where($table_branchcode.'_loans.loan_unique_id', 'LIKE', '%' .$searchvalue . '%')
    	// ->orWhere('tbl_client_basic_info.name', 'LIKE', $searchvalue . '%')
    	// ->orWhere('tbl_client_basic_info.client_uniquekey', 'LIKE', $searchvalue . '%')
    	// ->select( $table_branchcode.'_loans.*','loan_type.name as loantype_name','tbl_client_basic_info.*','tbl_group.group_uniquekey','tbl_center.center_uniquekey')
    	// ->orderBy($table_branchcode.'_loans.loan_application_date', 'DESC')->get();
    
        // $data = DB::table($table_branchcode.'_disbursement')
        // ->join('loan_type', 'loan_type.id', '=', $table_branchcode.'_disbursement.loan_product_id')
        // ->join($table_branchcode.'_loans', $table_branchcode.'_loans.loan_unique_id', '=', $table_branchcode.'_disbursement.loan_id')
        // ->join('tbl_client_basic_info', 'tbl_client_basic_info.client_uniquekey', '=', $table_branchcode.'_disbursement.client_id')
        // ->where($table_branchcode.'_loans.loan_officer_id',  $staffid)
        // ->where($table_branchcode.'_loans.loan_unique_id', 'LIKE', '%' .$searchvalue . '%')
        // ->orWhere('tbl_client_basic_info.name', 'LIKE', $searchvalue . '%')
        // ->orWhere('tbl_client_basic_info.client_uniquekey', 'LIKE', $searchvalue . '%')
        // ->select( $table_branchcode.'_disbursement.*','loan_type.name as loantype_name','tbl_client_basic_info.*')
        // ->orderBy($table_branchcode.'_disbursement.processing_date', 'DESC')->get();

        if($data){
            return response()->json(['status_code'=>200,'message'=>'success','data'=>$data]);
        }else{
            return response()->json(['status_code'=>422,'message'=>'result not found','data'=>null]);
        }
    }

    public function createDisbursement(Request $request)
    {
      	$branchid = $request->branchid;
        $staffid = $request->staffid;
    

        $loanid = $request->loan_id;
        $cashaccid = $request->cash_acc_id;
        $disbursedamount = $request->disbursed_amount;
        $loanamount = $request->loan_amount;
        $paiddepositbalance = $request->paid_deposit_balance;
        $paidby = $request->paid_by;
        $cashpay = $request->cash_pay;
        $remark = $request->remark;
        $firstinstallmentdate = date("Y-m-d H:i:s", strtotime($request->first_installment_date));
        $processingdate =  date("Y-m-d H:i:s", strtotime($request->loan_disbursement_date));
    

      	// Branch ID
        $bcode = "";
        $bcode = $this->getBranchID($staffid);

        $disbursement_loan = DB::table($bcode.'_loans')
            ->leftJoin('loan_type', 'loan_type.id', '=', $bcode.'_loans.loan_type_id')
            ->leftJoin('tbl_branches', 'tbl_branches.id', '=', $bcode.'_loans.branch_id')
            ->leftJoin('tbl_center', 'tbl_center.center_uniquekey', '=', $bcode.'_loans.center_id')
            ->leftJoin('tbl_group', 'tbl_group.group_uniquekey', '=', $bcode.'_loans.group_id')
            ->leftJoin('tbl_client_basic_info', 'tbl_client_basic_info.client_uniquekey', '=', $bcode.'_loans.client_id')
            ->leftJoin('tbl_staff', 'tbl_staff.staff_code', '=', $bcode.'_loans.loan_officer_id')
            ->leftJoin($bcode.'_deposit',$bcode.'_deposit.loan_id','=',$bcode.'_loans.loan_unique_id')
            ->where($bcode.'_loans.loan_unique_id','=', $request->loan_id)
            ->select($bcode.'_loans.*',$bcode.'_deposit.*','loan_type.*','tbl_client_basic_info.*','tbl_client_basic_info.client_uniquekey AS client_id', 'tbl_staff.*')
            ->first();

        $loan_id = DB::table($bcode.'_loans')
            ->where('loan_unique_id','=', $request->loan_id)
            ->first();

        $accounts = DB::table('account_chart_externals')->where('branch_id', '=', $branchid)->where('external_acc_code', 'LIKE', '1-11' . '%')->get();
    		
    	$acc_code=DB::table('account_chart_external_details')->where('account_chart_external_details.external_acc_id',$accounts[0]->id)->first()->main_acc_code;

         $main_acc_code = [$acc_code];
        	// dd($main_acc_code);
            for ($i = 0; $i < count($main_acc_code); $i++) {
            
                   $datadisbursement = DB::table($bcode.'_disbursement')->insert([
            'loan_id'=> $loanid,
            'client_id'=> $disbursement_loan->client_id,
            'center_id'=> $disbursement_loan->center_id,
            'group_id'=> $disbursement_loan->group_id,
            'branch_id'=> $disbursement_loan->branch_id,
            'loan_officer_id'=> $disbursement_loan->loan_officer_id,
            'loan_product_id'=> $disbursement_loan->loan_type_id,
            'cash_acc_id'=> $main_acc_code[$i],
            'disbursed_amount'=> $disbursedamount,

            'principal'=> $loanamount,
            // 'interest'=> $request->interest,
            // 'penalty'=> $request->penalty,
            // 'service_charges'=> $request->service_charges,
            // 'total_balance'=> $request->total_balance,
            'paid_deposit_balance'=> $paiddepositbalance,
            'paid_by'=> $paidby,
            'cash_by'=> $cashpay,
            'remark'=> $remark,
            'disbursement_status'=> "activated",
            'first_installment_date'=> $firstinstallmentdate,
            'processing_date'=> $processingdate
        ]);
            
                // DB::table($bcode . '_deposit')->insert([
                //     'loan_id' => $request->loan_id,
                //     'client_id' => $request->client_id,
                //     'ref_no' => $request->ref_no,
                //     'invoice_no' => $request->invoice_no,
                //     'acc_join_id' => $main_acc_code[$i],
                //     'total_deposit_balance' => $request->total_deposit_balance,
                //     'paid_deposit_balance' => $request->paid_deposit_balance,
                //     'note' => $request->note,
                //     'deposit_pay_date' => date("Y-m-d H:i:s", strtotime($request->deposit_pay_date)),
                // ]);
            }
    
       //  ]);

    
    
        if($datadisbursement){
        
                DB::table($bcode.'_loans')
                	->where('loan_unique_id', $loanid)->update([
                	'disbursement_status' => 'Activated'
            	]);
            
				return response()->json(['status_code'=>200,'message'=>'Congratulations, Loan ID '.$loanid.' is active','data'=>$datadisbursement]);
        	}else{
            	return response()->json(['status_code'=>422,'message'=>'disbursement not found','data'=>null]);
        	}



        // } 
		// else {
		// return redirect()->back()->withErrors($validator);
		// }


    }

    // create Disbursement
    public function createDisbursement_bk(Request $request)
    {
        $branchid = $request->branchid;
        $staffid = $request->staffid;
        $loanid = $request->loanid;
        $cashaccid = $request->cashaccid;
        $disbursedamount = $request->disbursed_amount;
        $loanamount = $request->loan_amount;
        $paiddepositbalance = $request->paid_deposit_balance;
        $paidby = $request->paid_by;
        $cashpay = $request->cash_pay;
        $remark = $request->remark;
        $firstinstallmentdate = date("Y-m-d H:i:s", strtotime($request->first_installment_date));
        $processingdate = $request->processing_date;
        
        // Branch ID
        $bcode = "";
        $bcode = $this->getBranchID($staffid);

        $disbursement_loan = DB::table($bcode.'_loans')
            ->leftJoin('loan_type', 'loan_type.id', '=', $bcode.'_loans.loan_type_id')
            ->leftJoin('tbl_branches', 'tbl_branches.id', '=', $bcode.'_loans.branch_id')
            ->leftJoin('tbl_center', 'tbl_center.center_uniquekey', '=', $bcode.'_loans.center_id')
            ->leftJoin('tbl_group', 'tbl_group.group_uniquekey', '=', $bcode.'_loans.group_id')
            ->leftJoin('tbl_client_basic_info', 'tbl_client_basic_info.client_uniquekey', '=', $bcode.'_loans.client_id')
            ->leftJoin('tbl_staff', 'tbl_staff.staff_code', '=', $bcode.'_loans.loan_officer_id')
            ->leftJoin($bcode.'_deposit',$bcode.'_deposit.loan_id','=',$bcode.'_loans.loan_unique_id')
            ->where($bcode.'_loans.loan_unique_id','=',  $loanid)
            ->select($bcode.'_loans.*',$bcode.'_deposit.*','loan_type.*','tbl_client_basic_info.*','tbl_client_basic_info.client_uniquekey AS client_id', 'tbl_staff.*')
            ->first();

        $datadisbursement = DB::table($bcode.'_disbursement')->insert([
            'loan_id'=> $loanid,
            'client_id'=> $disbursement_loan->client_id,
            'center_id'=> $disbursement_loan->center_id,
            'group_id'=> $disbursement_loan->group_id,
            'branch_id'=> $disbursement_loan->branch_id,
            'loan_officer_id'=> $disbursement_loan->loan_officer_id,
            'loan_product_id'=> $disbursement_loan->loan_type_id,
            'cash_acc_id'=> $cashaccid,
            'disbursed_amount'=> $disbursedamount,

            'principal'=> $loanamount,
            // 'interest'=> $request->interest,
            // 'penalty'=> $request->penalty,
            // 'service_charges'=> $request->service_charges,
            // 'total_balance'=> $request->total_balance,
            'paid_deposit_balance'=> $paiddepositbalance,
            'paid_by'=> $paidby,
            'cash_by'=> $cashpay,
            'remark'=> $remark,
            'disbursement_status'=> "activated",
            'first_installment_date'=> $firstinstallmentdate,
            'processing_date'=> $processingdate
        ]);

        if($datadisbursement){
            
            $dataloan = DB::table($bcode.'_loans')
                ->where($bcode.'_loans.loan_unique_id','=',$loanid)
                ->update(['disbursement_status'=>"Activated"]);
        
        	$this->liveDisburse($request);

            if($dataloan){
                return response()->json(['status_code'=>200,'message'=>'disbursement create is success','data'=>$data]);
            }else{
                return response()->json(['status_code'=>422,'message'=>'loan and disbursement not found','data'=>null]);
            }
        }else{
            return response()->json(['status_code'=>422,'message'=>'disbursement not found','data'=>null]);
        }
    }

	public function getMainUrl37()
    {
        // $url = 'http://172.16.50.101/mis-core/public/api/';
        $url = env('MAIN_URL');
        return $url;
    }

    public function loginToken()
    {
        $route = $this->getMainUrl37() . 'login';
        $headers = [
            'Accept' => 'application/json'
        ];
        $response = Http::withHeaders($headers)->post($route, [
            'username' => 'ict@mis.com',
            'password' => '123456'
        ]);
        return json_decode($response)->data[0]->token;
    }

    public function liveDisburse($disburseDetail)
    {
        $branch_id = DB::table('tbl_staff')->where('staff_code', session()->get('staff_code'))->first()->branch_id;
        $bcode = $this->getBranchCode();
        $main_loan_id = DB::table('tbl_main_join_loan')->where('portal_loan_id', $disburseDetail->loan_id)->first();
        if ($main_loan_id) {
            $main_loan_id = $main_loan_id->main_loan_id;

            $clientPID = DB::table($bcode . '_loans')->where('loan_unique_id', $disburseDetail->loan_id)->first()->client_id;
            $clientMID = DB::table('tbl_main_join_client')->where('portal_client_id', $clientPID)->first()->main_client_id;
            $clientMDetail = DB::connection('main')->table('clients')->where('id', $clientMID)->first();

            $route = $this->getMainUrl37();

            $route_name = 'create-disbursement';
            $routeurl = $route . $route_name;
            $authorization = $this->loginToken();
        
			$acc_code = DB::table('account_chart_external_details')->where('account_chart_external_details.external_acc_id', $disburseDetail->cashout_acc_id)->first()->main_acc_id;
        	$main_acc_code = [$acc_code];
        	// dd($main_acc_code);
        
            $client = new \GuzzleHttp\Client(['verify' => false]);
            $result = $client->post($routeurl, [
                'headers' => [
                    'Content-Type' => 'application/x-www-form-urlencoded',
                    'Authorization' => 'Bearer ' . $authorization,
                ],
                'form_params' => [
                    'contract_id' => $main_loan_id,
                    'paid_disbursement_date' => $disburseDetail->loan_disbursement_date,
                    'first_payment_date' =>  $disburseDetail->first_installment_date,
                    'client_id' => $clientMDetail->id,
                    'reference' => $disburseDetail->remark == NULL ? 0 : $disburseDetail->remark, //$disbursements[$i]->remark,
                    'client_name' => $clientMDetail->name,
                    'client_nrc' => $clientMDetail->nrc_number,
                    'invoice_no' => 'inv-02345678',
                    'compulsory_saving' => 0,
                    'loan_amount' => $disburseDetail->loan_amount,
                    'total_money_disburse' => $disburseDetail->disbursed_amount,
                    'cash_out_id' => $main_acc_code[0],
                    'paid_by_tran_id' => 2,
                    'cash_pay' => 0,
                    'disburse_by' => 'loan-officer',
                    'branch_id' => $branch_id,
                ]
            ]);

            $response = (string) $result->getBody();
            $response = json_decode($response); // Using this you can access any key like below
            if ($response->status_code == 200) {
                DB::table('tbl_main_join_loan_disbursement')->insert([
                    'main_loan_disbursement_id' => $main_loan_id,
                    'main_loan_disbursement_client_id' => DB::connection('main')->table('loans_' . $branch_id)->where('id', $main_loan_id)->first()->client_id,
                    'portal_loan_disbursement_id' => $disburseDetail->loan_id
                ]);
            
            	$LoansSchedule = DB::connection('portal')->table($bcode . '_loans_schedule')->where('loan_unique_id', $disburseDetail->loan_id)->get();
                $main_disbursement_id = DB::connection('portal')->table('tbl_main_join_loan')->where('portal_loan_id', $disburseDetail->loan_id)->first()->main_loan_id;
                $RpDetail = DB::connection('main')->table('loan_disbursement_calculate_' . $branch_id)->where('disbursement_id', $main_disbursement_id)->get();

                for ($y = 0; $y < count($LoansSchedule); $y++) {
                    if ($RpDetail[$y]->no == ($y + 1)) {
                        DB::table('tbl_main_join_repayment_schedule')->where('portal_disbursement_schedule_id', $LoansSchedule[$y]->id)->update([
                            'portal_disbursement_id' => $LoansSchedule[$y]->loan_unique_id,
                            'portal_disbursement_schedule_id' => $LoansSchedule[$y]->id,
                            'main_disbursement_schedule_id' => $RpDetail[$y]->id,
                            'branch_id' => $branch_id
                        ]);
                    }
                }
            }
        }
    }
}
