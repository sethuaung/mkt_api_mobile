<?php

namespace App\Http\Controllers\Api\Loan;

use App\Http\Controllers\Controller;
use App\Models\Loan\LoanType;
use Illuminate\Http\Request;

class LoanScheduleController extends Controller
{
    public function getLoanSchedule(Request $request) {
        date_default_timezone_set('Asia/Yangon');

        $branchid = $request->branchid; 
        $loantypeid = $request->loantypeid; 
        $loanamount = $request->loanamount;
        //$start_date = $request->startdate;
        $start_date = date('Y-m-d H:i:s', strtotime($request->startdate))

        $obj_loantype = LoanType::where('id',$loantypeid)->first();
        $loan_name = $obj_loantype->name;
        $loan_amount = $obj_loantype->description;
        $loan_term = $obj_loantype->loan_term;
        $loan_term_value = (Integer)$obj_loantype->loan_term_value;
        $interest_rate = $obj_loantype->interest_rate;
        $interest_rate_period = $obj_loantype->interest_rate_period;
        $interest_method = $obj_loantype->interest_method;
        $principal_formula = $obj_loantype->principal_formula;
        $interest_formula = $obj_loantype->interest_formula;

        // Repayment date
        if ($loan_term == "Day") {
            $loan_term = 1;
        } else if ($loan_term == "Week") {
            $loan_term = 7;
        } else if ($loan_term == "Two-Weeks") {
            $loan_term = 14;
        } else if ($loan_term == "Four-Weeks") {
            $loan_term = 28;
        }  else if ($loan_term == "Month") {
            $loan_term = 30;
        } else if ($loan_term == "Year") {
            $loan_term = 30;
        } 
        if(($loantypeid == 1) or ($loantypeid ==  2)){
            // GeneralWeekly14D
            $loanschedule = $this->fixedLoanMethod(
                $loan_amount,
                $loan_term,$loan_term_value,
                $interest_rate,
                $principal_formula,
                // $start_date);
                $entrydate);
        }else if(($loantypeid == 3) or ($loantypeid == 4)){
            // ExtraLoanWeekly
            $loanschedule = $this->principalFinalMethod30D(
                $loan_amount,
                $loan_term,$loan_term_value,
                $interest_rate,
                $principal_formula,
                // $start_date);
                $entrydate);
        }else if(($loantypeid >= 5) && ($loantypeid <= 19)){
            // 5 to 19
            // ExtraLoanWeekly
            $loanschedule = $this->fixedPrincipalMethod30D(
                $loan_amount,
                $loan_term,$loan_term_value,
                $interest_rate,
                $principal_formula,
                // $start_date);
                $entrydate);
        }else if(($loantypeid >= 20) && ($loantypeid <= 22)){
            // ExtraLoanWeekly
            $loanschedule = $this->principalFinalMethod28D(
                $loan_amount,
                $loan_term,$loan_term_value,
                $interest_rate,
                $principal_formula,
                // $start_date);
                $entrydate);
        }else if(($loantypeid >= 23) && ($loantypeid <= 25)){
            // GeneralMonthly28D
            $loanschedule = $this->fixedPrincipalMethod28D(
                $loan_amount,
                $loan_term,$loan_term_value,
                $interest_rate,
                $principal_formula,
                // $start_date);
                $entrydate);
        }else if(($loantypeid == 26) or ($loantypeid == 27)){
            // ExtraLoanWeekly
            $loanschedule = $this->fixedPrincipalMethod30D(
                $loan_amount,
                $loan_term,$loan_term_value,
                $interest_rate,
                $principal_formula,
                // $start_date);
                $entrydate);
        }
       
    	if($loan_term_value == 1){
    		$loan_term_value = 12;
    	}else{
    		$loan_term_value = $loan_term_value;
    	}
    
        $data_loantype = array(
            "loan-id" => $loantypeid,
            "loan-name" => $loan_name,
            "loan-schedule" => $loanschedule,
        );
       
        return response()->json($data);
       
    }

	//fixed principal and interest
    public function fixedLoanMethod($loan_amount,$loan_term,$loan_term_value,$interest_rate,$principal_formula,$start_date) {
        // amount * 0.083 - principal
        // amount *( 28% / 365 ) *28 - interest
        if( $loan_term_value == 1){ // 1 mean 12 months
            $loan_term_value = 12;
        }

        $schedule_date = strtotime($start_date);
        $principal = round($loan_amount / $loan_term_value);
        $interest = round($principal * 14/100);

        for ($i = 1; $i <= $loan_term_value; $i++) {
           
            $total = $loan_amount - $principal;
            $loan_amount -=  $principal;
            $balance = $loan_amount;

            $schedule_date = date("d-M-Y", strtotime($schedule_date . '+' . $loan_term . 'days'));
            $loanschedule[$i] = array(
                "repay_count" => $i,
                "repay_principal" => $principal,
                "repay_principal" => $principal,
                "repay_interest" => $interest,
                "now_balance" => $balance,
                "next_balance" => $total,
                "date" => $schedule_date
            );
        }
        return $loanschedule;
    }

    //pay principal at final 30days
    public function principalFinalMethod30D($loan_amount,$loan_term,$loan_term_value,$interest_rate,$principal_formula,$start_date) {
        // amount * 0.083 - principal
        // amount *( 28% / 365 ) *28 - interest
        if( $loan_term_value == 1){ // 1 mean 12 months
            $loan_term_value = 12;
        }

        $schedule_date = $start_date;
        $principal = 0;
        $next_balance = $loan_amount;
        for ($i = 1; $i <= $loan_term_value; $i++) {
            // $paid_principal = 0;
            if($i == $loan_term_value) {
                $principal = $loan_amount;
            }
            // $paid_principal += $principal;

            $interest = round($loan_amount * (($interest_rate/ 100) / 12));
            $next_balance -= $principal;
            $total = $loan_amount - $principal;
            $loan_amount -=  $principal;
            $balance = $loan_amount;

            $schedule_date = date("d-M-Y", strtotime($schedule_date . '+' . $loan_term . 'days'));

            $loanschedule[$i] = array(
                "repay_count" => $i,
                "repay_principal" => $principal,
                "repay_interest" => $interest,
                "now_balance" => $balance,
                "next_balance" => $total,
                "balance" => $balance,
                "total" => $total,
                "principal" => $principal,
                "date" => $schedule_date,
                "interest" => $interest,
            );
        }
        return $loanschedule;
    }

    //pay principal at final 28 days
    public function principalFinalMethod28D($loan_amount,$loan_term,$loan_term_value,$interest_rate,$principal_formula,$start_date) {
        // amount * 0.083 - principal
        // amount *( 28% / 365 ) *28 - interest
        if( $loan_term_value == 1){ // 1 mean 12 months
            $loan_term_value = 12;
        }

        $schedule_date = $start_date;
        $principal = 0;
        $next_balance = $loan_amount;
        $interest = round($loan_amount * (($interest_rate/ 100) / 365) * 28);

        for ($i = 1; $i <= $loan_term_value; $i++) {
            // $paid_principal = 0;
            if($i == $loan_term_value) {
                $principal = $loan_amount;
            }
            // $paid_principal += $principal;

            $next_balance -= $principal;
            $total = $loan_amount - $principal;
            $loan_amount -=  $principal;
            $balance = $loan_amount;

            $schedule_date = date("d-M-Y", strtotime($schedule_date . '+' . $loan_term . 'days'));

            $loanschedule[$i] = array(
                "repay_count" => $i,
                "repay_principal" => $principal,
                "repay_interest" => $interest,
                "now_balance" => $balance,
                "next_balance" => $total,
                "balance" => $balance,
                "total" => $total,
                "principal" => $principal,
                "date" => $schedule_date,
                "interest" => $interest,
            );
        }
        return $loanschedule;
    }

    //fixed principal and interest change 30 days
    public function fixedPrincipalMethod30D($loan_amount,$loan_term,$loan_term_value,$interest_rate,$principal_formula,$start_date) {
        // amount * 0.083 - principal
        // amount *( 28% / 365 ) *28 - interest
        if( $loan_term_value == 1){ // 1 mean 12 months
            $loan_term_value = 12;
        }
        // dd($principal_formula);
        $schedule_date = $start_date;
        $principal = round($loan_amount * $principal_formula);
        // dd($principal);
        $next_balance = $loan_amount;
        for ($i = 1; $i <= $loan_term_value; $i++) {

            $paid_principal = 0;
            if($i == $loan_term_value) {
                $principal = $loan_amount - $paid_principal;
            }
            $paid_principal += $principal;

            $interest = round($next_balance * ($interest_rate/ 100) / 12);
            $next_balance -= $principal;
            $total = $loan_amount - $principal;
            $loan_amount -=  $principal;
            $balance = $loan_amount;

            $schedule_date = date("d-M-Y", strtotime($schedule_date . '+' . $loan_term . 'days'));

            $loanschedule[$i] = array(
                "repay_count" => $i,
                "repay_principal" => $principal,
                "repay_interest" => $interest,
                "now_balance" => $balance,
                "next_balance" => $total,
                "balance" => $balance,
                "total" => $total,
                "principal" => $principal,
                "date" => $schedule_date,
                "interest" => $interest,
            );
        }
        return $loanschedule;
    }

     //fixed principal and interest change 28 days
     public function fixedPrincipalMethod28D($loan_amount,$loan_term,$loan_term_value,$interest_rate,$principal_formula,$start_date) {
        // amount * 0.083 - principal
        // amount *( 28% / 365 ) *28 - interest
        if( $loan_term_value == 1){ // 1 mean 12 months
            $loan_term_value = 12;
        }

        $schedule_date = $start_date;
        $principal = round($loan_amount * $principal_formula);
        $next_balance = $loan_amount;
        for ($i = 1; $i <= $loan_term_value; $i++) {

            $paid_principal = 0;
            if($i == $loan_term_value) {
                $principal = $loan_amount - $paid_principal;
            }
            $paid_principal += $principal;

            $interest = round($next_balance * (($interest_rate/ 100) / 365) * 28);
            $next_balance -= $principal;
            $total = $loan_amount - $principal;
            $loan_amount -=  $principal;
            $balance = $loan_amount;

            $schedule_date = date("d-M-Y", strtotime($schedule_date . '+' . $loan_term . 'days'));

            $loanschedule[$i] = array(
                "repay_count" => $i,
                "repay_principal" => $principal,
                "repay_interest" => $interest,
                "now_balance" => $balance,
                "next_balance" => $total,
                "balance" => $balance,
                "total" => $total,
                "principal" => $principal,
                "date" => $schedule_date,
                "interest" => $interest,
            );
        }
        return $loanschedule;
    }

}
