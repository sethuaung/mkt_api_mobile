<?php

namespace App\Http\Controllers\Api\Loan;

use App\Http\Controllers\Controller;
use App\Models\Branch;
use App\Models\CenterLeader;
use App\Models\Group;
use App\Models\Guarantor;
use App\Models\Loan\LoanBusinessCategory;
use App\Models\Loan\LoanBusinessType;
use App\Models\Loan\LoanInfo;
use App\Models\Loan\LoanType;
use App\Models\Staff;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LoanController extends Controller
{
    //auto get branch id
    public function getBranchID($staffid){
        //dd($request->staffid);
        $obj_branchcode = DB::table('tbl_staff')
            ->join('tbl_branches', 'tbl_branches.id', '=', 'tbl_staff.branch_id')
            ->where('tbl_staff.staff_code',$staffid)
            ->select('tbl_branches.branch_code')
            ->first();
        
        $res = strtolower((string)$obj_branchcode->branch_code); 
        return $res;               
    }  
       // showLoanList
    public function getLoanList (Request $request)
    {
    	//dd($request);
        $branchid = $request->branchid;
        $staffid = $request->staffid;
        $client_status = $request->client_status;
        $table_branchcode = "";
        $table_branchcode = $this->getBranchID($staffid);

        $data = DB::table($table_branchcode.'_loans')
        ->leftjoin('tbl_client_basic_info', 'tbl_client_basic_info.client_uniquekey', '=', $table_branchcode.'_loans.client_id')
        ->leftjoin('tbl_main_join_loan', 'tbl_main_join_loan.portal_loan_id', '=', $table_branchcode.'_loans.loan_unique_id')
       	->leftjoin('loan_type', 'loan_type.id', '=', $table_branchcode.'_loans.loan_type_id')
       	->leftjoin('tbl_group', 'tbl_group.group_uniquekey', '=', $table_branchcode.'_loans.group_id')
       	->leftjoin('tbl_center', 'tbl_center.center_uniquekey', '=', $table_branchcode.'_loans.center_id')
        ->leftjoin('tbl_main_join_client', 'tbl_main_join_client.portal_client_id', '=', $table_branchcode.'_loans.client_id')
       	->leftjoin('tbl_main_join_center', 'tbl_main_join_center.portal_center_id', '=', $table_branchcode.'_loans.center_id')
        ->leftjoin('tbl_main_join_group', 'tbl_main_join_group.portal_group_id', '=', $table_branchcode.'_loans.group_id')
        ->where($table_branchcode.'_loans.loan_officer_id', $staffid)
       	->select($table_branchcode.'_loans.*','loan_type.name as loantype_name','tbl_group.group_uniquekey',
                'tbl_center.center_uniquekey','tbl_client_basic_info.name as client_name',
                'tbl_main_join_loan.main_loan_id','tbl_main_join_loan.main_loan_code',
                'tbl_main_join_client.main_client_code','tbl_main_join_center.main_center_code','tbl_main_join_group.main_group_code')
        ->orderBy($table_branchcode.'_loans.loan_unique_id', 'DESC')->get();
        if($data){
            return response()->json(['status_code'=>200,'message'=>'success','data'=>$data]);
        }else{
            return response()->json(['status_code'=>422,'message'=>'fail','data'=>null]);
        }
    }

	// Approved wait list
    public function getLoanPendingList (Request $request)
    {
    	//dd($request);
        $branchid = $request->branchid;
        $staffid = $request->staffid;
        $table_branchcode = "";
        $table_branchcode = $this->getBranchID($staffid);

        $data = DB::table($table_branchcode.'_loans')
       	->leftjoin('loan_type', 'loan_type.id', '=', $table_branchcode.'_loans.loan_type_id')
       	->leftjoin('tbl_group', 'tbl_group.group_uniquekey', '=', $table_branchcode.'_loans.group_id')
       	->leftjoin('tbl_center', 'tbl_center.center_uniquekey', '=', $table_branchcode.'_loans.center_id')
        ->where($table_branchcode.'_loans.loan_officer_id', $staffid)
        ->where($table_branchcode.'_loans.disbursement_status', 'Pending')
       	->select( $table_branchcode.'_loans.*','loan_type.name as loantype_name','tbl_group.group_uniquekey','tbl_center.center_uniquekey')
        ->orderBy($table_branchcode.'_loans.first_installment_date', 'DESC')->get();
        if($data){
            return response()->json(['status_code'=>200,'message'=>'success','data'=>$data]);
        }else{
            return response()->json(['status_code'=>422,'message'=>'fail','data'=>null]);
        }
    }

	// Approved wait list
    public function getLoanDetail (Request $request)
    {
    	//dd($request);
        $branchid = $request->branchid;
        $staffid = $request->staffid;
     	$loanid = $request->loanid;
        $table_branchcode = "";
        $table_branchcode = $this->getBranchID($staffid);

        // $data = DB::table($table_branchcode.'_loans')
        // ->leftjoin('loan_type', 'loan_type.id', '=', $table_branchcode.'_loans.loan_type_id')
        // ->leftjoin('tbl_group', 'tbl_group.group_uniquekey', '=', $table_branchcode.'_loans.group_id')
        // ->leftjoin('tbl_center', 'tbl_center.center_uniquekey', '=', $table_branchcode.'_loans.center_id')
        // ->where($table_branchcode.'_loans.loan_officer_id', $staffid)
        // ->where($table_branchcode.'_loans.loan_unique_id', $loanid)
        // ->select( $table_branchcode.'_loans.*','loan_type.name as loantype_name',)
        // ->first();
    
        $data_loans = DB::table($table_branchcode.'_loans')
        ->leftjoin('tbl_client_basic_info', 'tbl_client_basic_info.client_uniquekey', '=', $table_branchcode.'_loans.client_id')
        ->leftjoin('tbl_staff', 'tbl_staff.staff_code', '=', $table_branchcode.'_loans.loan_officer_id')
        ->leftjoin('tbl_guarantors', 'tbl_guarantors.guarantor_uniquekey', '=', $table_branchcode.'_loans.guarantor_a')
        ->leftjoin('loan_type', 'loan_type.id', '=', $table_branchcode.'_loans.loan_type_id')
        ->leftjoin('tbl_main_join_loan', 'tbl_main_join_loan.portal_loan_id', '=', $table_branchcode.'_loans.loan_unique_id')
        ->leftjoin('tbl_main_join_client', 'tbl_main_join_client.portal_client_id', '=', $table_branchcode.'_loans.client_id')
       	->leftjoin('tbl_main_join_center', 'tbl_main_join_center.portal_center_id', '=', $table_branchcode.'_loans.center_id')
        ->leftjoin('tbl_main_join_group', 'tbl_main_join_group.portal_group_id', '=', $table_branchcode.'_loans.group_id')
        ->leftjoin('tbl_main_join_guarantor', 'tbl_main_join_guarantor.portal_guarantor_id', '=', $table_branchcode.'_loans.guarantor_a')
        ->where($table_branchcode.'_loans.loan_unique_id', $loanid)
        ->where($table_branchcode.'_loans.loan_officer_id',  $staffid)
        ->select( $table_branchcode.'_loans.*','loan_type.name as loantype_name','tbl_client_basic_info.*',
                'tbl_main_join_loan.main_loan_id','tbl_main_join_loan.main_loan_code',
                 'tbl_main_join_guarantor.main_guarantor_code as main_guarantor_cod_a',
                'tbl_main_join_client.main_client_code','tbl_main_join_center.main_center_code','tbl_main_join_group.main_group_code')
        ->get();
    

     	$obj_guarantor_a = Guarantor::where('guarantor_uniquekey',$data_loans[0]->guarantor_a)->select('id','branch_id','guarantor_uniquekey', 'name', 'dob','gender', 'nrc', 'phone_primary','guarantor_photo','address_primary')->get();
    	$obj_guarantor_b = Guarantor::where('guarantor_uniquekey',$data_loans[0]->guarantor_b)->select('id','branch_id','guarantor_uniquekey', 'name', 'dob', 'gender','nrc', 'phone_primary','guarantor_photo','address_primary')->get();
    	$obj_guarantor_c = Guarantor::where('guarantor_uniquekey',$data_loans[0]->guarantor_c)->select('id','branch_id','guarantor_uniquekey', 'name', 'dob', 'gender','nrc', 'phone_primary','guarantor_photo','address_primary')->get();
    
        if($data_loans){
            return response()->json(['status_code'=>200,'message'=>'success','data'=>$data_loans,
                                     'guarantor_a'=>$obj_guarantor_a,
                                     'guarantor_b'=>$obj_guarantor_b,
                                     'guarantor_c'=>$obj_guarantor_c]);
        }else{
            return response()->json(['status_code'=>422,'message'=>'fail','data'=>null]);
        }
    }



    // show search By Mutiple for Loan 
    public function searchByGeneral (Request $request)
    {
        $branchid = $request->branchid;
        $staffid = $request->staffid;
        $searchvalue = $request->generalvalue;
        $table_branchcode = "";
        $table_branchcode = $this->getBranchID($staffid);

        $data_loans = DB::table($table_branchcode.'_loans')
        ->leftjoin('tbl_client_basic_info', 'tbl_client_basic_info.client_uniquekey', '=', $table_branchcode.'_loans.client_id')
        ->leftjoin('tbl_staff', 'tbl_staff.staff_code', '=', $table_branchcode.'_loans.loan_officer_id')
        ->leftjoin('tbl_guarantors', 'tbl_guarantors.guarantor_uniquekey', '=', $table_branchcode.'_loans.guarantor_a')
        ->leftjoin('loan_type', 'loan_type.id', '=', $table_branchcode.'_loans.loan_type_id')
        ->leftjoin('tbl_main_join_loan', 'tbl_main_join_loan.portal_loan_id', '=', $table_branchcode.'_loans.loan_unique_id')
        ->leftjoin('tbl_main_join_client', 'tbl_main_join_client.portal_client_id', '=', $table_branchcode.'_loans.client_id')
       	->leftjoin('tbl_main_join_center', 'tbl_main_join_center.portal_center_id', '=', $table_branchcode.'_loans.center_id')
        ->leftjoin('tbl_main_join_group', 'tbl_main_join_group.portal_group_id', '=', $table_branchcode.'_loans.group_id')
        ->leftjoin('tbl_main_join_guarantor', 'tbl_main_join_guarantor.portal_guarantor_id', '=', $table_branchcode.'_loans.guarantor_a')
        //->join('tbl_group', 'tbl_group.group_uniquekey', '=', $table_branchcode.'_loans.group_id')
        //->join('tbl_center', 'tbl_center.center_uniquekey', '=', $table_branchcode.'_loans.center_id')
        ->where($table_branchcode.'_loans.loan_unique_id', 'LIKE', $searchvalue . '%')
        ->orWhere($table_branchcode.'_loans.disbursement_status', 'LIKE', $searchvalue . '%')
        ->orWhere($table_branchcode.'_loans.loan_officer_id', 'LIKE', $searchvalue . '%')
        ->orWhere('tbl_staff.name', 'LIKE', $searchvalue . '%')
        ->orWhere('tbl_client_basic_info.client_uniquekey', 'LIKE', $searchvalue . '%')
        ->orWhere('tbl_client_basic_info.name', 'LIKE', $searchvalue . '%')
        ->orWhere('tbl_client_basic_info.name_mm', 'LIKE', $searchvalue . '%')
        ->orWhere('tbl_client_basic_info.nrc', 'LIKE', $searchvalue . '%')
        ->orWhere('tbl_client_basic_info.old_nrc', 'LIKE', $searchvalue . '%')
        ->orWhere('tbl_client_basic_info.nrc_card_id', 'LIKE', $searchvalue . '%')
        ->orWhere('tbl_client_basic_info.dob', 'LIKE', $searchvalue . '%')
        ->orWhere('tbl_client_basic_info.phone_primary', 'LIKE', $searchvalue . '%')
        ->orWhere('tbl_client_basic_info.phone_secondary', 'LIKE', $searchvalue . '%')
        ->orWhere('tbl_guarantors.name', 'LIKE', $searchvalue . '%')
        ->orWhere('loan_type.name', 'LIKE', $searchvalue . '%')
        
        ->orWhere('tbl_main_join_client.main_client_code', 'LIKE', $searchvalue . '%') // main in search
        ->orWhere('tbl_main_join_loan.main_loan_id', 'LIKE', $searchvalue . '%') // main in search
        ->orWhere('tbl_main_join_center.main_center_code', 'LIKE', $searchvalue . '%') // main in search
        ->orWhere('tbl_main_join_group.main_group_code', 'LIKE', $searchvalue . '%') // main in search
        ->orWhere('tbl_main_join_guarantor.main_guarantor_code', 'LIKE', $searchvalue . '%') // main in search
        ->orWhere($table_branchcode.'_loans.group_id', 'LIKE', $searchvalue . '%')
        ->where($table_branchcode.'_loans.loan_officer_id',  $staffid)
        ->select( $table_branchcode.'_loans.*','loan_type.name as loantype_name','tbl_client_basic_info.*',
                 'tbl_main_join_loan.main_loan_id','tbl_main_join_loan.main_loan_code',
                 'tbl_main_join_guarantor.main_guarantor_code as main_guarantor_cod_a',
                'tbl_main_join_client.main_client_code','tbl_main_join_center.main_center_code','tbl_main_join_group.main_group_code')
        ->orderBy($table_branchcode.'_loans.loan_unique_id', 'DESC')
        ->get();
        if($data_loans){
            return response()->json(['status_code'=>200,'message'=>'success','data'=>$data_loans]);
        }else{
            return response()->json(['status_code'=>422,'message'=>'fail','data'=>null]);
        }
    }

    // loan product
    public function getLoanType (Request $request)
    {
        $loantype = $request->loantype;
        $obj_staff = Staff::where('staff_code',  $loantype)->select('id')->first();

        $data = LoanInfo::where('loan_officer_id',  $obj_staff->staff_code)
        ->leftJoin('loan_type', function($advancedLeftJoin){
            $advancedLeftJoin->on('loans.loan_type_id', '=', 'loan_type.id');
        })
        ->leftJoin('tbl_group', function($advancedLeftJoin){
            $advancedLeftJoin->on('loans.group_uniquekey', '=', 'tbl_group.id');
        })
        ->leftJoin('tbl_center', function($advancedLeftJoin){
            $advancedLeftJoin->on('loans.center_uniquekey', '=', 'tbl_center.id');
        })
        ->select( 'loans.*','loan_type.name as loantype_name','tbl_group.group_uniquekey','tbl_center.center_uniquekey')
        ->orderBy('first_installment_date', 'DESC')->get();
        if($data){
            return response()->json(['status_code'=>200,'message'=>'success','data'=>$data]);
        }else{
            return response()->json(['status_code'=>422,'message'=>'fail','data'=>null]);
        }
    }

        // loan product
    public function getClientListforLoan (Request $request){
        $branchid =  $request->branchid;
    	$staffid =  $request->staffid;
        //$staffid = Staff::where('staff_code',  $request->staffid)->select('id')->first();
        $dataclientinfo = DB::table('tbl_client_basic_info')
        ->join('tbl_client_photo', 'tbl_client_photo.client_uniquekey', '=', 'tbl_client_basic_info.client_uniquekey')
        ->join('tbl_client_join', 'tbl_client_join.client_id', '=', 'tbl_client_basic_info.client_uniquekey')
        ->leftjoin('tbl_main_join_client', 'tbl_main_join_client.portal_client_id', '=', 'tbl_client_basic_info.client_uniquekey')
        ->where('tbl_client_join.branch_id', $branchid)
        ->where('tbl_client_join.staff_id',  $staffid)
        ->select( 'tbl_client_basic_info.client_uniquekey',
        'tbl_client_basic_info.name_mm','tbl_client_basic_info.name',
        'tbl_client_basic_info.dob','tbl_client_basic_info.nrc','tbl_client_photo.client_photo as photo',
        'tbl_client_join.id as clientid','tbl_client_join.branch_id','tbl_client_join.staff_id','tbl_main_join_client.main_client_code')
        ->get();

        // $dataloaninfo =LoanInfo::where('client_id',  $dataclientinfo->clientid)
        // ->select( 'loan_unique_id','loan_type_id','loan_application_date')
        // ->get();

        $dataloaninfo = "loading";
        if($dataclientinfo){
            return response()->json(['status_code'=>200,'message'=>'success','data_clientinfo'=>$dataclientinfo,'data_loaninfo'=>$dataloaninfo]);
        }else{
            return response()->json(['status_code'=>422,'message'=>'fail','data'=>null]);
        }
    }

    public function getGuarantorListforLoan (Request $request){
        $branchid =  $request->branchid;
        //$staffid = Staff::where('staff_code',  $request->staffid)->select('id')->first();
        //$dataguarantorinfo = Guarantor::where('branch_id',$branchid)->get();
        $dataguarantorinfo = DB::table('tbl_guarantors')
        	->leftjoin('tbl_main_join_guarantor', 'tbl_main_join_guarantor.portal_guarantor_id', '=', 'tbl_guarantors.guarantor_uniquekey')
        	//->where('tbl_guarantors.guarantor_uniquekey',  $request->staffid)
            ->where('tbl_guarantors.branch_id', $branchid)
        	->get();
    
        if($dataguarantorinfo){
            return response()->json(['status_code'=>200,'message'=>'success','data_guarantorinfo'=>$dataguarantorinfo]);
        }else{
            return response()->json(['status_code'=>422,'message'=>'fail','data'=>null]);
        }
    }

    public function getBusinessTypeforLoan (Request $request){
        $business_types = LoanBusinessType::orderby('business_type_name')->select('business_type_name','id')->get();
        if($business_types){
            return response()->json(['status_code'=>200,'message'=>'success','data_business_types'=>$business_types]);
        }else{
            return response()->json(['status_code'=>422,'message'=>'fail','data'=>null]);
        }
    }

    public function getBusinessCategoryforLoan (Request $request){
        $business_categories = LoanBusinessCategory::where('business_type_id',$request->business_types)
        ->orderby('business_category_name')->select('business_category_name','id','business_type_id','business_category_name_mm')->get();
        if($business_categories){
            return response()->json(['status_code'=>200,'message'=>'success','data_business_categories'=>$business_categories]);
        }else{
            return response()->json(['status_code'=>422,'message'=>'fail','data'=>null]);
        }
    }

    public function getLoanTypeForCreate(Request $request)
    {
        $branchid =  $request->branchid;
        $staffid =  $request->staffid;
     	$loantypeid =  $request->loantypeid;
        $obj_branchid = Branch::where('id',$branchid)->get()->last();

        // Get Loan Officer 
        $obj_staff = Staff::where('branch_id', $branchid)->select('id','staff_code','name')->get();
        // Get Center
        // $obj_center = CenterLeader::where('branch_id', $branchid)->select('id','center_uniquekey')->get();
        // Get Group
        // $obj_group = Group::where('branch_id', $branchid)->get();
    
        // Get Center
        $obj_center = DB::table('tbl_center')
        	->join('tbl_main_join_center', 'tbl_main_join_center.portal_center_id', '=', 'tbl_center.center_uniquekey')
        	->where('tbl_center.branch_id', $branchid)
        	->where('tbl_center.staff_id', $staffid)
        	->get();
        // Get Group
        $obj_group = DB::table('tbl_group')
        	->join('tbl_main_join_group', 'tbl_main_join_group.portal_group_id', '=', 'tbl_group.group_uniquekey')
            //->where('tbl_group.center_id', $staffid)
        	->get();
    
        // Get Customer Type
        $obj_guarantor = Guarantor::select('id','branch_id','guarantor_uniquekey', 'name', 'dob', 'nrc', 'phone_primary')->get();
        // Get Loantype
        $obj_loantype = LoanType::all();

        $obj_charge = DB::table('loan_charges_and_type')
            ->leftJoin('loan_charges_type', 'loan_charges_type.id', '=', 'loan_charges_and_type.charges_id')
            ->where('loan_charges_and_type.loan_type_id', '=', $loantypeid)
            ->select('loan_charges_type.*', 'loan_charges_type.id as loan_charge_id')
            ->get();

        $obj_compulsory = DB::table('loan_compulsory_product')
            ->leftJoin('loan_compulsory_type', 'loan_compulsory_type.compulsory_code', '=', 'loan_compulsory_product.compulsory_product_id')
            // ->where('loan_compulsory_product.branch_id', Staff::where('staff_code',$staffid)->first()->branch_id)
            ->select('loan_compulsory_type.*', 'loan_compulsory_type.compulsory_code as loan_compulsory_id')
            ->get();


        $data = array(
            array(
                "clientid" =>"",
                "branchid" => $obj_branchid->id,
                "branchname" => $obj_branchid->branch_name,
                "branchcode" => $obj_branchid->branch_code,
            )
        );

        return response()->json(['status_code'=>200, 'message'=>'success',
        'data'=>$data,'loanofficer-list'=>$obj_staff,'center-list'=>$obj_center,
        'group-list'=>$obj_group,'guarantor-list'=>$obj_guarantor,
        'loantype-list'=>$obj_loantype,'charge-list'=>$obj_charge,'compulsorysaving-list'=>$obj_compulsory]);
    }
    

    public function getChargesCompulsory(Request $request)
    {
        $branchid =  $request->branchid;
        $staffid =  $request->staffid;
        $loantypeid =  $request->loantypeid;
       
        $charge = DB::table('loan_charges_and_type')
            ->leftJoin('loan_charges_type', 'loan_charges_type.id', '=', 'loan_charges_and_type.charges_id')
            ->where('loan_charges_and_type.loan_type_id', '=', $loantypeid)
            ->select('loan_charges_type.*', 'loan_charges_type.id as loan_charge_id')
            ->get();
    
        // $compulsory = DB::table('loan_compulsory_product')
        //     ->leftJoin('loan_compulsory_type', 'loan_compulsory_type.id', '=', 'loan_compulsory_product.compulsory_product_id')
        //     ->where('loan_compulsory_product.branch_id', Staff::where('staff_code',$staffid)->first()->branch_id)
        //     ->select('loan_compulsory_type.*', 'loan_compulsory_type.id as loan_compulsory_id')
        //     ->get();
        // $charge = DB::table('loan_charges_and_type')
        //     ->Join('loan_charges_type', 'loan_charges_type.charge_code', '=', 'loan_charges_and_type.charges_id')
        //     ->where('loan_charges_and_type.loan_type_id', $loantypeid)
        //     ->select('loan_charges_type.*', 'loan_charges_type.id as loan_charge_id')
        //     ->get();

        $compulsory = DB::table('loan_compulsory_product')
            ->leftJoin('loan_compulsory_type', 'loan_compulsory_type.compulsory_code', '=', 'loan_compulsory_product.compulsory_product_id')
            // ->where('loan_compulsory_product.branch_id', $branchid)
            ->select('loan_compulsory_type.*', 'loan_compulsory_type.id as loan_compulsory_id')
            ->get();

        if($compulsory && $charge){
            return response()->json(['status_code'=>200,'message'=>'success','charge-list'=>$charge,'compulsorysaving-list'=>$compulsory]);
        }else{
            return response()->json(['status_code'=>422,'message'=>'fail','data'=>null]);
        }
    }

	public function getLoanSchedule(Request $request) {
        $branchid =  $request->branchid;
        $staffid =  $request->staffid;
        $entrydate = $request->startdate;
		$loantypeid = $request->loantypeid;
    	$loanid = $request->loanid;
    
    	if($loanid){
             $table_branchcode = "";
       		 $table_branchcode = $this->getBranchID($staffid);

        	$loan_info = DB::table($table_branchcode.'_loans')
       		->join('loan_type', 'loan_type.id', '=', $table_branchcode.'_loans.loan_type_id')
        	->where($table_branchcode.'_loans.loan_officer_id', $staffid)
       		->select( $table_branchcode.'_loans.*','loan_type.name as loantype_name')
        	->first();
    	}else{
        	$loan_info = null;
        }

   
        //------------------------------------
        $obj_loantype = DB::table('loan_type')->where('id',$loantypeid)->first();
     	$loan_id = $obj_loantype->id;
        $loan_name = $obj_loantype->name;
        if($request->loan_amount) {
            $loan_amount = $request->loan_amount;
        } else {
            $loan_amount = $obj_loantype->description;
        }
        $loan_term = $obj_loantype->loan_term;
        $loan_term_value = (Integer)$obj_loantype->loan_term_value;
        $interest_rate = $obj_loantype->interest_rate;
        $interest_rate_period = $obj_loantype->interest_rate_period;
        $interest_method = $obj_loantype->interest_method;
        $principal_formula = $obj_loantype->principal_formula;
        $interest_formula = $obj_loantype->interest_formula;


  
        // Repayment date
        if ($loan_term == "Day") {
            $loan_term = 1;
        } else if ($loan_term == "Week") {
            $loan_term = 7;
        } else if ($loan_term == "Two-Weeks") {
            $loan_term = 14;
        } else if ($loan_term == "Four-Weeks") {
            $loan_term = 28;
        }  else if ($loan_term == "Month") {
            $loan_term = 30;
        } else if ($loan_term == "Year") {
            $loan_term = 30;
        } 
        if(($loantypeid == 1) or ($loantypeid ==  2)){
            // GeneralWeekly14D
            $loanschedule = $this->fixedLoanMethod(
                $loan_amount,
                $loan_term,$loan_term_value,
                $interest_rate,
                $principal_formula,
                // $start_date);
                $entrydate);
        }else if(($loantypeid == 3) or ($loantypeid == 4)){
            // ExtraLoanWeekly
            $loanschedule = $this->principalFinalMethod30D(
                $loan_amount,
                $loan_term,$loan_term_value,
                $interest_rate,
                $principal_formula,
                // $start_date);
                $entrydate);
        }else if(($loantypeid >= 5) && ($loantypeid <= 19)){
            // 5 to 19
            // ExtraLoanWeekly
            $loanschedule = $this->fixedPrincipalMethod30D(
                $loan_amount,
                $loan_term,$loan_term_value,
                $interest_rate,
                $principal_formula,
                // $start_date);
                $entrydate);
        }else if(($loantypeid >= 20) && ($loantypeid <= 22)){
            // ExtraLoanWeekly
            $loanschedule = $this->principalFinalMethod28D(
                $loan_amount,
                $loan_term,$loan_term_value,
                $interest_rate,
                $principal_formula,
                // $start_date);
                $entrydate);
        }else if(($loantypeid >= 23) && ($loantypeid <= 25)){
            // GeneralMonthly28D
            $loanschedule = $this->fixedPrincipalMethod28D(
                $loan_amount,
                $loan_term,$loan_term_value,
                $interest_rate,
                $principal_formula,
                // $start_date);
                $entrydate);
        }else if(($loantypeid == 26) or ($loantypeid == 27)){
            // ExtraLoanWeekly
            $loanschedule = $this->fixedPrincipalMethod30D(
                $loan_amount,
                $loan_term,$loan_term_value,
                $interest_rate,
                $principal_formula,
                // $start_date);
                $entrydate);
        }
       
    	if($loan_term_value == 1){
    		$loan_term_value = 12;
    	}else{
    		$loan_term_value = $loan_term_value;
    	}
    
        $data_loantype = array(
            "loan_id" => $loan_id,
            "loan_name" => $loan_name,
        	"loan_amount" => $loan_amount,
            "loan_term" => $loan_term,
         	"loan_term_value" => $loan_term_value,
         	"interest_rate" => $interest_rate,
         	"interest_rate_period" => $interest_rate_period,
         	"interest_method" => $interest_method,
        );

        //------------------------------------
        if($loanschedule){
            return response()->json(['status_code'=>200,'message'=>'success','loantype_info'=>$data_loantype,'loan_info'=>$loan_info,'loan_schedule'=>$loanschedule]);
        }else{
            return response()->json(['status_code'=>422,'message'=>'fail','data'=>null]);
        }
    }

//fixed principal and interest
    public function fixedLoanMethod($loan_amount,$loan_term,$loan_term_value,$interest_rate,$principal_formula,$start_date) {
        // amount * 0.083 - principal
        // amount *( 28% / 365 ) *28 - interest
        if( $loan_term_value == 1){ // 1 mean 12 months
            $loan_term_value = 12;
        }

        $schedule_date = $start_date;
        $principal = round($loan_amount / $loan_term_value);
        $interest = round($principal * 14/100);

        for ($i = 1; $i <= $loan_term_value; $i++) {
           
            $total = $loan_amount - $principal;
            $loan_amount -=  $principal;
            $balance = $loan_amount;

            if($i == 1) {
                $schedule_date = date("d-M-Y", strtotime($schedule_date . '+' . '0' . 'days'));
            } else {
                $schedule_date = date("d-M-Y", strtotime($schedule_date . '+' . $loan_term . 'days'));
            }
        
            $loanschedule[$i] = array(
                "repay_count" => $i,
                "repay_principal" => $principal,
                "repay_principal" => $principal,
                "repay_interest" => $interest,
                "now_balance" => $balance,
                "next_balance" => $total,
                "date" => $schedule_date
            );
        }
        return $loanschedule;
    }

    //pay principal at final 30days
    public function principalFinalMethod30D($loan_amount,$loan_term,$loan_term_value,$interest_rate,$principal_formula,$start_date) {
        // amount * 0.083 - principal
        // amount *( 28% / 365 ) *28 - interest
        if( $loan_term_value == 1){ // 1 mean 12 months
            $loan_term_value = 12;
        }

        $schedule_date = $start_date;
        $principal = 0;
        $next_balance = $loan_amount;
        for ($i = 1; $i <= $loan_term_value; $i++) {
            // $paid_principal = 0;
            if($i == $loan_term_value) {
                $principal = $loan_amount;
            }
            // $paid_principal += $principal;

            $interest = round($loan_amount * (($interest_rate/ 100) / 12));
            $next_balance -= $principal;
            $total = $loan_amount - $principal;
            $loan_amount -=  $principal;
            $balance = $loan_amount;

            if($i == 1) {
                $schedule_date = date("d-M-Y", strtotime($schedule_date . '+' . '0' . 'days'));
            } else {
                $schedule_date = date("d-M-Y", strtotime($schedule_date . '+' . $loan_term . 'days'));
            }

            $loanschedule[$i] = array(
                "repay_count" => $i,
                "repay_principal" => $principal,
                "repay_interest" => $interest,
                "now_balance" => $balance,
                "next_balance" => $total,
                "balance" => $balance,
                "total" => $total,
                "principal" => $principal,
                "date" => $schedule_date,
                "interest" => $interest,
            );
        }
        return $loanschedule;
    }

    //pay principal at final 28 days
    public function principalFinalMethod28D($loan_amount,$loan_term,$loan_term_value,$interest_rate,$principal_formula,$start_date) {
        // amount * 0.083 - principal
        // amount *( 28% / 365 ) *28 - interest
        if( $loan_term_value == 1){ // 1 mean 12 months
            $loan_term_value = 12;
        }

        $schedule_date = $start_date;
        $principal = 0;
        $next_balance = $loan_amount;
        $interest = round($loan_amount * (($interest_rate/ 100) / 365) * 28);

        for ($i = 1; $i <= $loan_term_value; $i++) {
            // $paid_principal = 0;
            if($i == $loan_term_value) {
                $principal = $loan_amount;
            }
            // $paid_principal += $principal;

            $next_balance -= $principal;
            $total = $loan_amount - $principal;
            $loan_amount -=  $principal;
            $balance = $loan_amount;

            if($i == 1) {
                $schedule_date = date("d-M-Y", strtotime($schedule_date . '+' . '0' . 'days'));
            } else {
                $schedule_date = date("d-M-Y", strtotime($schedule_date . '+' . $loan_term . 'days'));
            }

            $loanschedule[$i] = array(
                "repay_count" => $i,
                "repay_principal" => $principal,
                "repay_interest" => $interest,
                "now_balance" => $balance,
                "next_balance" => $total,
                "balance" => $balance,
                "total" => $total,
                "principal" => $principal,
                "date" => $schedule_date,
                "interest" => $interest,
            );
        }
        return $loanschedule;
    }

    //fixed principal and interest change 30 days
    public function fixedPrincipalMethod30D($loan_amount,$loan_term,$loan_term_value,$interest_rate,$principal_formula,$start_date) {
        // amount * 0.083 - principal
        // amount *( 28% / 365 ) *28 - interest
        if( $loan_term_value == 1){ // 1 mean 12 months
            $loan_term_value = 12;
        }
        // dd($principal_formula);
        $schedule_date = $start_date;
        $principal = round($loan_amount * $principal_formula);
        // dd($principal);
        $next_balance = $loan_amount;
        for ($i = 1; $i <= $loan_term_value; $i++) {

            $paid_principal = 0;
            if($i == $loan_term_value) {
                $principal = $loan_amount - $paid_principal;
            }
            $paid_principal += $principal;

            $interest = round($next_balance * ($interest_rate/ 100) / 12);
            $next_balance -= $principal;
            $total = $loan_amount - $principal;
            $loan_amount -=  $principal;
            $balance = $loan_amount;

            if($i == 1) {
                $schedule_date = date("d-M-Y", strtotime($schedule_date . '+' . '0' . 'days'));
            } else {
                $schedule_date = date("d-M-Y", strtotime($schedule_date . '+' . $loan_term . 'days'));
            }

            $loanschedule[$i] = array(
                "repay_count" => $i,
                "repay_principal" => $principal,
                "repay_interest" => $interest,
                "now_balance" => $balance,
                "next_balance" => $total,
                "balance" => $balance,
                "total" => $total,
                "principal" => $principal,
                "date" => $schedule_date,
                "interest" => $interest,
            );
        }
        return $loanschedule;
    }

     //fixed principal and interest change 28 days
     public function fixedPrincipalMethod28D($loan_amount,$loan_term,$loan_term_value,$interest_rate,$principal_formula,$start_date) {
        // amount * 0.083 - principal
        // amount *( 28% / 365 ) *28 - interest
        if( $loan_term_value == 1){ // 1 mean 12 months
            $loan_term_value = 12;
        }

        $schedule_date = $start_date;
        $principal = round($loan_amount * $principal_formula);
        $next_balance = $loan_amount;
        for ($i = 1; $i <= $loan_term_value; $i++) {

            $paid_principal = 0;
            if($i == $loan_term_value) {
                $principal = $loan_amount - $paid_principal;
            }
            $paid_principal += $principal;

            $interest = round($next_balance * (($interest_rate/ 100) / 365) * 28);
            $next_balance -= $principal;
            $total = $loan_amount - $principal;
            $loan_amount -=  $principal;
            $balance = $loan_amount;

            if($i == 1) {
                $schedule_date = date("d-M-Y", strtotime($schedule_date . '+' . '0' . 'days'));
            } else {
                $schedule_date = date("d-M-Y", strtotime($schedule_date . '+' . $loan_term . 'days'));
            }

            $loanschedule[$i] = array(
                "repay_count" => $i,
                "repay_principal" => $principal,
                "repay_interest" => $interest,
                "now_balance" => $balance,
                "next_balance" => $total,
                "balance" => $balance,
                "total" => $total,
                "principal" => $principal,
                "date" => $schedule_date,
                "interest" => $interest,
            );
        }
        return $loanschedule;
    }
}
