<?php

namespace App\Http\Controllers\Api\Loan;

use App\Http\Controllers\Controller;
use App\Models\Staff;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Loan\SavingWithdrawl;

class LoanDepositController extends Controller
{
    //auto get branch id
    public function getBranchID($staffid){
        //dd($request->staffid);
        $obj_branchcode = DB::table('tbl_staff')
            ->join('tbl_branches', 'tbl_branches.id', '=', 'tbl_staff.branch_id')
            ->where('tbl_staff.staff_code',$staffid)
            ->select('tbl_branches.branch_code')
            ->first();
        
        $res = strtolower((string)$obj_branchcode->branch_code); 
        return $res;               
    } 



    // approved Deposit List
    public function getLoanDepositApprovedList (Request $request)
    {
        $branchid = $request->branchid;
        $staffid = $request->staffid;
        // Branch ID
        $bcode = "";
        $bcode = $this->getBranchID($staffid);
		
        $data = DB::table($bcode.'_loans')
        ->join('loan_type', 'loan_type.id', '=', $bcode.'_loans.loan_type_id')
        ->join('tbl_group', 'tbl_group.group_uniquekey', '=', $bcode.'_loans.group_id')
        ->join('tbl_center', 'tbl_center.center_uniquekey', '=', $bcode.'_loans.center_id')
        ->join('tbl_client_basic_info', 'tbl_client_basic_info.client_uniquekey', '=', $bcode.'_loans.client_id')
        ->leftjoin('tbl_main_join_loan', 'tbl_main_join_loan.portal_loan_id', '=', $bcode.'_loans.loan_unique_id')
        ->leftjoin('tbl_main_join_client', 'tbl_main_join_client.portal_client_id', '=', $bcode.'_loans.client_id')
       	->leftjoin('tbl_main_join_center', 'tbl_main_join_center.portal_center_id', '=', $bcode.'_loans.center_id')
        ->leftjoin('tbl_main_join_group', 'tbl_main_join_group.portal_group_id', '=', $bcode.'_loans.group_id')
        ->leftjoin('tbl_main_join_guarantor', 'tbl_main_join_guarantor.portal_guarantor_id', '=', $bcode.'_loans.guarantor_a')
        ->where($bcode.'_loans.loan_officer_id',  $staffid)
        ->where($bcode.'_loans.disbursement_status',  'Approved')
        ->select( $bcode.'_loans.*','loan_type.name as loantype_name','tbl_client_basic_info.*','tbl_group.group_uniquekey','tbl_center.center_uniquekey',
                 'tbl_main_join_guarantor.main_guarantor_code as main_guarantor_cod_a',
                 'tbl_main_join_client.main_client_code','tbl_main_join_loan.main_loan_code','tbl_main_join_center.main_center_code','tbl_main_join_group.main_group_code')
        ->orderBy($bcode.'_loans.first_installment_date', 'DESC')->get();

     	// Payment Number
    	$loanpaymentnumber = $this->genPaymentNum($bcode);
    
        if($data){
            return response()->json(['status_code'=>200,'message'=>'success','data-paymentnumber' => $loanpaymentnumber,'data'=>$data]);
        }else{
            return response()->json(['status_code'=>422,'message'=>'fail','data'=>null]);
        }
    }

    public function getLoanDepositTotal(Request $request)
    {
	//return response()->json(['data' => $request->loanamount]);
        $branchid = $request->branchid;
        $staffid = $request->staffid;
        $loan_unique_id = $request->loanid;
	$loan_amount = $request->loanamount;
        // Branch ID
        $bcode = "";
        $bcode = $this->getBranchID($staffid);

        $charge = DB::table($bcode . '_loan_charges_compulsory_join')
            ->leftJoin('loan_charges_type', $bcode . '_loan_charges_compulsory_join.loan_charge_id', '=', 'loan_charges_type.charge_code')
            ->where($bcode . '_loan_charges_compulsory_join.loan_unique_id', $loan_unique_id)
            ->where($bcode . '_loan_charges_compulsory_join.loan_charge_id', '!=', null)
            ->get();

        // dd($charge);

        $compulsory = DB::table($bcode . '_loan_charges_compulsory_join')
            ->leftJoin('loan_compulsory_type', $bcode . '_loan_charges_compulsory_join.loan_compulsory_id', '=', 'loan_compulsory_type.compulsory_code')
            ->where($bcode . '_loan_charges_compulsory_join.loan_unique_id', $loan_unique_id)
            ->where($bcode . '_loan_charges_compulsory_join.loan_compulsory_id', '!=', null)
            ->get();

        $this_branch_id = DB::table('tbl_staff')->where('staff_code', '=', $staffid)->select('branch_id')->first();
        $accounts = DB::table('account_chart_externals')->where('branch_id', '=', $branchid )->where('external_acc_code', 'LIKE', '1-11' . '%')->get();
        $charge_deposit = 0;
        foreach ($charge as $c) {
            if ($c->charge_option == 2) {
                $charge_deposit += $loan_amount * ($c->amount / 100);
            } else {
                $charge_deposit += $c->amount;
            }
        }

        $compulsory_deposit = 0;
        foreach ($compulsory as $c) {
            if ($c->charge_option == 2) {
                $compulsory_deposit += $loan_amount * ($c->saving_amount / 100);
            } else {
                $compulsory_deposit += $c->saving_amount;
            }
        }


        $total_deposit = $charge_deposit + $compulsory_deposit;
    
    // Payment Number
    	$loanpaymentnumber = $this->genPaymentNum($bcode);

        if($total_deposit){
            return response()->json(['status_code'=>200,'message'=>'total deposit found','data-totaldeposit' => $total_deposit,'data-paymentnumber' => $loanpaymentnumber]);
        }else{
            return response()->json(['status_code'=>422,'message'=>'total deposit not found','data'=>null]);
        }
    }

    // active Deposit List
    public function getLoanDepositActiveList (Request $request)
    {
        $branchid = $request->branchid;
        $staffid = $request->staffid;
        // Branch ID
        $table_branchcode = "";
        $table_branchcode = $this->getBranchID($staffid);

        $data = DB::table($table_branchcode.'_deposit')
        ->join($table_branchcode.'_loans', $table_branchcode.'_loans.loan_unique_id', '=', $table_branchcode.'_deposit.loan_id')
        ->join('tbl_client_basic_info', 'tbl_client_basic_info.client_uniquekey', '=', $table_branchcode.'_deposit.client_id')
        ->where($table_branchcode.'_loans.loan_officer_id',  $staffid)
        ->select( $table_branchcode.'_deposit.*',$table_branchcode.'_loans.*','tbl_client_basic_info.*')
        ->orderBy($table_branchcode.'_deposit.deposit_pay_date', 'DESC')->get();

        //$accounts = DB::table('account_chart_externals')->where('branch_id', '=', $$branchid)->where('external_acc_code', 'LIKE', '1-11' . '%')->get();
        if($data){
            return response()->json(['status_code'=>200,'message'=>'success','data'=>$data]);
        }else{
            return response()->json(['status_code'=>422,'message'=>'fail','data'=>null]);
        }
    }


    // search Deposit List
    public function getLoanDepositListSearch (Request $request)
    {
        $branchid = $request->branchid;
        $staffid = $request->staffid;
        $searchvalue = $request->searchvalue;
        // Branch ID
        $table_branchcode = "";
        $table_branchcode = $this->getBranchID($staffid);
    
		$data = DB::table($table_branchcode.'_loans')
        ->leftjoin('tbl_client_basic_info', 'tbl_client_basic_info.client_uniquekey', '=', $table_branchcode.'_loans.client_id')
        ->leftjoin('tbl_staff', 'tbl_staff.staff_code', '=', $table_branchcode.'_loans.loan_officer_id')
        ->leftjoin('tbl_guarantors', 'tbl_guarantors.guarantor_uniquekey', '=', $table_branchcode.'_loans.guarantor_a')
        ->leftjoin('loan_type', 'loan_type.id', '=', $table_branchcode.'_loans.loan_type_id')
        ->join('tbl_group', 'tbl_group.group_uniquekey', '=', $table_branchcode.'_loans.group_id')
        ->join('tbl_center', 'tbl_center.center_uniquekey', '=', $table_branchcode.'_loans.center_id')
        ->leftjoin('tbl_main_join_loan', 'tbl_main_join_loan.portal_loan_id', '=', $table_branchcode.'_loans.loan_unique_id')
        ->leftjoin('tbl_main_join_client', 'tbl_main_join_client.portal_client_id', '=', $table_branchcode.'_loans.client_id')
       	->leftjoin('tbl_main_join_center', 'tbl_main_join_center.portal_center_id', '=', $table_branchcode.'_loans.center_id')
        ->leftjoin('tbl_main_join_group', 'tbl_main_join_group.portal_group_id', '=', $table_branchcode.'_loans.group_id')
        ->leftjoin('tbl_main_join_guarantor', 'tbl_main_join_guarantor.portal_guarantor_id', '=', $table_branchcode.'_loans.guarantor_a')
        ->where($table_branchcode.'_loans.loan_unique_id', 'LIKE', $searchvalue . '%')
        ->orWhere($table_branchcode.'_loans.disbursement_status', 'LIKE', $searchvalue . '%')
        ->orWhere($table_branchcode.'_loans.loan_officer_id', 'LIKE', $searchvalue . '%')
        ->orWhere('tbl_staff.name', 'LIKE', $searchvalue . '%')
        ->orWhere('tbl_client_basic_info.client_uniquekey', 'LIKE', $searchvalue . '%')
        ->orWhere('tbl_client_basic_info.name', 'LIKE', $searchvalue . '%')
        ->orWhere('tbl_client_basic_info.name_mm', 'LIKE', $searchvalue . '%')
        ->orWhere('tbl_client_basic_info.nrc', 'LIKE', $searchvalue . '%')
        ->orWhere('tbl_client_basic_info.old_nrc', 'LIKE', $searchvalue . '%')
        ->orWhere('tbl_client_basic_info.nrc_card_id', 'LIKE', $searchvalue . '%')
        ->orWhere('tbl_client_basic_info.phone_primary', 'LIKE', $searchvalue . '%')
        ->orWhere('tbl_client_basic_info.phone_secondary', 'LIKE', $searchvalue . '%')
        ->orWhere('tbl_guarantors.name', 'LIKE', $searchvalue . '%')
        ->orWhere('loan_type.name', 'LIKE', $searchvalue . '%')
        ->orWhere('tbl_client_basic_info.dob', 'LIKE', $searchvalue . '%')
        ->orWhere('tbl_client_basic_info.phone_secondary', 'LIKE', $searchvalue . '%')
        ->orWhere('tbl_main_join_client.main_client_code', 'LIKE', $searchvalue . '%') // main in search
        ->orWhere('tbl_main_join_loan.main_loan_id', 'LIKE', $searchvalue . '%') // main in search
        ->orWhere('tbl_main_join_center.main_center_code', 'LIKE', $searchvalue . '%') // main in search
        ->orWhere('tbl_main_join_group.main_group_code', 'LIKE', $searchvalue . '%') // main in search
        ->orWhere('tbl_main_join_guarantor.main_guarantor_code', 'LIKE', $searchvalue . '%') // main in search
        ->orWhere($table_branchcode.'_loans.center_id', 'LIKE', $searchvalue . '%')
        ->orWhere($table_branchcode.'_loans.group_id', 'LIKE', $searchvalue . '%')
        ->where($table_branchcode.'_loans.loan_officer_id',  $staffid)
		->where($table_branchcode.'_loans.disbursement_status','=',  'Approved')
         ->select( $table_branchcode.'_loans.*','loan_type.name as loantype_name','tbl_group.group_uniquekey','tbl_center.center_uniquekey','tbl_client_basic_info.*',
                  'tbl_main_join_guarantor.main_guarantor_code as main_guarantor_cod_a',
                  'tbl_main_join_client.main_client_code','tbl_main_join_loan.main_loan_code','tbl_main_join_center.main_center_code','tbl_main_join_group.main_group_code')

        ->orderBy($table_branchcode.'_loans.loan_unique_id', 'DESC')
        ->get();
//     	$data = DB::table($table_branchcode.'_loans')
//          ->leftjoin('tbl_client_basic_info', 'tbl_client_basic_info.client_uniquekey', '=', $table_branchcode.'_loans.client_id')
//         ->leftjoin('loan_type', 'loan_type.id', '=', $table_branchcode.'_loans.loan_type_id')
//         ->join('tbl_group', 'tbl_group.group_uniquekey', '=', $table_branchcode.'_loans.group_id')
//         ->join('tbl_center', 'tbl_center.center_uniquekey', '=', $table_branchcode.'_loans.center_id')
       
//         ->where($table_branchcode.'_loans.loan_officer_id',  $staffid)
//         ->where($table_branchcode.'_loans.disbursement_status','=',  'Approved')
//         ->orWhere($table_branchcode.'_loans.loan_unique_id', 'LIKE',$searchvalue . '%')
//         ->orWhere('tbl_client_basic_info.name', 'LIKE', $searchvalue . '%')
//         ->orWhere('tbl_client_basic_info.client_uniquekey', 'LIKE', $searchvalue . '%')
//         ->select( $table_branchcode.'_loans.*','loan_type.name as loantype_name','tbl_group.group_uniquekey','tbl_center.center_uniquekey')
        // ->orderBy($table_branchcode.'_loans.first_installment_date', 'DESC')->get();
    
        // $data = DB::table($table_branchcode.'_deposit')
        // ->join($table_branchcode.'_loans', $table_branchcode.'_loans.loan_unique_id', '=', $table_branchcode.'_deposit.loan_id')
        // ->join('tbl_client_basic_info', 'tbl_client_basic_info.client_uniquekey', '=', $table_branchcode.'_deposit.client_id')
        // ->join('loan_type', 'loan_type.id', '=', $table_branchcode.'_loans.loan_type_id')
        // ->where($table_branchcode.'_loans.loan_officer_id',  $staffid)
        // ->orwhere($table_branchcode.'_loans.loan_unique_id', 'LIKE', '%' .$searchvalue . '%')
        // ->orWhere('tbl_client_basic_info.name', 'LIKE', $searchvalue . '%')
        // ->orWhere('tbl_client_basic_info.client_uniquekey', 'LIKE', $searchvalue . '%')
        // ->select( $table_branchcode.'_deposit.*',$table_branchcode.'_loans.*','tbl_client_basic_info.*','loan_type.name as loantype_name')
        // ->orderBy($table_branchcode.'_deposit.deposit_pay_date', 'DESC')->get();

    
        if($data){
            return response()->json(['status_code'=>200,'message'=>'success','data'=>$data]);
        }else{
            return response()->json(['status_code'=>422,'message'=>'result not found','data'=>null]);
        }
    }

	public function genPaymentNum($bcode) {
        $bcode = strtoupper($bcode);
        $randomNum = rand(1, 9999);;
        $num = $bcode;
        $num .= '-P-';
        $num .= str_pad($randomNum, 5, 0, STR_PAD_LEFT);
        $num .= date('d');
    	return $num;
    }


    public function createDeposit(Request $request)
    {
    
        	
     	$branchid = $request->branchid;
        $staffid = $request->staffid;
        $loan_id = $request->loan_id;
        $bcode = $this->getBranchID($staffid);
    


        $acc_code_for_principal_saving=array("203851","203852");
        $principal_save_acccode=DB::table('accounts')->whereIn('sub_acc_code',$acc_code_for_principal_saving)->get('id')->toArray();
        $acc_code_for_interest_saving=array("203853","203854");
        $interest_save_acccode=DB::table('accounts')->whereIn('sub_acc_code',$acc_code_for_interest_saving)->select('id')->get();
        $acc_code_for_interest_debit=array("646030","630010");
        $interest_debit_acccode=DB::table('accounts')->whereIn('sub_acc_code',$acc_code_for_interest_debit)->select('id')->get();
        // dd($interest_debit_acccode);
        

        $loan=DB::table($bcode.'_loans')->where('loan_unique_id',$loan_id)
                    ->leftJoin('tbl_client_basic_info','tbl_client_basic_info.client_uniquekey','=',$bcode.'_loans.client_id')
                    ->first();

        $charge = DB::table($bcode.'_loan_charges_compulsory_join')
                ->leftJoin('loan_charges_type', 'loan_charges_type.charge_code','=',$bcode.'_loan_charges_compulsory_join.loan_charge_id')
                ->where($bcode.'_loan_charges_compulsory_join.loan_unique_id', $loan_id)
                ->where($bcode.'_loan_charges_compulsory_join.loan_charge_id', '!=', null)
                ->where('loan_charges_type.product_type','=','savings')
                ->get();
        
//         DB::table('acc_saving_join')->insert([
//             'loan_unique_id'=>$request->loan_id,
//             'principal_credit_acc_id1'=>$principal_save_acccode[0]->id,
//             'principal_credit_acc_id2'=>$principal_save_acccode[1]->id,
//             'interest_credit_acc_id1'=>$interest_save_acccode[0]->id,
//             'interest_credit_acc_id2'=>$interest_save_acccode[1]->id,
//             'interest_debit_acc_id1'=>$interest_debit_acccode[0]->id,
//             'interest_debit_acc_id2'=>$interest_debit_acccode[1]->id,

//         ]);
//         $lastId = DB::table('acc_saving_join')->get('acc_loan_join_id')->last();
        // dd($lastId);

            if($charge){
            $saving_in_charges=0;
                for ($i=0; $i < count($charge); $i++) {
                    if($charge[$i]->charge_option=="1"){
                        $saving_in_charges += $charge[$i]->charge;
                    }else{
                        $charge2=$loan->loan_amount * ($charge[$i]->charge / 100);
                        $saving_in_charges += $charge2;
                    }
                }
            }else{
                $saving_in_charges=0;
            }

            $compulsory = DB::table($bcode.'_loan_charges_compulsory_join')
                ->leftJoin('loan_compulsory_type', 'loan_compulsory_type.compulsory_code','=',$bcode.'_loan_charges_compulsory_join.loan_compulsory_id')
                ->where($bcode.'_loan_charges_compulsory_join.loan_unique_id', $loan_id)
                ->where($bcode.'_loan_charges_compulsory_join.loan_compulsory_id', '!=', null)
                ->get();
           
            if($compulsory){
                $compulsory_saving=0;
                    for ($i=0; $i < count($compulsory); $i++) {
                        if($compulsory[$i]->charge_option=="1"){
                            $compulsory_saving += $compulsory[$i]->saving_amount;
                        }else{
                            $charge2=$loan->loan_amount * ($compulsory[$i]->saving_amount / 100);
                            $compulsory_saving += $charge2;
                        }
                    }
                }else{
                    $compulsory_saving=0;
                }
        $total_saving=$saving_in_charges+ $compulsory_saving;
        // dd($total_saving);

                // DB::table($bcode.'_deposit')->insert([
                //     'loan_id' => $loan_id,
                //     'client_id' => $request->client_id,
                //     'ref_no' => $request->ref_no,
                //     'invoice_no' => $request->invoice_no,
                //     // 'compulsory_saving' => $compulsory_saving,
                //     'acc_join_id' => $lastId->acc_loan_join_id,
                //     //'acc_join_id' => $lastId->acc_loan_join_id,
                //     'total_deposit_balance' => $request->total_deposit_balance,
                //     'paid_deposit_balance' => $request->paid_deposit_balance,
                //     'note' => $request->note,
                //     'deposit_pay_date' => date("Y-m-d H:i:s", strtotime($request->deposit_pay_date))
                //  ]);
    
    		$accounts = DB::table('account_chart_externals')->where('branch_id', '=', $branchid)->where('external_acc_code', 'LIKE', '1-11' . '%')->get();
    		
    		$acc_code=DB::table('account_chart_external_details')->where('account_chart_external_details.external_acc_id',$accounts[0]->id)->first()->main_acc_code;

            $main_acc_code = [$acc_code];
        	// dd($main_acc_code);
            for ($i = 0; $i < count($main_acc_code); $i++) {
                DB::table($bcode . '_deposit')->insert([
                    'loan_id' => $request->loan_id,
                    'client_id' => $request->client_id,
                    'ref_no' => $request->ref_no,
                    'invoice_no' => $request->invoice_no,
                    'acc_join_id' => $main_acc_code[$i],
                    'total_deposit_balance' => $request->total_deposit_balance,
                    'paid_deposit_balance' => $request->paid_deposit_balance,
                    'note' => $request->note,
                    'deposit_pay_date' => date("Y-m-d H:i:s", strtotime($request->deposit_pay_date)),
                ]);
            }

            $charges_compulsory = DB::table('loan_charges_compulsory_join')
                ->leftJoin($bcode.'_loans', 'loan_charges_compulsory_join.loan_unique_id', '=', $bcode.'_loans.loan_unique_id')
                ->where($bcode.'_loans.loan_unique_id', $loan_id)
                ->select('loan_charges_compulsory_join.*')
                ->get();

            foreach ($charges_compulsory as $c) {
                DB::table('loan_charges_compulsory_join')
                    ->where('compulsory_code', $c->id)
                    ->update([
                        "status" => "paid"
                    ]);
            }


            $entry=DB::table($bcode.'_loans')->where('loan_unique_id', $loan_id)->update([
                'disbursement_status' => 'Deposited'
            ]);

           //Add to saving withdrawl
//             $current_saving=DB::table($bcode.'_saving_withdrawl')->where('client_idkey',$loan->client_uniquekey)->get()->first();
//             $saving_id=$this->generateSavingAccID($staffid);
    
//             $this->liveDeposit($request);
    		

//             if(empty($current_saving)){
                
            
                // $saving_withdrawl = new SavingWithdrawl;
                // $saving_withdrawl->setTable($bcode.'_saving_withdrawl');
                // $saving_withdrawl->saving_unique_id=$saving_id;
                // $saving_withdrawl->loan_unique_id = $loan_id;
                // $saving_withdrawl->client_idkey=$loan->client_uniquekey;
                // $saving_withdrawl->client_name=$loan->name;
                // $saving_withdrawl->total_saving=$total_saving;
                // $saving_withdrawl->total_interest=NULL;
                // $saving_withdrawl->saving_left=$total_saving;
                // $saving_withdrawl->interest_left=NULL;
                // $saving_withdrawl->withdrawl_type='none';
                // $saving_withdrawl->status='none';
                // $saving_withdrawl->save();
            
        		// if($saving_withdrawl){
        		// return response()->json(['status_code'=>200,'message'=>'Deposit success & your saving account ID is'.$saving_id]);
        		// }else{
        		// return response()->json(['status_code'=>422,'message'=>'Deposit fail']);
        		// }

            // }else{
                // DB::table($bcode.'_saving_withdrawl')
                //     ->where('saving_unique_id', $current_saving->saving_unique_id)
                //     ->update([
                //         "loan_unique_id"=>$request->loan_id,
                //         "total_saving" => $current_saving->total_saving + $total_saving,
                //         "saving_left"=>$current_saving->saving_left + $total_saving,
                //     ]);
                // $saving_withdrawl = new SavingWithdrawl;
                // $saving_withdrawl->setTable($bcode.'_saving_withdrawl');
                // $saving_withdrawl->saving_unique_id=$current_saving->saving_unique_id;
                // $saving_withdrawl->loan_unique_id = $loan_id;
                // $saving_withdrawl->client_idkey=$loan->client_uniquekey;
                // $saving_withdrawl->client_name=$loan->name;
                // $saving_withdrawl->total_saving=$current_saving->saving_left + $total_saving;
                // $saving_withdrawl->total_interest=$current_saving->interest_left;
                // $saving_withdrawl->saving_left=$current_saving->saving_left + $total_saving;
                // $saving_withdrawl->interest_left=$current_saving->interest_left;
                // $saving_withdrawl->withdrawl_type='none';
                // $saving_withdrawl->status='none';
                // $saving_withdrawl->save();
            

                if($entry){
            		return response()->json(['status_code'=>200,'message'=> 'Deposit success & your saving amount is updated to existing account!']);
        		}else{
            		return response()->json(['status_code'=>422,'message'=>'Deposit fail']);
        		}

            //}
            
        
    }

    //auto generate saving uniquekey
    public function generateSavingAccID($staffid)
    {
            $bcode = $this->getBranchID($staffid);

            $savingId = DB::table($bcode.'_saving_withdrawl')->get()->last();
        	// dd($guarantorkey->guarantor_uniquekey);
        	if($savingId == null) {
            	$bcode = strtoupper( $bcode);
            	$savingId = $bcode;
            	$savingId .= '-';
            	$savingId .= 'S';
            	$savingId .= '-';
            	$savingId .= '0000001';
            } else {
            	$savingId = $savingId->saving_unique_id;

                $bstr = substr($savingId, 3);
            	// dd($bstr);
                $id = substr($bstr, -7, 7);
            	// dd($id);
                $bstr_cusm = $id + 1;
                $bstr_cusm = (string)str_pad($bstr_cusm, 7, '0', STR_PAD_LEFT);

                // get loan_uniquekey in tbl_loans
                $savingId = strtoupper($bcode); // Branch Code
                $savingId .= '-';
            	$savingId .= 'S';
            	$savingId .= '-';
                $savingId .= $bstr_cusm;
            }

            return $savingId;
    }

	public function getMainUrl37()
    {
        // $url = 'http://172.16.50.101/mis-core/public/api/';
        $url = env('MAIN_URL');
        return $url;
    }

    public function loginToken()
    {
        $route = $this->getMainUrl37() . 'login';
        $headers = [
            'Accept' => 'application/json'
        ];
        $response = Http::withHeaders($headers)->post($route, [
            'username' => 'ict@mis.com',
            'password' => '123456'
        ]);
        return json_decode($response)->data[0]->token;
    }

    public function liveDeposit($depositDetail)
    {
        $route = $this->getMainUrl37();
        $route_name = 'create-deposit';
        $routeurl = $route . $route_name;
        $authorization = $this->loginToken();
    	$branch_id = DB::table('tbl_staff')->where('staff_code', Session::get('staff_code'))->first()->branch_id;

        $loan_id = DB::table('tbl_main_join_loan')->where('portal_loan_id', $depositDetail->loan_id)->first();
        if ($loan_id) {
            $loan_id = $loan_id->main_loan_id;
        	$acc_code = DB::table('account_chart_external_details')->where('account_chart_external_details.external_acc_id', $depositDetail->cash_acc_id)->first()->main_acc_code;
            $main_acc_code = [$acc_code];
        	// dd($main_acc_code);
        

            $client = new \GuzzleHttp\Client(['verify' => false]);
            $result = $client->post($routeurl, [
                'headers' => [
                    'Content-Type' => 'application/x-www-form-urlencoded',
                    'Authorization' => 'Bearer ' . $authorization,
                ],
                'form_params' => [
                    'applicant_number_id' => $loan_id,
                    'loan_deposit_date' =>  $depositDetail->deposit_pay_date,
                    'referent_no' => $depositDetail->ref_no,
                    'customer_name' => $depositDetail->client_name,
                    'nrc' => $depositDetail->client_nrc,
                    'client_id' => DB::table('tbl_main_join_client')->where('portal_client_id', $depositDetail->client_id)->first()->main_client_id,
                    'invoice_no' => $depositDetail->invoice_no,
                    'compulsory_saving_amount' => $depositDetail->compulsory_saving,
                    'cash_acc_id' => $main_acc_code[0],  //153620,
                    'total_deposit' => $depositDetail->total_deposit_balance,
                    'client_pay' => $depositDetail->paid_deposit_balance,
                    'note' => $depositDetail->note,
                    'branch_id' => $branch_id,
                ]

            ]);

            $response = (string) $result->getBody();
            $response = json_decode($response);
            if ($response->status_code == 200) {
                DB::table('tbl_main_join_loan_deposit')->insert([
                    'main_loan_deposit_id' => $loan_id,
                    'main_loan_deposit_client_id' => DB::table('tbl_main_join_client')->where('portal_client_id', $depositDetail->client_id)->first()->main_client_id,
                    'portal_loan_deposit_id' => $depositDetail->loan_id
                 ]);
            }
        }
    }
}
