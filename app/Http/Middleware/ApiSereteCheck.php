<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ApiSereteCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    // secrete
    public function handle(Request $request, Closure $next)
    {
        if($request->headers->get('secretekey') !== env('KEY_SECER')){
              return response()->json(['status_code'=>401,'message'=>'Unauthorized, invalid secrete_key']);
        }
        return $next($request);
    }

}
