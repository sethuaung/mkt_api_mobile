<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    use HasFactory;
    protected $table='tbl_client_photo';
    protected $fillable =
    [
        'client_uniquekey','client_photo','recognition_front','recognition_back','registration_family_form_front','registration_family_form_back','figner_print_bio','government_recommendations_photo'
    ];
}
