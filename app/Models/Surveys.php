<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Surveys extends Model
{
    use HasFactory;
    protected $table= 'tbl_surveys';
    protected $fillable =
    [
        'name','created_by','updated_by'
    ];
}
