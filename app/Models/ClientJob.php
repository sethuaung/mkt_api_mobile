<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClientJob extends Model
{
    use HasFactory;
    protected $table= 'tbl_client_job_main';
    protected $fillable=
    [
        'client_uniquekey', 'industry_id', 'job_status', 'job_position_id', 'department_id', 'experience', 'salary', 'company_name', 'company_phone', 'company_address'
    ];
}
