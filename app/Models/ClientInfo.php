<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClientInfo extends Model
{
    use HasFactory;
    protected $table= 'clients';
    protected $fillable =
    [
        'branch_id', 'loan_officer_access_id', 'customer_group_id', 'seq', 'center_code', 'client_number', 'center_name', 'register_date', 'remark', 'account_number', 'name', 'name_other', 'gender', 'dob', 'current_age', 'education', 'primary_phone_number', 'alternate_phone_number', 'optional_phone_number', 'reason_for_no_finger_print', 'nrc_number', 'nrc_type', 'marital_status', 'status', 'father_name', 'husband_name', 'occupation_of_husband', 'no_children_in_family', 'no_of_working_people', 'no_of_dependent', 'no_of_person_in_family', 'more_information', 'address1', 'address2', 'family_registration_copy', 'photo_of_client', 'nrc_photo', 'scan_finger_print', 'reason_for_finger_print', 'you_are_a_group_leader', 'you_are_a_center_leader', 'group_leader_name', 'center_leader_name', 'survey_id', 'ownership_of_farmland', 'ownership', 'province_id', 'district_id', 'commune_id', 'village_id', 'ward_id', 'street_number', 'house_number', 'user_id', 'center_leader_id', 'loan_officer_id', 'updated_by', 'created_by', 'created_at', 'updated_at', 'education_status', 'registration_no', 'id_format', 'cash_acc_code', 'contact_person', 'family_phone_number', 'form_photo_front', 'form_photo_back', 'company_letter_head', 'community_recommendation', 'employment_certificate', 'other_document', 'company_province_id', 'company_district_id', 'company_commune_id', 'company_village_id', 'company_ward_id', 'company_house_number', 'company_address1', 'condition', 'lat', 'lng', 'client_pending_id', 'excel'
    ];
}
