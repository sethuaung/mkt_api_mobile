<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JobDepartment extends Model
{
    use HasFactory;
    protected $table='tbl_job_department';
    protected $fillable = 
    [
        'industry_id', 'department_name', 'department_name_mm', 'del_status'
    ];
}
