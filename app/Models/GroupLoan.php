<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GroupLoan extends Model
{
    use HasFactory;
    protected $table='group_loans';
    protected $fillable = 
    [
        'group_code', 'group_name', 'seq', 'center_id', 'created_at', 'updated_at', 'created_by', 'updated_by', 'id_format', 'branch_id', 'group_pending', 'group_deposit', 'group_disbursement', 'group_repayment'
    ];
}
