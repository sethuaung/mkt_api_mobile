<?php

namespace App\Models\Loan;

use App\Models\Branch;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LoanDeposit extends Model
{
    use HasFactory;

    protected $table= 'deposit';

    function __construct($value = null) {
        // $_REQUEST['branchid'] same of (branchid = 6)
        $branchcode = strtolower((string)(Branch::where('id',$_REQUEST['branchid'])->first())->branch_code);
        $this->table = $branchcode.'_deposit'; // yg_loan_type 
    }

    protected $fillable =[
        'deposit_id', 'loan_id', 'client_id', 'ref_no', 'invoice_no', 'compulsory_saving', 'cash_paid_bankaccount_id', 'total_deposit_balance', 'paid_deposit_balance', 'note', 'deposit_pay_date'
    ];

}
