<?php

namespace App\Models\Loan;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LoanBusinessType extends Model
{
    use HasFactory;
    protected $table ='tbl_business_type';
    protected $fillable =['business_type_name','business_type_name_mm'];
}
