<?php

namespace App\Models\Loan;

use App\Models\Branch;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LoanDisbursement extends Model
{
    use HasFactory;
    protected $table= 'disbursement';

    function __construct($value = null) {
        // $_REQUEST['branchid'] same of (branchid = 6)
        $branchcode = strtolower((string)(Branch::where('id',$_REQUEST['branchid'])->first())->branch_code);
        $this->table = $branchcode.'_disbursement'; // yg_loan_type 
    }

    protected $fillable =[
        'disbursement_id', 'loan_id', 'client_id', 'center_id', 'group_id', 'branch_id', 'loan_officer_id', 'loan_product_id', 'cash_acc_id', 'disbursed_amount', 'principal', 'interest', 'penalty', 'service_charges', 'total_balance', 'paid_deposit_balance', 'paid_by', 'cash_by', 'remark', 'disbursement_status', 'first_installment_date', 'processing_date'
    ];
}
