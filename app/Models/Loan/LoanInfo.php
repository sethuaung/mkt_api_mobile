<?php

namespace App\Models\Loan;

use App\Models\Branch;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LoanInfo extends Model
{
    use HasFactory;

    protected $table= 'loans';

    function __construct($value = null) {
        // $_REQUEST['branchid'] same of (branchid = 6)
        $branchcode = strtolower((string)(Branch::where('id',$_REQUEST['branchid'])->first())->branch_code);
        $this->table = $branchcode.'_loans'; // yg_loan_type 
    }

    protected $fillable =[
        'loan_unique_id', 'process_join_id', 'branch_id', 'group_id', 'center_id', 'loan_officer_id', 'client_id', 'guarantor_a', 'guarantor_b', 'guarantor_c', 'loan_type_id', 'loan_amount', 'estimate_receivable_amount', 'loan_term_value', 'loan_term', 'interest_rate_period', 'disbursement_status', 'interest_method', 'remark', 'status_del', 'loan_application_date', 'first_installment_date', 'disbursement_date', 'cancel_date', 'active_at', 'created_at', 'updated_at', 'business_category_id'
    ];
}
