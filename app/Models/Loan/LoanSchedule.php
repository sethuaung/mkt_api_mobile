<?php

namespace App\Models\Loan;

use App\Models\Branch;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LoanSchedule extends Model
{
    use HasFactory;

    protected $table= 'loans_schedule';

    function __construct($value = null) {
        // $_REQUEST['branchid'] same of (branchid = 6)
        $branchcode = strtolower((string)(Branch::where('id',$_REQUEST['branchid'])->first())->branch_code);
        $this->table = $branchcode.'_loans_schedule'; // yg_loan_type 
    }
    
    protected $fillable =[
        'loan_unique_id', 'loan_type_id', 'month', 'capital', 'interest', 'discount', 'refund_interest', 'refund_amount', 'final_amount', 'status', 'repayment_date', 'created_at', 'updated_at', 'principal_balance', 'interest_balance', 'total_balance'
    ];
}
