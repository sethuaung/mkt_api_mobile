<?php

namespace App\Models\Loan;

use App\Models\Branch;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LoanRepaymentDue extends Model
{
    use HasFactory;

    protected $table= 'repayment_due';

    function __construct($value = null) {
        // $_REQUEST['branchid'] same of (branchid = 6)
        $branchcode = strtolower((string)(Branch::where('id',$_REQUEST['branchid'])->first())->branch_code);
        $this->table = $branchcode.'_repayment_due'; // yg_loan_type 
    }
    
    protected $fillable =[
        'repayment_id', 'disbursement_id', 'cash_acc_id', 'payment_number', 'receipt_no', 'repayment_process_id', 'principal', 'interest', 'penalty', 'compulsory_saving', 'total_balance', 'final_paid_balance', 'own_balance', 'over_days', 'documents', 'payment_method', 'repayment_status', 'payment_date'
    ];
}
