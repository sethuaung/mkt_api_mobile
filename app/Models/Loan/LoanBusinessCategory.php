<?php

namespace App\Models\Loan;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LoanBusinessCategory extends Model
{
    use HasFactory;
    protected $table ='tbl_business_category';
    protected $fillable =['business_type_id','business_category_name','business_category_name_mm'];
}
