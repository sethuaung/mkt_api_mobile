<?php

namespace App\Models\Loan;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LoanType extends Model
{
    use HasFactory;

    protected $table= 'loan_type';

    protected $fillable =
    [
        'name', 'description', 'interest_method', 'interest_rate_period', 'interest_rate', 'loan_term', 'loan_term_value', 'status', 'active_at'
    ];


}
