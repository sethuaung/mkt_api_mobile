<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Guarantor extends Model
{
    use HasFactory;
    protected $table= 'tbl_guarantors';
    protected $fillable=
    [
        'branch_id', 'guarantor_uniquekey', 'name', 'dob', 'nrc', 'gender', 'phone_primary', 'phone_secondary', 'email', 'blood_type', 'religion', 'nationality', 'education_id', 'other_education', 'income', 'village_id', 'province_id', 'quarter_id', 'township_id', 'district_id', 'division_id', 'city_id', 'address_primary', 'address_secondary', 'guarantor_nrc_front_photo', 'guarantor_nrc_back_photo', 'guarantor_photo', 'status'
    ];
}
