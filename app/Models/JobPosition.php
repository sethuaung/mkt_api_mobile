<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JobPosition extends Model
{
    use HasFactory;
    protected $table='tbl_job_position';
    protected $fillable = 
    [
        'department_id', 'job_position_name', 'job_position_name_mm', 'del_status'
    ];
}
