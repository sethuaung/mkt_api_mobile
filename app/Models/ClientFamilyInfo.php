<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClientFamilyInfo extends Model
{
    use HasFactory;
    protected $table= 'tbl_client_family_info';
    protected $fillable = 
    [
        'client_uniquekey','father_name','marital_status','spouse_name','occupation_of_spouse','no_of_family','no_of_working_family','no_of_children','no_of_working_progeny'
    ];
}
