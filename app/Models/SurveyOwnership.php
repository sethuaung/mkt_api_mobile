<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SurveyOwnership extends Model
{
    use HasFactory;
    protected $table = 'tbl_client_survey_ownership';
    protected $fillable =
    [
        'client_uniquekey', 'assetstype_removable', 'assetstype_unremovable', 'purchase_price', 'current_value', 'quantity', 'attach_1', 'attach_2', 'attach_3', 'attach_4'
    ];
}
