<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Staff extends Model
{
    use HasFactory;
    protected $table = 'tbl_staff';
    protected $fillable = [ 'staff_code', 'finger_biometric', 'name', 'phone', 'father_name', 'nrc', 'full_address', 'email', 'email_verified_at', 'user_acc', 'user_password', 'photo','branch_code','branch_id', 'center_leader_id', 'user_token', 'del_status'];
}

