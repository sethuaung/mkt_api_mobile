<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClientBasicInfo extends Model
{
    use HasFactory;
    protected $table = 'tbl_client_basic_info';
    protected $fillable = [
        'client_uniquekey', 'name', 'name_mm', 'dob', 'nrc', 'old_nrc', 'nrc_card_id', 'gender', 'phone_primary', 'phone_secondary', 'email', 'blood_type', 'religion', 'nationality', 'education_id', 'other_education', 'village_id', 'province_id', 'quarter_id', 'township_id', 'district_id', 'division_id', 'city_id', 'address_primary', 'address_secondary', 'client_type', 'status'
    ];
}
