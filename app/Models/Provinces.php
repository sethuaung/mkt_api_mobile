<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Provinces extends Model
{
    use HasFactory;
    protected $table='tbl_provinces';
    protected $fillable = 
    [
        'district_id', 'province_name', 'province_name_mm', 'del_status'
    ];
}
