<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JobIndustryType extends Model
{
    use HasFactory;
    protected $table='tbl_industry_type';
    protected $fillable = 
    [
        'industry_name', 'industry_name_mm', 'del_status'
    ];
}
