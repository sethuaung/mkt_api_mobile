<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Quarters extends Model
{
    use HasFactory;
    protected $table='tbl_quarters';
    protected $fillable = 
    [
        'township_id', 'quarter_name', 'quarter_name_mm', 'del_status'
    ];
}
