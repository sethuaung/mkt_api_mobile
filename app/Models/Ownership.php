<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ownership extends Model
{
    use HasFactory;
    protected $table= 'tbl_ownerships';
    protected $fillable =
    [
        'name','created_by','updated_by'
    ];
}
