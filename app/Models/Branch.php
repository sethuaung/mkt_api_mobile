<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    use HasFactory;
    protected $table ='tbl_branches';
    protected $fillable= 
    [
        'branch_name','branch_code','client_prefix','branch_photo','title','phone_primary','phone_secondary','phone_tertiary','discription','village_id','quarter_id',
        'province_id','township_id','district_id','city_id','division_id','location','full_address'
    ];
}
